angular.module("admin", ['angularUtils.directives.dirPagination'])
    .controller('LanguageCtrl', function ($scope, $http) {
        
        $scope.languageDataAll = [];
        $scope.currentPage = 1;
        $scope.showLoader = true;
        $scope.showLanguageList = false;
        $scope.showNoRecord = false;
        $scope.showadd = true;
        $scope.showedit = false;
        $scope.total_count = 0;
        $scope.orderByField = 'created_on';
        $scope.reverseSort = true;
        $scope.itemsPage = ['10', '25', '50', '100'];
        $scope.selectedName = $scope.itemsPage[0];
        $scope.itemsPerPage = $scope.itemsPage[0];
        
        
        $scope.getRecords = function (currentPage) {
                var begin = ((currentPage - 1) * $scope.itemsPerPage);
                var end = begin + $scope.itemsPerPage;
                $http.get(base_url + 'admin/language/getLanguage/' + begin + '/' + $scope.itemsPerPage)
                        .success(function (response) {
                            if (response.status === 'OK') {
                                $scope.showLoader = false;
                                $scope.showLanguageList = true;
                                $scope.showNoRecord = false;
                                $scope.languageDataAll = response.records;
                                $scope.currentPage = currentPage;
                                $scope.total_count = response.numrecords;
                                $scope.tempLanguageData = {};
                            } else {
                                $scope.showLoader = false;
                                $scope.showNoRecord = true;
                                $scope.showLanguageList = false;
                            }
                        });
            };

        $scope.getRecords($scope.currentPage);
        
        $scope.changeNoOfItem = function () {
                $scope.itemsPerPage = $scope.selectedName;
                $scope.getRecords($scope.currentPage);
        };
        
        $scope.addLanguage = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                  
                    var data = $.param({
                        'data': $scope.tempLanguageData
                    });
                    $http.post(base_url + "admin/language/addLanguage", data, config)
                            .success(function (response) { 
                                if (response.status == 'OK') {

                                    $('#closeModalbtn').trigger('click');
                                    $scope.tempLanguageData = {};
                                    $scope.getRecords($scope.currentPage);
                                    toastr.success(response.msg);

                                } else {
                                   
                                    toastr.error(response.msg);
                                }
                            });
                }
         };
         
        $scope.deleteLanguage = function (language) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': language.id,
                                'type': 'delete'
                            });
                            $http.post(base_url + "admin/language/deleteLanguage", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.getRecords($scope.currentPage);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });

                        });

        };
        
        $scope.changeStatus = function (language) {
                var data = $.param({
                    'id': language.id,
                    'status': language.status
                });
                $http.post(base_url + "admin/language/changeStatus", data, config)
                        .success(function (response) {
                            if (response.status == 'OK') {
                                $scope.getRecords($scope.currentPage);
                                toastr.success(response.msg);
                            } else {
                                toastr.error(response.msg);
                            }
                        });

        };
        
        $scope.cancel = function () {
                $scope.tempLanguageData = {};
                $scope.showadd = true;
                $scope.showedit = false;
               
        };
        
        $scope.editLanguage = function (language) {
                $scope.tempLanguageData = {
                    id: language.id,
                    name: language.name
                };
               
                $scope.showadd = false;
                $scope.showedit = true;
        };
        
        $scope.updateLanguage = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                    var data = $.param({
                        'data': $scope.tempLanguageData
                    });
                    $http.post(base_url + "admin/language/editLanguage", data, config)
                            .success(function (response) {

                                if (response.status == 'OK') {
                                    $('#closeModalbtn').trigger('click');
                                    //$scope.abForm.$setPristine();
                                    $scope.tempLanguageData = {};
                                    $scope.getRecords($scope.currentPage);
                                    $scope.showadd = true;
                                    $scope.showedit = false;
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }

                            });
                }

        };
 })
 .filter('dateToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
 });