angular.module("admin", ['angularUtils.directives.dirPagination'])
    .controller('FieldCtrl', function ($scope, $http) {
        
        $scope.fieldDataAll = [];
        $scope.currentPage = 1;
        $scope.showLoader = true;
        $scope.showfieldList = false;
        $scope.showNoRecord = false;
        $scope.showadd = true;
        $scope.showedit = false;
        $scope.total_count = 0;
        $scope.orderByField = 'createdOn';
        $scope.reverseSort = true;
        $scope.itemsPage = ['10', '25', '50', '100'];
        $scope.selectedName = $scope.itemsPage[0];
        $scope.itemsPerPage = $scope.itemsPage[0];
        $scope.showParentField = false;
        $scope.parentFieldList = [];
        $scope.tempFieldData = {};
        $scope.add=true;
        $scope.edit=false;


         $scope.getRecords = function (currentPage) {
                var begin = ((currentPage - 1) * $scope.itemsPerPage);
                var end = begin + $scope.itemsPerPage;
                $http.get(base_url + 'admin/field/getField/' + begin + '/' + $scope.itemsPerPage)
                        .success(function (response) {
                            if (response.status === 'OK') {
                                $scope.showLoader = false;
                                $scope.showFieldList = true;
                                $scope.showNoRecord = false;
                                $scope.fieldDataAll = response.records;
                                $scope.tempFieldData = {};
                                $scope.currentPage = currentPage;
                                $scope.total_count = response.numrecords;
                                var parentFieldOb= {};
                                $.each($scope.fieldDataAll, function (key, value) {
                                parentFieldOb[value.id]=value.fieldName;
                               }); 
                                
                                $scope.parentFieldOb=parentFieldOb;
                               

                            } else {
                                $scope.showLoader = false;
                                $scope.showNoRecord = true;
                                $scope.showFieldList = false;
                            }
                        });
                        $scope.add=true;
                        $scope.edit=false;
            };

            $scope.getRecords($scope.currentPage);
        /*$scope.parentFieldList=function(){

            $http.get(base_url + 'admin/field/getParentField')
                        .success(function (response) {
                            if (response.status === 'OK') {
                              return response.records;
                             } 
                        });
         
        };*/
        $scope.changeNoOfItem = function () {
                $scope.itemsPerPage = $scope.selectedName;
                $scope.getRecords($scope.currentPage);
        };
        
       
         
        $scope.deleteField = function (field) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': field.id
                               
                            });
                            $http.post(base_url + "admin/field/deleteField", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.getRecords($scope.currentPage);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });

                        });

        };
        
        $scope.changeStatus = function (field) {
                var data = $.param({
                    'id': field.id,
                    'status': field.status
                });
                $http.post(base_url + "admin/field/changeStatus", data, config)
                        .success(function (response) {
                            if (response.status == 'OK') {
                                $scope.getRecords($scope.currentPage);
                                toastr.success(response.msg);
                            } else {
                                toastr.error(response.msg);
                            }
                        });

        };
        
        $scope.cancel = function () {
                $scope.tempFieldData = {};
                $scope.showadd = true;
                $scope.showedit = false;
                $scope.add=true;
                $scope.edit=false;
               
        };
        
        $scope.editField = function (field) {
                $scope.tempFieldData = {
                    id: field.id,
                    fieldName: field.fieldName,
                    parentId: field.parentId
                };
                    var id ='';
                     $scope.parentFieldList={};
                    if (field.parentId == 0)
                    {   $scope.showParentField=false;
                        $scope.tempFieldData.isParent=1;
                        id=field.id;
                       
                       
                    } else {
                        $scope.showParentField=true;
                        $scope.tempFieldData.isParent=2;

                    }
              $scope.add=false;
              $scope.edit=true;  
              $scope.checkedYes=false;    
               
              $http.get(base_url + 'admin/field/getParentField/'+id)
              .success(function (response) {
              if (response.status === 'OK') {
                $scope.parentFieldList=response.records;
               
              }
              });    

             

               
                $scope.showadd = false;
                $scope.showedit = true;
        };
        
                                                               

        $scope.addEditField = function (isValid,action) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");

                } else if($('input:radio[name=isParent]:checked').val()==2 && $('#parentId').val()=="" ){
                    toastr.error("Parent Field Required !!");

                } else { //alert(JSON.stringify($scope.tempFieldData,null,4));
                    var data = $.param({
                        'data': $scope.tempFieldData
                    });
                    var functionName = action== 'add' ? 'addField' : 'editField';

                    $http.post(base_url + "admin/field/"+functionName, data, config)
                            .success(function (response) {

                                if (response.status == 'OK') {
                                    $('#closeModalbtn').trigger('click');
                                    
                                    $scope.getRecords($scope.currentPage);
                                    $scope.tempFieldData = {};
                                    $scope.showadd = true;
                                    $scope.showedit = false;
                                    $scope.showParentField = false;
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }

                            });
                             $scope.add=true;
                             $scope.edit=false;
                }

        };

       $scope.addField=function(){
         $scope.tempFieldData.isParent=1;
           $scope.add=true;
           $scope.edit=false;

         $http.get(base_url + 'admin/field/getParentField/')
              .success(function (response) {
              if (response.status === 'OK') {
                $scope.parentFieldList=response.records;
                $scope.showParentField=false;
            }
         });
       }; 
 })
 .filter('dateToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
 });