//function to fetch users from database
var app = angular.module("admin", ['angularUtils.directives.dirPagination',  'commonDirectives'])

        .controller('jobseekerController', function ($scope, multipartForm, $http,$log) {
            $scope.imgpath = IMAGE_URL;


            $scope.itemsPage = ['10', '25', '50', '100'];
            $scope.showLoader = true;
            $scope.showUserList = false;
            $scope.selectedName = $scope.itemsPage[0];
            $scope.itemsPerPage = 10;
            $scope.currentPage = 1;
            $scope.reverseSort = true;
            $scope.btnDisable = false;
            $scope.showadd = true;
            $scope.getCountryList = {};
            $scope.getcityList = {};



            $scope.changeNoOfItem = function () {
                $scope.itemsPerPage = $scope.selectedName;
                $scope.getRecords($scope.currentPage);
            };

              $scope.changeCountry = function (i) {
                //alert(i);
                var data = $.param({
                    'country_id': i
                });
                $http.post(base_url + "admin/jobseeker/getCity", data, config)
                        .success(function (response) { alert(JSON.stringify(response.cityList,null,4));
                            if (response) { 
                                $scope.getCityList = response.cityList;
                                $scope.tempUserData.jobseekerCity = '';
                            }
                        });
            };  

           
            $scope.getRecords = function (currentPage) {
                var begin = ((currentPage - 1) * $scope.itemsPerPage);
                var end = begin + $scope.itemsPerPage;
                $http.get(base_url + "admin/jobseeker/getusers/" + begin + "/" + $scope.itemsPerPage)
                        .success(function (response) {
                           
                            if (response.status == 'OK') {
                                $scope.showLoader = false;
                                $scope.showUserList = true;
                                $scope.users = response.records;
                                $scope.currentPage = currentPage;
                                $scope.total_count = response.numrecords;
                                $scope.showNoRecord = false;
                            } else
                            {
                                $scope.showLoader = false;
                                $scope.showNoRecord = true;
                                $scope.showUserList = false;
                            }
                        });
            };  
             $scope.getRecords($scope.currentPage);

              $scope.selectDob = function (dobdate) {
                console.log("dob date=>: ", dobdate);
                return $scope.dobdate = dobdate;
            };


            $scope.phoneNumbr = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;

            $scope.deleteJobSeeker = function (user) {                                      
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                         function () {
                            var data = $.param({
                                'userId': user.userId,
                                'type': 'delete'
                            });
                            $http.post(base_url + "admin/jobseeker/deletejobseeker", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                             $scope.getRecords($scope.currentPage);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });
                        });
            };


             $scope.Submit = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                
                   
                    $scope.disableElm();
                    $scope.tempUserData.dob = $scope.dobdate;
                    var uploadUrl = base_url + "admin/jobseeker/addusers";
                    var cat = multipartForm.post(uploadUrl, $scope.tempUserData);
                    cat.success(function (response) {  //console.log(response);
                        if (response.status === 'OK') {
                            $('#closeModalbtn').trigger('click');
                            $scope.btnDisable = true;
                            $scope.tempUserData = {};
                            $scope.showUserList = true;
                            $scope.getRecords($scope.currentPage);
                            toastr.success(response.msg);
                        } else {
                            $scope.btnDisable = false;
                            toastr.error(response.msg);
                        }
                    });
                }
            };

             $scope.disableElm = function () {
                $scope.btnDisable = true;
            };
           

           
           $scope.editUserdata = function (user) {
                 $scope.SeekerTotalAchievement = {};
                run_waitMe('ios', '#vertical-tabs-1-1', 'Please Wait.....');
                $http.get(base_url + "admin/jobseeker/dataEdit/" + user)
                        .success(function (response) { 
                            if (response.status == 'OK') {
                               
                                //$scope.showpersonal = true;
                                $scope.tempSeekerWorkExp = {
                                    userid: response.personal.userId
                                };
                                $scope.tempSeekerPortfolio = {
                                    userid: response.personal.userId
                                };
                                $scope.tempSeekerEduDetail = {
                                    userid: response.personal.userId
                                };
                                $scope.tempSeekerCertification = {
                                    userid: response.personal.userId
                                };
                                $scope.tempSeekerAchievement = {
                                    userid: response.personal.userId
                                };
                                $scope.tempUserSkillAdd = {
                                    userid: response.personal.userId
                                };
                                $scope.temphobbies = {
                                    userid: response.personal.userId,
                                    hobbies: response.personal.hobbies
                                };
                                $scope.tempUserLangAdd = {
                                    userid: response.personal.userId
                                };
                                $scope.getRefernceList = response.getRefernceList;
                                $scope.getIndustryList = response.getIndustryList;
                                //$scope.getRoleList = response.getRoleList;
                                /*str = JSON.stringify($scope.getIndustryList, null, 4);
                                 alert(str);*/
                                $scope.getFunctionList = response.getFunctionList;
                                $scope.getRoleList = response.getRoleList;
                                $scope.getAllRoleList = response.getAllRoleList;
                                $scope.getskillList = response.getskillList;
                                $scope.getLanguageList = response.getLanguageList;
                                $scope.getqualificationList = response.getqualificationList;
                                $scope.getProficiencyList = response.getProficiencyList;
                                $scope.getGenderList = response.getGenderList;
                                $scope.getMaritalStatusList = response.getMaritalStatusList;
                                $scope.getCountryList = response.getCountryList;

                                $scope.getSpecializationListEdit = response.getSpecializationListEdit;
                                if (response.workExperienceData != false) {
                                    $scope.SeekerTotalWorkExp = response.workExperienceData;

                                } else {
                                    $scope.SeekerTotalWorkExp = {};
                                }
                                if (response.portfolioData != false) {
                                    $scope.SeekerTotalPortfolio = response.portfolioData;

                                } else {
                                    $scope.SeekerTotalPortfolio = {};
                                }
                                if (response.educationDetailData != false) {
                                    $scope.SeekerTotaleducationDetail = response.educationDetailData;

                                } else {
                                    $scope.SeekerTotaleducationDetail = {};
                                }
                                if (response.certificationData != false) {
                                    $scope.SeekerTotalCertification = response.certificationData;

                                } else {
                                    $scope.SeekerTotalCertification = {};
                                }
                                

                                if (response.achievementData != false) {
                                    $scope.SeekerTotalAchievement = response.achievementData;

                                } else {
                                    $scope.SeekerTotalAchievement = {};
                                }
                                if (response.seekerSkillData != false) {
                                    $scope.SeekerTotalSkill = response.seekerSkillData;

                                } else {
                                    $scope.SeekerTotalSkill = {};
                                }

                                if (response.seekerLanguageData != false) {
                                    $scope.SeekerTotalLanguage = response.seekerLanguageData;

                                } else {
                                    $scope.SeekerTotalLanguage = {};
                                }

                                //str = JSON.stringify(response.workExperienceData, null, 4);
                                //alert(str);
                                $scope.tempUserData.jobseekerPreferedIndustry = [];
                                $scope.tempUserData = response.personal;
                                $scope.tempUserData.jobseekerSubscribeEmail = response.personal.jobseekerSubscribeEmail==1 ? true :false;
                                $scope.editUserdataStatus = true;
                            } else
                            {
                                $scope.tempUserData = {};
                            }
                            $('#vertical-tabs-1-1').waitMe('hide');
                        });
            };

            /*=======================================================================*/
           // app.$inject = ['$scope'];
            $scope.progressVisible = false;
            $scope.setFiles = function (element) {
                $scope.$apply(function ($scope) {
                    console.log('element:', element);
                    console.log('files:', element.files);
                    // Turn the FileList object into an Array
                    $scope.files = [];
                    for (var i = 0; i < element.files.length; i++) {
                        $scope.files.push(element.files[i]);
                    }
                    console.log('files2:', $scope.files);
                    $scope.progressVisible = false;
                    $scope.uploadFile();
                });
            };

             $scope.uploadFile = function () {
                // /alert($scope.tempUserData.id);
                var fd = new FormData();
                for (var i in $scope.files) {
                    fd.append("profileImage", $scope.files[i]);

                }
                fd.append("userId", $scope.tempUserData.id); 
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", uploadProgress, false);
                xhr.addEventListener("load", uploadComplete, false);
                xhr.addEventListener("error", uploadFailed, false);
                xhr.addEventListener("abort", uploadCanceled, false);
                xhr.open("POST", base_url + "admin/jobseeker/profileImageUpload");
                $scope.progressVisible = true;
                xhr.send(fd);
            };

            function uploadProgress(evt) {
                $scope.$apply(function () {
                    if (evt.lengthComputable) {
                        $scope.progress = Math.round(evt.loaded * 100 / evt.total);
                    } else {
                        $scope.progress = 'unable to compute';
                    }
                });
            }

            function uploadComplete(evt) {

                /* This event is raised when the server send back a response */
                var response = evt.target.responseText;

                if (response == '1') {
                    toastr.success('Jobseeker Profile Image has been Updated successfully');

                } else if (response == '2') {
                    // $scope.progressVisible = false; 
                    toastr.error('File format will be jpeg,jpg,gif,png or bmp');
                } else if (response == '4') {
                    // $scope.progressVisible = false; 
                    toastr.error('Maximaum file size will be 200kb');
                } else {
                    toastr.error('Some problem occurred, please try again.');
                }
                $scope.editUserdata($scope.tempUserData.userId);
                $scope.progressVisible = false;
            }

            function uploadFailed(evt) {
                toastr.error("There was an error attempting to upload the file.");
            }

            function uploadCanceled(evt) {
                $scope.$apply(function () {
                    $scope.progressVisible = false;
                });
                toastr.error("The upload has been canceled by the user or the browser dropped the connection.");
            }

            /*$scope.afterSubmit = function (fileRes) {
                console.log("add uploaded file=>: ", fileRes);
                return $scope.fileNameNew = fileRes;
            };*/
            /*=====================================================*/


            $scope.progressVisible = false;
            $scope.setResume = function (element) {
                $scope.$apply(function ($scope) {
                    console.log('element:', element);
                    console.log('files:', element.files);
                    // Turn the FileList object into an Array
                    $scope.files = [];
                    for (var i = 0; i < element.files.length; i++) {
                        $scope.files.push(element.files[i]);
                    }
                    console.log('files2:', $scope.files);
                    $scope.progressVisible = false;
                    $scope.uploadFileResume();
                });
            };

             $scope.uploadFileResume = function () {
                // /alert($scope.tempUserData.id);
                var fd = new FormData();
                for (var i in $scope.files) {
                    fd.append("resumeupload", $scope.files[i]);

                }
                fd.append("jobseeker_id", $scope.tempUserData.id); 
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", uploadProgressResume, false);
                xhr.addEventListener("load", uploadCompleteResume, false);
                xhr.addEventListener("error", uploadFailedResume, false);
                xhr.addEventListener("abort", uploadCanceledResume, false);
                xhr.open("POST", base_url + "admin/jobseeker/resumeUpload");
                $scope.progressVisible = true;
                xhr.send(fd);
            };

            function uploadProgressResume(evt) {
                $scope.$apply(function () {
                    if (evt.lengthComputable) {
                        $scope.progress = Math.round(evt.loaded * 100 / evt.total);
                    } else {
                        $scope.progress = 'unable to compute';
                    }
                });
            }

            function uploadCompleteResume(evt) {
                /* This event is raised when the server send back a response */
                var response = evt.target.responseText;
              
                if (response == '1') {
                    toastr.error('Resume format will be microsoft word, doc, docx or PDF only');

                } else if (response == '2') {
                    // $scope.progressVisible = false; 
                    toastr.error('Resume size will be not more than 4MB.');
                } else if (response == '3'){
                    toastr.error('Some problem occurred, please try again.');
                }
                else{
                    toastr.success('Jobseeker Resume has been Updated successfully'); 
                }
               
                $scope.editUserdata($scope.tempUserData.userId);
                $scope.progressVisible = false;
            }



            function uploadFailedResume(evt) {
                toastr.error("There was an error attempting to upload the file.");
            }

            function uploadCanceledResume(evt) {
                $scope.$apply(function () {
                    $scope.progressVisible = false;
                });
                toastr.error("The upload has been canceled by the user or the browser dropped the connection.");
            }

            

             $scope.editSeeker = function (isValid, formtype) {

                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else
                {

                    if (formtype == 'personal') {
                        if($scope.dobdate){
                            $scope.tempUserData.jobseekerDob = $scope.dobdate;
                        }
                        /*if($scope.tempUserData.jobseekerReferenceId){
                            $scope.tempUserData.jobseekerReferenceId = $scope.tempUserData.jobseekerReferenceId.id;
                        }*/
                        $scope.tempUserData.jobseekerSubscribeEmail=$scope.tempUserData.jobseekerSubscribeEmail == true ? 1 : 0;
                        var data = $.param({
                            'personal': $scope.tempUserData
                        });
                        //alert(JSON.stringify($scope.tempUserData),null,4);
                    }
                    if (formtype == 'hobbies') {
                        var data = $.param({
                            'hobbies': $scope.temphobbies
                        });
                    }
                    run_waitMe('ios', '#vertical-tabs-1-1', 'Please Wait.....');
                    $http.post(base_url + "admin/jobseeker/editSeeker", data, config).success(function (response) {
                        $('#vertical-tabs-1-1').waitMe('hide'); //alert(JSON.stringify(response,null,4));
                        if (response.status == 'OK') { 
                            //$scope.showUserList = true;


                            toastr.success(response.msg);

                        } else {
                            toastr.error(response.msg);
                        }

                    });
                }

            };
            $scope.selectFrom = function (dobdate) {
                console.log("from date=>: ", dobdate);
                return $scope.job_from = dobdate;
            };
            $scope.selectTo = function (dobdate) {
                console.log("to date=>: ", dobdate);
                return $scope.job_to = dobdate;
            };


            $scope.addeditWorkExp = function (isValid, action,item) {

                if (!isValid) {
                    toastr.error("From validation required !!");
                } else
                {
                    // /alert('uysdua'); return false;
                    if (action == 'add') {
                        if ($scope.job_from) {
                            $scope.tempSeekerWorkExp.job_from = $scope.job_from;
                        }
                        if ($scope.job_to) {
                            $scope.tempSeekerWorkExp.job_to = $scope.job_to;
                        }
                        //str = JSON.stringify($scope.tempSeekerWorkExp, null, 4);
                       // alert(str);
                        var data = $.param({
                            'add': $scope.tempSeekerWorkExp
                        });
                    }
                    if (action == 'edit') {
                        if ($scope.job_from) {
                            isValid["job_from"] = $scope.job_from;
                        }
                        if ($scope.job_to) {
                            isValid["job_to"] = $scope.job_to;
                        }
                        var data = $.param({
                            'edit': item
                        });
                    }
                    run_waitMe('ios', '#vertical-tabs-1-2', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/addeditSeekerWorkExp", data, config);

                    promise.then(
                            function (payload) {
                                var response = payload.data;
                                if (response.status == 'OK') {
                                    toastr.success(response.msg);
                                    if (action == 'add') {
                                        $scope.editUserdata(response.data.user_id);
                                        $('#closeModalbtn').trigger('click');
                                    }
                                    if (action == 'edit') {
                                        $scope.editUserdata(response.data.user_id);
                                    }
                                } else {
                                    toastr.error(response.msg);
                                }
                                $('#vertical-tabs-1-2').waitMe('hide');
                            });
                }
            };

             /*  $scope.changeRole = function (i, k) {
                var data = $.param({
                    'functionId': i
                });
                $http.post(base_url + "profile/getRoleName", data, config).then(function (response) {
                    response = response.data;
                    if (response) {
                        if (k == 'add') {
                            $scope.getRoleListAdd = response.data;
                        } else {
                            $scope.getRoleList[k] = response.data;
                        }
                    }
                });
            };*/

            $scope.changeRole = function (i, k) {
                //alert(key);
                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                };
                var data = $.param({
                    'functionId': i
                });
                $http.post(base_url + "admin/jobseeker/getRoleName", data, config).success(function (response) {
                    if (response) {
                        if (k == 'add') {
                            $scope.getRoleListAdd = response.data;
                        } else {
                            $scope.getRoleList[k] = response.data;
                        }
                    }

                });
            };

             $scope.deleteWorkExp = function (id, userId) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {

                            var data = $.param({
                                'id': id,
                                'type': 'workexp'
                            });
                            $http.post(base_url + "admin/jobseeker/deleteCommon", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.editUserdata(userId);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });
                        });

            };

             $scope.changeSpec = function (i, k) {
                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                };
                var data = $.param({
                    'id': i
                });
                $http.post(base_url + "admin/jobseeker/getSpecializationList", data, config).success(function (response) {
                    if (response) {
                                 //alert(str);
                        if (k == 'add') {
                            $scope.getSpecializationListAdd = response.data;
                        } else {
                            $scope.getSpecializationListEdit[k] = response.data;
                        }

                    }

                });
            };
            $scope.addeditEduDetail = function (isValid, action,item) {
             
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else
                {
                    if (action == 'add') {
                        var data = $.param({
                            'add': $scope.tempSeekerEduDetail
                        });
                    }
                    if (action == 'edit') {
                        var data = $.param({
                            'edit': item
                        });
                    }
                    run_waitMe('ios', '#vertical-tabs-1-4', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/addeditSeekerEduDetail", data, config);

                    promise.then(
                            function (payload) {
                                //str = JSON.stringify(payload, null, 4);
                                var response = payload.data;
                                if (response.status == 'OK') {

                                    if (action == 'add') {
                                        $scope.editUserdata(response.data.user_id);
                                        $('#closeModalEduDetailbtn').trigger('click');
                                    }
                                    if (action == 'edit') {
                                        $scope.editUserdata(response.data.user_id);
                                    }
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }
                                $('#vertical-tabs-1-4').waitMe('hide');
                            });

                }
            };
             $scope.deleteEduDetail = function (id, userId) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': id,
                                'type': 'eduDetail'
                            });
                            $http.post(base_url + "admin/jobseeker/deleteCommon", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.editUserdata(userId);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });
                        });
            };

             $scope.addeditCertification = function (isValid, action,item) {

                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else
                { 
                    if (action == 'add') {
                        var data = $.param({
                            'add': $scope.tempSeekerCertification
                        });
                    }
                    if (action == 'edit') {
                        var data = $.param({
                            'edit': item
                        });
                    }
                    run_waitMe('ios', '#vertical-tabs-1-6', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/addeditSeekerCertification", data, config);

                    promise.then(
                            function (payload) {
                                $('#vertical-tabs-1-6').waitMe('hide');

                                var response = payload.data;
                                if (response.status == 'OK') {

                                    if (action == 'add') {
                                        $scope.editUserdata(response.data.user_id);
                                        $('#closeModalCertificationbtn').trigger('click');
                                    }
                                    if (action == 'edit') {
                                        $scope.editUserdata(response.data.user_id);
                                    }

                                    toastr.success(response.msg);

                                } else {
                                    toastr.error(response.msg);
                                }
                            });

                }
            };

         $scope.deleteCertification = function (id, userId) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {

                            var data = $.param({
                                'id': id,
                                'type': 'certify'
                            });

                            $http.post(base_url + "admin/jobseeker/deleteCommon", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.editUserdata(userId);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });
                        });
            };


          $scope.addeditAchievement = function (isValid, action,item) {

                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else
                {
                    if (action == 'add') {
                        var data = $.param({
                            'add': $scope.tempSeekerAchievement
                        });
                    }
                    if (action == 'edit') {
                        var data = $.param({
                            'edit': item
                        });
                    }
                    run_waitMe('ios', '#vertical-tabs-1-7', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/addeditSeekerAchievement", data, config);

                    promise.then(
                            function (payload) {
                                $('#vertical-tabs-1-7').waitMe('hide');
                                var response = payload.data;
                                if (response.status == 'OK') {

                                    if (action == 'add') {
                                        $scope.editUserdata(response.data.user_id);
                                        $('#closeModalAchievementbtn').trigger('click');
                                    }
                                    if (action == 'edit') {
                                        $scope.editUserdata(response.data.user_id);
                                    }
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }
                            });

                }
            };

            $scope.deleteAchievement = function (id, userId) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {

                            var data = $.param({
                                'id': id,
                                'type': 'achievement'
                            });

                            $http.post(base_url + "admin/jobseeker/deleteCommon", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.editUserdata(userId);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });
                        });
            };

            $scope.afterSubmitOnEdit = function (fileRes) {
                console.log("edit uploaded file=>: ", fileRes);
                var stat = fileRes;
                console.log("stat=>: ", stat);
                if (stat == '1') {
                    toastr.error('Upload Resume3 format will be microsoft word, doc, docx or PDF only');
                } else if (stat == '2') {
                    toastr.error('Resume size will be not more than 4MB.');
                } else if (stat == '0') {
                    toastr.error('Some problem occurred, please try again.');
                } else {
                    toastr.success('Resume Updated successfully');
                    $scope.editUserdata($scope.tempUserData.userId);
                }
                //return $scope.fileNameNew = fileRes;
            };

             $scope.addeditSkill = function (isValid, action, type) {

                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else
                {
                    if (action == 'add') {
                        var data = $.param({
                            'add': $scope.tempUserSkillAdd
                        });
                    }
                   
                    run_waitMe('ios', '#vertical-tabs-1-5', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/addeditSeekerSkill", data, config);
                  
                    promise.then(
                            function (payload) {
                                $('#vertical-tabs-1-5').waitMe('hide');
                                var response = payload.data; //alert(JSON.stringify(response.data,null,4));
                                if (response.status == 'OK') {

                                    if (action == 'add') {
                                        $scope.editUserdata(response.data.user_id);
                                    }
                                  
                                    toastr.success(response.msg);

                                } else {
                                    toastr.error(response.msg);
                                }
                            });

                }
            };

            $scope.deleteJobSeekerSkill = function (id, userId) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': id,
                                'type': 'skill'
                            });
                            $http.post(base_url + "admin/jobseeker/deleteCommon", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.editUserdata(userId);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });
                        });
            };

            $scope.changeExperience = function (value, id) {
                if (value)
                {
                    var data = $.param({
                        'id': id,
                        'value': value,
                        'type': 'experience'
                    });

                    var config = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                        }
                    };

                    var promise = $http.post(base_url + "admin/jobseeker/changeAppliedStudied", data, config);

                    promise.then(
                            function (payload) {

                                var response = payload.data;
                                if (response.status == 'OK') {

                                    toastr.success(response.msg);
                                    $scope.editUserdata($scope.tempUserData.userId);
                                } else {
                                    toastr.error(response.msg);
                                }
                            });

                }
            };

            $scope.addeditLanguage = function (isValid, action) {

                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else
                {
                    $scope.showLang = false;
                    $scope.showLoaderProfile = true;
                    if (action == 'add') {
                        var data = $.param({
                            'add': $scope.tempUserLangAdd
                        });
                    }
                   
                    run_waitMe('ios', '#vertical-tabs-1-8', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/addeditSeekerLanguage", data, config);

                    promise.then(
                            function (payload) {

                                var response = payload.data;
                                if (response.status == 'OK') {

                                    if (action == 'add') {
                                        $scope.editUserdata(response.data.user_id);
                                    }
                                  

                                    toastr.success(response.msg);

                                } else {
                                    toastr.error(response.msg);
                                }
                                $('#vertical-tabs-1-8').waitMe('hide');
                            });

                }
            };

            $scope.deleteJobSeekerLanguage = function (id, userId) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': id,
                                'type': 'language'
                            });

                            $http.post(base_url + "admin/jobseeker/deleteCommon", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.editUserdata(userId);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });
                        });
            };

            $scope.changeSkillLanguageRead = function (checkValue, id) {
                if (checkValue)
                {
                    var data = $.param({
                        'id': id,
                        'value': checkValue,
                        'type': 'languageRead'
                    });

                    run_waitMe('ios', '#vertical-tabs-1-8', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/changeReadWriteSpeak", data, config);

                    promise.then(
                            function (payload) {
                                $('#vertical-tabs-1-8').waitMe('hide');
                                var response = payload.data;
                                if (response.status == 'OK') {

                                    toastr.success(response.msg);
                                    $scope.editUserdata($scope.tempUserData.userId);
                                } else {
                                    toastr.error(response.msg);
                                }
                            });

                }
            };

            $scope.changeSkillLanguageWrite = function (checkValue, id) {
                if (checkValue)
                {
                    var data = $.param({
                        'id': id,
                        'value': checkValue,
                        'type': 'languageWrite'
                    });
                    run_waitMe('ios', '#vertical-tabs-1-8', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/changeReadWriteSpeak", data, config);

                    promise.then(
                            function (payload) {

                                var response = payload.data;
                                if (response.status == 'OK') {

                                    toastr.success(response.msg);
                                    $scope.editUserdata($scope.tempUserData.userId);
                                } else {
                                    toastr.error(response.msg);
                                }
                                $('#vertical-tabs-1-8').waitMe('hide');
                            });

                }
            };

            $scope.changeSkillLanguageSpeak = function (checkValue, id) {
                if (checkValue)
                {
                    var data = $.param({
                        'id': id,
                        'value': checkValue,
                        'type': 'languageSpeak'
                    });

                    var config = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                        }
                    };
                    run_waitMe('ios', '#vertical-tabs-1-8', 'Please Wait.....');
                    var promise = $http.post(base_url + "admin/jobseeker/changeReadWriteSpeak", data, config);

                    promise.then(
                            function (payload) {

                                var response = payload.data;
                                if (response.status == 'OK') {

                                    toastr.success(response.msg);
                                    $scope.editUserdata($scope.tempUserData.userId);
                                } else {
                                    toastr.error(response.msg);
                                }
                                $('#vertical-tabs-1-8').waitMe('hide');
                            });

                }
            };

                            


     
        })
        .directive('errSrc', function () {
            return {
                link: function (scope, element, attrs) {
                    element.bind('error', function () {
                        if (attrs.src != attrs.errSrc) {
                            attrs.$set('src', attrs.errSrc);
                        }
                    });
                }
            };
        })

        .directive('jqdatepicker', function () {
            return {
                //require: 'ngModel',
                restrict: 'A',
                scope: {
                    returnDobDate: "&"
                },
                link: function (scope, element) {
                    $(element).datepicker({
                        dateFormat: 'yy-mm-dd',
                        showButtonPanel: false,
                        changeMonth: true,
                        changeYear: true,
                        onSelect: function (date) {
                            scope.dob = date;
                            scope.$apply();
                            scope.returnDobDate({"dob": date});
                        }
                    });
                }
            };
        });
        
        /*.directive('myDirectiveOLD', function (httpPostFactory) {
            return {
                restrict: 'A',
                scope: {
                    returnFileName: "&"
                },
                link: function (scope, element, attr) {

                    element.bind('change', function () {
                        //alert(element[0].files[0].type);

                        var formData = new FormData();
                        formData.append('resumeupload', element[0].files[0]);
                        httpPostFactory(base_url + "admin/jobseeker/resumeUpload", formData, function (callback) {
                            // recieve image name to use in a ng-src 
                            //console.log(JSON.parse(callback)  + "==");
                            scope.returnFileName({"resumeupload": JSON.parse(callback)});
                            var stat = JSON.parse(callback);
                            if (stat == '1') {
                                toastr.error('Resume format will be microsoft word, doc, docx or PDF only');
                            } else if (stat == '0') {
                                toastr.error('Some problem occurred, please try again.');
                            } else {
                                toastr.success('Uploaded successfully');
                            }
                        });

                    });

                }
            };
        })*/
        /*.directive('validFile', function (httpPostFactory) {
            return {
                require: 'ngModel',
                restrict: 'A',
                scope: {
                    returnFileName: "&"
                },
                link: function (scope, elem, attr, ngModel) {
                    function bindEvent(element, type, handler) {
                        if (element.addEventListener) {
                            element.addEventListener(type, handler, false);
                        } else {
                            element.attachEvent('on' + type, handler);
                        }
                    }

                    bindEvent(elem[0], 'change', function () {
                        //alert('File size:' + this.files[0].size);
                        var FileExtension = this.files[0].type;
                        var token = false;
                        switch (FileExtension) {
                            case 'application/msword':
                            case 'application/rtf':
                            case 'application/pdf':
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                                token = true;
                                break;
                            default:
                                //alert(FileExtension);
                                toastr.error('Resume format will be microsoft word, doc, docx or PDF only');
                                this.value = '';
                                token = false;
                        }

                        if (token === true) {
                            var formData = new FormData();
                            formData.append('resumeupload', this.files[0]);
                            httpPostFactory(base_url + "admin/jobseeker/resumeUpload", formData, function (callback) {
                                // recieve image name to use in a ng-src 
                                //console.log(JSON.parse(callback)  + "==");
                                scope.returnFileName({"resumeupload": JSON.parse(callback)});
                                var stat = JSON.parse(callback);
                                if (stat == '1') {
                                    toastr.error('Invalid Format!!');
                                } else if (stat == '0') {
                                    toastr.error('Some problem occurred, please try again.');
                                } else {
                                    toastr.success('Uploaded successfully');
                                    ngModel.$setViewValue(elem.val());
                                    ngModel.$render();
                                }
                            });
                        }

                    });
                }
            };
        })
        .directive('editResume', function (httpPostFactory) {
            return {
                restrict: 'A',
                scope: {
                    returnFileName: "&"
                },
                link: function (scope, element, attr) {
                    element.bind('change', function () {
                        var formData = new FormData();
                        formData.append('resumeupload', element[0].files[0]);
                        formData.append('jobseeker_id', element.attr('jobseeker-id'));
                        httpPostFactory(base_url + "admin/jobseeker/resumeUpload", formData, function (callback) {
                            // recieve image name to use in a ng-src 
                            //console.log(JSON.parse(callback)  + "==");
                            //var stat = JSON.parse(callback);
                            scope.returnFileName({"resumeupload": JSON.parse(callback)});
                            //$scope.editUserdata($scope.tempUserData.id);
//                            if(stat == '1'){
//                                toastr.error('Resume format will be microsoft word, doc, docx or PDF only');
//                            }else if(stat == '0'){
//                                toastr.error('Some problem occurred, please try again.');
//                            }else{
//                                toastr.success('Resume Updated successfully');                               
//                            }

                        });
                    });

                }
            };
        })
        .factory('httpPostFactory', function ($http) {
            return function (file, data, callback) {
                $http({
                    url: file,
                    method: "POST",
                    data: data,
                    headers: {'Content-Type': undefined}
                }).success(function (response) {
                    callback(response);
                });
            };
        })
        .directive('errSrc', function () {
            return {
                link: function (scope, element, attrs) {
                    element.bind('error', function () {
                        if (attrs.src != attrs.errSrc) {
                            attrs.$set('src', attrs.errSrc);
                        }
                    });
                }
            };
        })
        .filter('dateToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
        })
        .directive('jqdatepicker', function () {
            return {
                require: 'ngModel',
                restrict: 'A',
                scope: {
                    returnDobDate: "&"
                },
                link: function (scope, element, attrs, ngModelCtrl) {
                    $(element).datepicker({
                        dateFormat: 'yy-mm-dd',
                        showButtonPanel: false,
                        changeMonth: true,
                        changeYear: true,
                        onSelect: function (date) {
                            scope.dob = date;
                            scope.$apply();
                            scope.returnDobDate({"dob": date});
                        }
                    });
                }
            };
        })
        .directive('fileInput', function ($parse) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.bind('change', function () {
                        $parse(attrs.fileInput).assign(scope, element[0].files);
                        scope.$apply();
                    });
                }
            };
        });*/
