var app = angular.module("admin", ['angularUtils.directives.dirPagination','commonDirectives']);

app.filter('dateToISO', function () {
    return function (input) {
        input = new Date(input).toISOString();
        return input;
    };
});

app.controller('adminUserController', function ($scope, $http) {

    $scope.usersData = [];
    $scope.users = [];
    $scope.itemsPage = ['10', '25', '50', '100'];
    $scope.selectedName = $scope.itemsPage[0];
    $scope.showLoader = true;
    $scope.showUserList = false;
    $scope.currentPage = 1;
    $scope.tempUserData = {};
    $scope.orderByField = 'createdOn';
    $scope.reverseSort = true;
    $scope.total_count = 0;
    $scope.itemsPerPage = 10;
    $scope.showadd = true;
    $scope.showedit = false;
    $scope.phoneNumbr = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;
    var adminUserType = [];
    adminUserType[1]='Superadmin';
    adminUserType[2]='Admin';
    adminUserType[3]='Subadmin';
    $scope.adminUserType=adminUserType;      
   
    $scope.changeNoOfItem = function () {
        $scope.itemsPerPage = $scope.selectedName;
        $scope.getRecords($scope.currentPage);
    };

    $scope.getRecords = function (currentPage) {
        var begin = ((currentPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;
        $http.get(base_url + 'admin/adminuser/getusers/' + begin + '/' + $scope.itemsPerPage)
                .success(function (response) {
                    if (response.status == 'OK') {
                        $scope.showLoader = false;
                        $scope.showUserList = true;
                        $scope.users = response.records;
                        $scope.currentPage = currentPage;
                        $scope.total_count = response.numrecords;
                    } else {
                        $scope.showLoader = false;
                        $scope.showNoRecord = true;
                        $scope.showUserList = false;
                    }
                });
    };

    $scope.getRecords($scope.currentPage);

    $scope.deleteUser = function (user) {
        swal({
            title: "Are you sure?",
            text: "Record will be deleted permanently.",
            type: "",
            showCancelButton: true,
            confirmButtonColor: "#3085D6",
            cancelButtonColor: "#D50000",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true
        },
                function () {
                    var data = $.param({
                        'id': user.id,
                        'type': 'delete'
                    });
                    $http.post(base_url + "admin/adminuser/deleteusers", data, config)
                            .success(function (response) {
                                if (response.status == 'OK') {
                                    //var index = $scope.users.indexOf(user);
                                    //$scope.users.splice(index, 1);
                                    $scope.getRecords($scope.currentPage);
                                    //$scope.messageSuccess(response.msg);
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }
                            });

                });

    };

    // function to insert or update user data in modal to the database
    $scope.addUser = function (isValid) {
        if (!isValid) {
            toastr.error("From validation required !!");
        } else
        {
            $scope.users = [];
            var data = $.param({
                'data': $scope.tempUserData
            });
            $http.post(base_url + "admin/adminuser/addusers", data, config).success(function (response) {
                if (response.status == 'OK') {

                    $('#closeModalbtn').trigger('click');
                    //angular.element(document.querySelector("#modal-lg-primary")).modal("hide");//bypass method
                    $scope.tempUserData = {};
                    $scope.showUserList = true;
                    $scope.getRecords($scope.currentPage);
                    toastr.success(response.msg);

                } else if (response.status === 'emailExists') {
                    toastr.warning(response.msg);
                } else {
                    $('#closeModalbtn').trigger('click');
                    toastr.error(response.msg);
                }

            });


        }


    };

    $scope.editUser = function (user) {
        $scope.tempUserData = {
            id: user.id,
            fullname: user.adminFullName,
            useremail: user.adminUsername,
            userpassword: "******",
            contactno: user.adminPhoneNumber,
            usertype: user.adminUserType
        };
        $scope.index = $scope.users.indexOf(user);
        $scope.showadd = false;
        $scope.showedit = true;
    };

    $scope.updateUser = function (isValid) {
        if (!isValid) {
            toastr.error("From validation required !!");
        } else
        {
            var data = $.param({
                'data': $scope.tempUserData
            });
            //console.log(data);
            $http.post(base_url + "admin/adminuser/editusers", data, config).success(function (response) {

                if (response.status == 'OK') {
                    $('#closeModalbtn').trigger('click');
                    //angular.element(document.querySelector("#modal-lg-primary")).modal("hide");//bypass method
                    $scope.userForm.$setPristine();
                    $scope.tempUserData = {};
                    $scope.showUserList = true;
                    $scope.getRecords($scope.currentPage);
                    toastr.success(response.msg);


                } else if (response.status === 'emailExists') {
                    toastr.warning(response.msg);

                } else {
                    toastr.error(response.msg);
                }

            });
        }

    };

    $scope.cancel = function ()
    {
        $scope.tempUserData = {};
        $scope.showadd = true;
        $scope.showedit = false;
        $scope.userForm.$setPristine();
    };


});    