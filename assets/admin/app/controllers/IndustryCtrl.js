angular.module("admin", ['angularUtils.directives.dirPagination'])
    .controller('IndustryCtrl', function ($scope, $http) {
        
        $scope.industryDataAll = [];
        $scope.tempIndustryData = {};
        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;
        $scope.showLoader = true;
        $scope.showIndustryList = false;
        $scope.showNoRecord = false;
        $scope.showadd = true;
        $scope.showedit = false;
        $scope.total_count = 0;
        $scope.orderByField = 'createdOn';
        $scope.reverseSort = true;
        $scope.itemsPage = ['10', '25', '50', '100'];
        $scope.selectedName = $scope.itemsPage[0];
        
        
        $scope.getRecords = function (currentPage) {
                var begin = ((currentPage - 1) * $scope.itemsPerPage);
                var end = begin + $scope.itemsPerPage;
                $http.get(base_url + 'admin/industry/getindustry/' + begin + '/' + $scope.itemsPerPage)
                        .success(function (response) {
                            if (response.status === 'OK') {
                                $scope.showLoader = false;
                                $scope.showIndustryList = true;
                                $scope.showNoRecord = false;
                                $scope.industryDataAll = response.records;
                                $scope.currentPage = currentPage;
                                $scope.total_count = response.numrecords;
                            } else {
                                $scope.showLoader = false;
                                $scope.showNoRecord = true;
                                $scope.showIndustryList = false;
                            }
                        });
            };

        $scope.getRecords($scope.currentPage);
        
        $scope.changeNoOfItem = function () {
                $scope.itemsPerPage = $scope.selectedName;
                $scope.getRecords($scope.currentPage);
        };
        
        $scope.addIndustry = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                    //$scope.industryDataAll = [];
                    var data = $.param({
                        'data': $scope.tempIndustryData
                    });
                    $http.post(base_url + "admin/industry/addindustry", data, config)
                            .success(function (response) { 
                                if (response.status == 'OK') {

                                    $('#closeModalbtn').trigger('click');
                                    $scope.tempIndustryData = {};
                                    //$scope.showIndustryList = true;
                                    $scope.getRecords($scope.currentPage);
                                    toastr.success(response.msg);

                                } else {
                                    //$('#closeModalbtn').trigger('click');
                                    toastr.error(response.msg);
                                }
                            });
                }
         };
         
        $scope.deleteIndustry = function (industry) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': industry.id,
                                'type': 'delete'
                            });
                            $http.post(base_url + "admin/industry/deleteindustry", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            //var index = $scope.industryDataAll.indexOf(industry);
                                            //$scope.industryDataAll.splice(index, 1);
                                            $scope.getRecords($scope.currentPage);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });

                        });

        };
        
        $scope.changeStatus = function (industry) {
                //$scope.succClass = '';
                var data = $.param({
                    'id': industry.id,
                    'status': industry.status
                });
                $http.post(base_url + "admin/industry/changeStatus", data, config)
                        .success(function (response) {
                            if (response.status == 'OK') {
                                //$scope.tempIindustryData = {};
                                //$scope.showIindustryList = true;
                                $scope.getRecords($scope.currentPage);
                                toastr.success(response.msg);
                            } else {
                                toastr.error(response.msg);
                            }
                        });

        };
        
        $scope.cancel = function () {
                $scope.tempIndustryData = {};
                $scope.showadd = true;
                $scope.showedit = false;
               // $scope.industryForm.$setPristine();
        };
        
        $scope.editIndustry = function (industry) {
                $scope.tempIndustryData = {
                    id: industry.id,
                    industryName: industry.industryName
                };
                //$scope.index = $scope.industryDataAll.indexOf(industry);
                $scope.showadd = false;
                $scope.showedit = true;
        };
        
        $scope.updateIndustry = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                    var data = $.param({
                        'data': $scope.tempIndustryData
                    });
                    $http.post(base_url + "admin/industry/editindustry", data, config)
                            .success(function (response) {

                                if (response.status == 'OK') {
                                    $('#closeModalbtn').trigger('click');
                                    $scope.industryForm.$setPristine();
                                    $scope.tempIndustryData = {};
                                    $scope.getRecords($scope.currentPage);
                                    $scope.showadd = true;
                                    $scope.showedit = false;
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }

                            });
                }

        };
 })
 .filter('dateToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
 });