angular.module("admin", ['angularUtils.directives.dirPagination'])
        .controller('DepartmentCtrl', function ($scope, $http) {
            $scope.departmentDataAll = [];
            $scope.tempDepartmentData = {};
            $scope.currentPage = 1;
            $scope.showLoader = true;
            $scope.showDepartmentList = false;
            $scope.showNoRecord = false;
            $scope.showadd = true;
            $scope.showedit = false;
            $scope.total_count = 0;
            $scope.orderByField = 'createdOn';
            $scope.reverseSort = true;
            $scope.itemsPage = ['10', '25', '50', '100'];
            $scope.selectedName = $scope.itemsPage[0];
            $scope.itemsPerPage = $scope.itemsPage[0];
            // function to get records from the database

            $scope.getRecords = function (currentPage) {
                var begin = ((currentPage - 1) * $scope.itemsPerPage);
                var end = begin + $scope.itemsPerPage;
                $http.get(base_url + 'admin/department/getDepartment/' + begin + '/' + $scope.itemsPerPage)
                        .success(function (response) { 
                            if (response.status === 'OK') {
                                $scope.showLoader = false;
                                $scope.showDepartmentList = true;
                                $scope.showNoRecord = false;
                                $scope.departmentDataAll = response.records;
                                $scope.currentPage = currentPage;
                                $scope.total_count = response.numrecords;
                            } else {
                                $scope.showLoader = false;
                                $scope.showNoRecord = true;
                                $scope.showDepartmentList = false;
                            }
                        });
            };

            $scope.getRecords($scope.currentPage);

            $scope.changeNoOfItem = function () {
                $scope.itemsPerPage = $scope.selectedName;
                $scope.getRecords($scope.currentPage);
            };

            $scope.editDepartment = function (department) {
                $scope.tempDepartmentData = {
                    id: department.id,
                    departmentName: department.departmentName
                };
               
                $scope.showadd = false;
                $scope.showedit = true;
            };

            $scope.addDepartment = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                    $scope.DepartmentDataAll = [];
                    var data = $.param({
                        'data': $scope.tempDepartmentData
                    });
                    $http.post(base_url + "admin/department/addDepartment", data, config)
                            .success(function (response) {
                                if (response.status == 'OK') {

                                    $('#closeModalbtn').trigger('click');
                                    $scope.tempDepartmentData = {};
                                    $scope.getRecords($scope.currentPage);
                                    toastr.success(response.msg);

                                } else {
                                     
                                    toastr.error(response.msg);
                                }
                            });
                }
            };

            $scope.updateDepartment = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                    var data = $.param({
                        'data': $scope.tempDepartmentData
                    });
                    $http.post(base_url + "admin/department/editDepartment", data, config)
                            .success(function (response) {

                                if (response.status == 'OK') {
                                    $('#closeModalbtn').trigger('click');
                                    $scope.departmentForm.$setPristine();
                                    $scope.tempDepartmentData = {};
                                    $scope.getRecords($scope.currentPage);
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }

                            });
                }

            };

            $scope.deleteDepartment= function (department) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': department.id
                            });
                            $http.post(base_url + "admin/department/deleteDepartment", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.tempDepartmentData = {};
                                            $scope.getRecords($scope.currentPage);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });

                        });

            };
            
            $scope.changeStatus = function (department) {
                $scope.succClass = '';
                var data = $.param({
                    'id': department.id,
                    'status': department.status
                });
                $http.post(base_url + "admin/department/changeStatus", data, config)
                        .success(function (response) {
                            if (response.status == 'OK') {
                                $scope.getRecords($scope.currentPage);
                                toastr.success(response.msg);
                            } else {
                                toastr.error(response.msg);
                            }
                        });

            };

            $scope.cancel = function () {
                $scope.tempDepartmentData = {};
                $scope.showadd = true;
                $scope.showedit = false;
            };

        })
        .filter('dateToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
        });




