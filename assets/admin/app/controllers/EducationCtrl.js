angular.module("admin", ['angularUtils.directives.dirPagination'])
    .controller('EducationCtrl', function ($scope, $http) {
        
        $scope.educationDataAll = [];
        $scope.currentPage = 1;
        $scope.showLoader = true;
        $scope.showEducationList = false;
        $scope.showNoRecord = false;
        $scope.showadd = true;
        $scope.showedit = false;
        $scope.total_count = 0;
        $scope.orderByField = 'createdOn';
        $scope.reverseSort = true;
        $scope.itemsPage = ['10', '25', '50', '100'];
        $scope.selectedName = $scope.itemsPage[0];
        $scope.itemsPerPage = $scope.itemsPage[0];
        
        
        $scope.getRecords = function (currentPage) {
                var begin = ((currentPage - 1) * $scope.itemsPerPage);
                var end = begin + $scope.itemsPerPage;
                $http.get(base_url + 'admin/education/getEducation/' + begin + '/' + $scope.itemsPerPage)
                        .success(function (response) {
                            if (response.status === 'OK') {
                                $scope.showLoader = false;
                                $scope.showEducationList = true;
                                $scope.showNoRecord = false;
                                $scope.educationDataAll = response.records;
                                $scope.currentPage = currentPage;
                                $scope.total_count = response.numrecords;
                                $scope.tempEducationData = {};
                            } else {
                                $scope.showLoader = false;
                                $scope.showNoRecord = true;
                                $scope.showEducationList = false;
                            }
                        });
            };

        $scope.getRecords($scope.currentPage);
        
        $scope.changeNoOfItem = function () {
                $scope.itemsPerPage = $scope.selectedName;
                $scope.getRecords($scope.currentPage);
        };
        
        /*$scope.addEducation = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                  
                    var data = $.param({
                        'data': $scope.tempEducationData
                    });
                    $http.post(base_url + "admin/education/addEducation", data, config)
                            .success(function (response) { 
                                if (response.status == 'OK') {

                                    $('#closeModalbtn').trigger('click');
                                    $scope.tempEducationData = {};
                                    $scope.getRecords($scope.currentPage);
                                    toastr.success(response.msg);

                                } else {
                                   
                                    toastr.error(response.msg);
                                }
                            });
                }
         };*/
         
        $scope.deleteEducation = function (education) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "",
                    showCancelButton: true,
                    confirmButtonColor: "#3085D6",
                    cancelButtonColor: "#D50000",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true
                },
                        function () {
                            var data = $.param({
                                'id': education.id
                               
                            });
                            $http.post(base_url + "admin/education/deleteEducation", data, config)
                                    .success(function (response) {
                                        if (response.status === 'OK') {
                                            $scope.getRecords($scope.currentPage);
                                            toastr.success(response.msg);
                                        } else {
                                            toastr.error(response.msg);
                                        }
                                    });

                        });

        };
        
        $scope.changeStatus = function (education) {
                var data = $.param({
                    'id': education.id,
                    'status': education.status
                });
                $http.post(base_url + "admin/education/changeStatus", data, config)
                        .success(function (response) {
                            if (response.status == 'OK') {
                                $scope.getRecords($scope.currentPage);
                                toastr.success(response.msg);
                            } else {
                                toastr.error(response.msg);
                            }
                        });

        };
        
        $scope.cancel = function () {
                $scope.tempEducationData = {};
                $scope.showadd = true;
                $scope.showedit = false;
               
        };
        
        $scope.editEducation = function (education) {
                $scope.tempEducationData = {
                    id: education.id,
                    educationLevelName: education.educationLevelName
                };
               
                $scope.showadd = false;
                $scope.showedit = true;
        };
        
        /*$scope.updateEducation = function (isValid) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                    var data = $.param({
                        'data': $scope.tempEducationData
                    });
                    $http.post(base_url + "admin/education/editEducation", data, config)
                            .success(function (response) {

                                if (response.status == 'OK') {
                                    $('#closeModalbtn').trigger('click');
                                    //$scope.abForm.$setPristine();
                                    $scope.tempEducationData = {};
                                    $scope.getRecords($scope.currentPage);
                                    $scope.showadd = true;
                                    $scope.showedit = false;
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }

                            });
                }

        };*/

        $scope.addEditEducation = function (isValid,action) {
                if (!isValid) {
                    toastr.error("Form Validation Required !!");
                } else {
                    var data = $.param({
                        'data': $scope.tempEducationData
                    });
                    var functionName = action== 'add' ? 'addEducation' : 'editEducation';

                    $http.post(base_url + "admin/education/"+functionName, data, config)
                            .success(function (response) {

                                if (response.status == 'OK') {
                                    $('#closeModalbtn').trigger('click');
                                    //$scope.abForm.$setPristine();
                                    $scope.tempEducationData = {};
                                    $scope.getRecords($scope.currentPage);
                                    $scope.showadd = true;
                                    $scope.showedit = false;
                                    toastr.success(response.msg);
                                } else {
                                    toastr.error(response.msg);
                                }

                            });
                }

        };
 })
 .filter('dateToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
 });