'use strict';
/**
 * @author Batch Themes Ltd.
 */
(function() {
    $(function() {
        if (!element_exists('#dashboards-analytics')) {
            return false;
        }
        var config = $.localStorage.get('config');
        var colors = config.colors;
		var counterOneValue=$('.counter-1').attr('data-value');
        $('.counter-1').text(counterOneValue).counterUp({
            delay: 1,
            time: 100
        });
		var counterTwoValue=$('.counter-2').attr('data-value');
        $('.counter-2').text(counterTwoValue).counterUp({
            delay: 1,
            time: 100
        });
		var counterThreeValue=$('.counter-3').attr('data-value');
        $('.counter-3').text(counterThreeValue).counterUp({
            delay: 1,
            time: 100
        });
		var counterFourValue=$('.counter-4').attr('data-value');
        $('.counter-4').text(counterFourValue).counterUp({
            delay: 1,
            time: 100
        });
        $('.counter-5').text('52,774').counterUp({
            delay: 1,
            time: 100
        });
        $('.counter-6').text('5,221').counterUp({
            delay: 1,
            time: 100
        });
        $('.counter-7').text('8,771').counterUp({
            delay: 1,
            time: 100
        });
        $('.counter-8').text('964').counterUp({
            delay: 1,
            time: 100
        });
        animatedPeityBar('.small-bar-1', 32, colors.primary);
        animatedPeityBar('.small-bar-2', 32, colors.primary);
        animatedPeityBar('.small-bar-3', 32, colors.primary);
        animatedPeityBar('.small-bar-4', 32, colors.primary);
        //bar chart
    var dt= [{
        "key": "Job Posted",
        "values": [{
            "x": 'indusnet Technology',
            "y": 8
        }, {
            "x": 'Induslnd Bank',
            "y": 10
        }, {
            "x": 'Axis Bank',
            "y": 6
        }, {
            "x": 'Cloud Fox Tech',
            "y": 5
        }, {
            "x": 'Airtel India Ltd.',
            "y": 4
        }]
    }, {
        "key": "Applications Received",
        "values": [{
            "x": 'indusnet Technology',
            "y": 3
        }, {
            "x": 'Induslnd Bank',
            "y": 4
        }, {
            "x": 'Axis Bank',
            "y": 3
        }, {
            "x": 'Cloud Fox Tech',
            "y": 1
        }, {
            "x": 'Airtel India Ltd.',
            "y": 2
        }]
    }, {
        "key": "Shortlisted",
        "values": [{
            "x": 'indusnet Technology',
            "y": 1
        }, {
            "x": 'Induslnd Bank',
            "y": 2
        }, {
            "x": 'Axis Bank',
            "y": 2
        }, {
            "x": 'Cloud Fox Tech',
            "y": 0
        }, {
            "x": 'Airtel India Ltd.',
            "y": 1
        }]
    }];      
     
        nv.addGraph(function() {
            var chart = nv.models.multiBarChart()
                .reduceXTicks(false)
                .rotateLabels(-20)
                .showControls(true)
                .groupSpacing(0.1)
                .showLegend(true)
                .showYAxis(true)
                .showXAxis(true)
                .color([colors.danger, colors.warning, colors.success]);
            chart.xAxis
                .tickFormat(function(d) { return d; });
            chart.yAxis
                .tickFormat(d3.format(',f'));
            d3.select('#nvd3-bar svg')
                .datum(dt)
                .call(chart);
            nv.utils.windowResize(chart.update);
            return chart;
        });
        easyPieChart('.easy-pie-chart-primary-1', colors.primary, colors.grey200, 100);
        easyPieChart('.easy-pie-chart-primary-2', colors.primary, colors.grey200, 100);
    });
})();
