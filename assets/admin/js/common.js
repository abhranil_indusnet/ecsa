/*
 * @description: Used to for common js function
 * @author: Mohan
 * @date: 17/01/2017 
 */
$(document).ready(function () {

});

function successAlert(msg, dom) {
    dom = dom || "messages";
    $('html, body').animate({
        scrollTop: $(".modal-body").offset().top
    }, 500);
    success_msg = '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' + msg + '</div>';
    $("#" + dom).html(success_msg).show().delay(3000).fadeOut('slow');
}

function errorAlert(msg, dom) {
    dom = dom || "messages";
    $('html, body').animate({
        scrollTop: $(".modal-body").offset().top
    }, 500);

    err_msg = '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><strong>Stop!</strong> ' + msg + '</div>';

    $("#" + dom).html(err_msg).show().delay(3000).fadeOut('slow');
}

function warningAlert(msg, dom) {
    dom = dom || "messages";
    $('html, body').animate({
        scrollTop: $(".modal-body").offset().top
    }, 500);

    err_msg = '<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><strong>Stop!</strong> ' + msg + '</div>';

    $("#" + dom).html(err_msg).show().delay(3000).fadeOut('slow');
}