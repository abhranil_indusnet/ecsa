<div class="jumbotron-1">
        <div class="jumbotron jumbotron-fluid">
            <div class="container-fluid">
                <h1 class="display-3">Welcome To ECSA Admin Console</h1>
                <ol class="breadcrumb icon-home icon-angle-right no-bg">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
     <div class="col-xs-12 main">
                <div class="page-on-top">
                    <div class="row m-b-40">
                        <div class="col-xs-12 col-sm-6 col-xl-3">
                            <div class="text-widget-1 color-white">
                                <div class="row flex-items-xs-middle bg-blue-900">
                                    <div class="col-xs-4 bg-blue-700">
                                        <i class="fa fa-usd fa-2x"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="title">Total Subscriptions</div>
                                        <div class="subtitle counter-1" data-value="1,245"><span>$</span>0</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-xl-3">
                            <div class="text-widget-1 color-white">
                                <div class="row flex-items-xs-middle bg-blue-900">
                                    <div class="col-xs-4 bg-blue-700">
                                        <i class="fa fa-bullseye fa-2x"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="title">Active Candidates</div>
                                        <div class="subtitle counter-2" data-value="50">0</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-xl-3">
                            <div class="text-widget-1 color-white">
                                <div class="row flex-items-xs-middle bg-blue-900">
                                    <div class="col-xs-4 bg-blue-700">
                                        <i class="fa fa-code fa-2x"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="title">Active Jobs</div>
                                        <div class="subtitle counter-3" data-value="2,000">0</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-xl-3">
                            <div class="text-widget-1 color-white">
                                <div class="row flex-items-xs-middle bg-blue-900">
                                    <div class="col-xs-4 bg-blue-700">
                                        <i class="fa fa-bullhorn fa-2x"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="title">Active Employers</div>
                                        <div class="subtitle counter-4" data-value="85">0</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row counters">
                        <div class="col-xs-12 col-xl-9">
                            <div class="charts">
                                <div class="row m-b-20">
                                    <div class="col-xs-12 col-md-6">
                                        <h3><span>$</span><span class="counter-5">0</span></h3>
                                        <p class="color-grey-700 text-sm m-b-10">Total Sales</p>
                                        <span class="small-bar-1 color-white">7,4,5,5,4,4,4,4,6,2,5,10,8,9,7,6,10,9,2,9,8,7,2,3,4,5,2,8,8,7,9,4,9,3,7,2,6,10,5,2</span>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <h3><span class="counter-6">0</span></h3>
                                        <p class="color-grey-700 text-sm m-b-10">Job Seeker Profiles Created</p>
                                        <span class="small-bar-2 color-white">10,8,2,8,6,7,4,5,6,4,4,3,10,4,5,3,8,5,9,5,10,7,8,8,9,5,9,4,4,3,5,4,9,5,10,5,10,4,9,6</span>
                                    </div>
                                </div>
                                <div class="row m-b-20">
                                    <div class="col-xs-12 col-md-6">
                                        <h3><span class="counter-7">0</span></h3>
                                        <p class="color-grey-700 text-sm m-b-10">Employer Profiles Created</p>
                                        <span class="small-bar-3 color-white">6,2,7,5,5,10,6,3,5,5,7,4,10,4,2,8,7,10,2,10,2,5,3,10,9,3,5,8,5,3,4,9,4,9,4,8,9,9,10,3</span>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <h3><span class="counter-8">0</span></h3>
                                        <p class="color-grey-700 text-sm m-b-10">Job Posted</p>
                                        <span class="small-bar-4 color-white">3,6,6,2,8,7,9,5,6,9,4,7,4,5,7,2,6,10,9,10,3,2,8,6,10,10,3,8,9,2,10,10,7,6,10,4,2,9,8,7</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-xl-3">
                            <div class="widget">
                                <div class="row m-b-20">
                                    <div class="col-xs-12">
                                        <p class="color-grey-900 m-b-5">Job Vs Application Status</p>
                                       
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="activity-widget-6">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <p>Total Job Posted</p>
                                                    <p class="f-w-300 m-b-10">This Week</p>
                                                    <progress class="progress-sm progress progress-warning" value="33" max="100">33%</progress>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <p>Total Application received</p>
                                                    <p class="f-w-300 m-b-10">This week</p>
                                                    <progress class="progress-sm progress progress-danger" value="60" max="100">60%</progress>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-b-40">
                        <div class="col-xs-12">
                            <div class="widget">
                                <div class="row m-b-20">
                                    <div class="col-xs-12">
                                        <p class="color-grey-900 m-b-5">Job Vs Application (Trending employers)</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                 
                                        <div id="nvd3-bar">
                                            <svg style="height:320px;width:100%"></svg>
                                        </div>
                                 </div>
                             </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-b-40">
                        <div class="col-xs-12 col-xl-8">
                            <div class="widget">
                                <div class="row m-b-20">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            
                                            <button class="btn btn-outline-secondary btn-raised btn-rounded btn-sm">
		                                    Message</button>
                                        </div>
                                        <p class="color-grey-900 m-b-5">New Candidates</p>
                                      
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div class="checkbox checkbox-primary">
                                                                <label>
                                                <input type="checkbox" data-checked="false" id="checkbox-1">
                                            </label>
                                                            </div>
                                                        </th>
                                                        <th>DOJ</th>
                                                        <th>Name</th>
                                                        <th>Experience</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <label>
                                                <input type="checkbox">
                                            </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            December 21, 2016
                                                        </td>
                                                        <td>
                                                            Ajaya Singh </td>
                                                        <td>
                                                            2 yrs 6 months </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <label>
                                                <input type="checkbox">
                                            </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            December 29, 2016
                                                        </td>
                                                        <td>
                                                            Nilanjan Kr. </td>
                                                        <td>
                                                            4 yrs 2 months </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <label>
                                                <input type="checkbox">
                                            </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            December 29, 2016
                                                        </td>
                                                        <td>
                                                            Rajasri </td>
                                                        <td>
                                                            Freshers </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox checkbox-primary">
                                                                <label>
                                                <input type="checkbox">
                                            </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            December 29, 2016
                                                        </td>
                                                        <td>
                                                            Mukesh Kr. </td>
                                                        <td>
                                                            6 yrs 9 months </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-xl-4">
                            <div class="widget">
                                <div class="row m-b-20">
                                    <div class="col-xs-12">
                                        <div class="dropdown pull-right">
                                            <button type="button" class="btn btn-raised btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filter</button>
                                            <div class="dropdown-menu dropdown-menu-right from-right">
                                                <a class="dropdown-item">This week</a>
                                                <a class="dropdown-item">This month</a>
                                                <a class="dropdown-item">This year</a>
                                                <a class="dropdown-item">Today</a>
                                            </div>
                                        </div>
                                        <p class="color-grey-900 m-b-5">Recruiters</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="user-widget-2">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <div class="badge badge-50">
                                                        <span class="tag tag-raised tag-rounded tag-secondary">2</span>
                                                        <img src="<?=base_url()?>assets/admin/faces/m1.png" class="max-w-50 img-circle" alt="badge" />
                                                    </div>
                                                    <div class="bmd-list-group-col">
                                                        <p class="list-group-item-heading">Lucas smith</p>
                                                        <p class="list-group-item-text color-primary">Apple, Inc.</p>
                                                        <p class="list-group-item-text">Vital Database Dude</p>
                                                    </div>
                                                    <i class="pull-xs-right fa fa-dot-circle-o icon color-secondary"></i>
                                                </li>
                                                <li class="list-group-item">
                                                    <div class="badge badge-50">
                                                        <span class="tag tag-raised tag-rounded tag-info">5</span>
                                                        <img src="<?=base_url()?>assets/admin/faces/w1.png" class="max-w-50 img-circle" alt="badge" />
                                                    </div>
                                                    <div class="bmd-list-group-col">
                                                        <p class="list-group-item-heading">Janet Abshire</p>
                                                        <p class="list-group-item-text color-primary">Pinterest</p>
                                                        <p class="list-group-item-text">Lead Innovation Officer</p>
                                                    </div>
                                                    <i class="pull-xs-right fa fa-dot-circle-o icon color-info"></i>
                                                </li>
                                                <li class="list-group-item">
                                                    <div class="badge badge-50">
                                                        <span class="tag tag-raised tag-rounded tag-primary">6</span>
                                                        <img src="<?=base_url()?>assets/admin/faces/m2.png" class="max-w-50 img-circle" alt="badge" />
                                                    </div>
                                                    <div class="bmd-list-group-col">
                                                        <p class="list-group-item-heading">Lucas Koch</p>
                                                        <p class="list-group-item-text color-primary">Reddit</p>
                                                        <p class="list-group-item-text">Incomparable UX Editor</p>
                                                    </div>
                                                    <i class="pull-xs-right fa fa-dot-circle-o icon color-primary"></i>
                                                </li>
                                                <li class="list-group-item">
                                                    <div class="badge badge-50">
                                                        <span class="tag tag-raised tag-rounded tag-warning">6</span>
                                                        <img src="<?=base_url()?>assets/admin/faces/w2.png" class="max-w-50 img-circle" alt="badge" />
                                                    </div>
                                                    <div class="bmd-list-group-col">
                                                        <p class="list-group-item-heading">Gladys Schuster</p>
                                                        <p class="list-group-item-text color-primary">Coursera</p>
                                                        <p class="list-group-item-text">Primary Product Pioneer</p>
                                                    </div>
                                                    <i class="pull-xs-right fa fa-dot-circle-o icon color-warning"></i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->

                </div>
            </div>