<div class="modal modal-primary fade scale" id="modal-lg-primary" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">

        <form method="post" name="userForm" class="form" novalidate>
            <!--<input type="text" class="form-control" id="usereditid" name="usereditid" value="0" ng-model="tempUserData.id" ng-show="showUserId">-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">User</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <div class="form-group bmd-form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" name="fullname" id="fullname" ng-model="tempUserData.fullname" required>
                        <small class="text-danger" ng-show="submitted && userForm.fullname.$error.required || userForm.fullname.$touched && userForm.fullname.$invalid">Please enter full name</small>
                    </div>
                    <div class="form-group bmd-form-group">
                        <label>Email (Username)</label>
                        <input type="email" class="form-control" name="useremail" id="useremail" ng-model="tempUserData.useremail" required>
                         <small class="text-danger" ng-show="submitted && userForm.useremail.$error.required">Please enter email</small>

                        <small class="text-danger" ng-show="userForm.useremail.$touched && userForm.useremail.$invalid">Please enter a valid email</small>

                    </div>
                    <div class="form-group bmd-form-group">
                        <label>Password</label>

                        <input type="password" class="form-control" name="userpassword" id="userpassword" ng-model="tempUserData.userpassword" ng-minlength="6" required>


                        <small class="text-danger"  ng-show="submitted && userForm.userpassword.$error.required || userForm.userpassword.$invalid && userForm.userpassword.$touched && !userForm.userpassword.$error.minlength">Please enter password</small>
                        <small class="text-danger"  ng-show="userForm.userpassword.$error.minlength">Password should be minimum 6 character long</small>
                    </div>
                    <a href="javascript:void(0);" id="changepass" class="changepass" ng-show="showedit">Change password</a>
                    <a href="javascript:void(0);" id="canceltext" style="display:none;" class="changepass">Cancel</a>			

                    <div class="form-group bmd-form-group">

                        <label>Contact Number</label>
                        <input type="text" class="form-control" name="contactno" id="contactno" value="<?php echo isset($userdata['adminPhoneNumber']) ? $userdata['adminPhoneNumber'] : ""; ?>" ng-model="tempUserData.contactno" number-only  max="10" required>
                       
                        <small class="text-danger" ng-show="submitted && userForm.contactno.$error.required || userForm.contactno.$touched &&  userForm.contactno.$invalid">Please Enter the Mobile Number</small>

                        <!--<small class="text-danger" ng-show="userForm.contactno.$touched && userForm.contactno.$error.pattern">Please Enter the Mobile Number matching pattern [9XXXXXXXXX]</small>
                        <small class="form-text text-muted">Enter Mobile Number in the format of (91xxxxxxxx) :</small>-->
                    </div>
                    
                    <div class="form-group bmd-form-group">
                        <label>User Type</label>
                        <select class="form-control" name="usertype" id="usertype" ng-model="tempUserData.usertype" required> 
                            <option value="">Select User Type</option>
                            <?php foreach ($usertype as $usrtype) { ?>
                                <option value="<?php echo $usrtype['id']; ?>"><?php echo $usrtype['adminUserType']; ?></option>
                            <?php } ?>
                        </select>
                        <small class="text-danger" id="smallusertype"  ng-show="submitted && userForm.usertype.$error.required || userForm.usertype.$touched && userForm.usertype.$invalid">Please select a user type</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="button"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="addUser(userForm.$valid);submitted=true">Submit</button>
                    <button type="button" ng-show="showedit" class="btn btn-raised btn-secondary btn-flat" ng-click="updateUser(userForm.$valid)">Update</button>
                </div>

            </div>
        </form>


    </div>
</div>
<script>

    $(document).on('click', '.changepass', function () {

        $('#userpassword').val('');
        $('#canceltext').show();
        $('#changepass').hide();


    });
    $(document).on('click', '#canceltext', function () {
        $('#userpassword').val('******');
        $('#canceltext').hide();
        $('#changepass').show();


    });

</script>
