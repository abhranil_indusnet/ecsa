<script src="<?= base_url() ?>assets/admin/app/controllers/adminUserController.js"></script>
<div class="jumbotron-1">
    <div class="jumbotron jumbotron-fluid">
        <div class="container-fluid">                
            <h1 class="display-3">User Management</h1>
            <ol class="breadcrumb icon-home icon-angle-right no-bg">
                <li>
                    <a href="<?= base_url() ?>admin/dashboard">
                        Dashboard                    </a>
                </li>
                <li class="active">
                    Admin Users </li>
            </ol>

        </div>
    </div>
</div>
<div class="col-xs-12 main" ng-cloak>
    <div class="page-on-top">
        <div class="row">
            <div class="col-xs-12">
                <div class="widget">
                    <div class="row m-b-20">
                        <div class="col-xs-12">
                            <p class="color-grey-900 m-b-5">Admin Users</p>
                            <div id="messages"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">

                            <div id="datatable-example-2_wrapper" class="dataTables_wrapper">
                                <div class="dataTables_length" id="datatable-example-2_length"><label>Show 

                                        <select ng-options="x for x in itemsPage"  class="form-control"
                                                ng-model="selectedName" 
                                                ng-change="changeNoOfItem()">
                                        </select>
                                        entries
                                    </label></div>
                            <?php if(checkActionPermission($adminMenuSlug,2)){?>
                            <div class="dataTables_length" id="datatable-example-2_length"><a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#modal-lg-primary" id="addmodal">Add User</a></div>
                            <?php } ?>

                                <div id="datatable-example-2_filter1" class="dataTables_filter"><label>Search:<input class="form-control" name="userSearch" ng-model="userSearch" placeholder="" aria-controls="datatable-example-2" type="search"></label>

                                </div>
                               <?php //$adminUserType=array(1=>'Superadmin',2=>'Admin',3=>'Subadmin');?>
                                <table class="table table-hover table-striped table-bordered dataTable" role="grid" aria-describedby="datatable-example-2_info">

                                    <thead>
                                        <tr role="row"> 
                                            <th ng-class="{ sorting_desc: reverseSort == true && orderByField == 'createdOn' , sorting_asc: reverseSort == false && orderByField == 'createdOn' , sorting : orderByField != 'createdOn' }" ng-click="orderByField = 'createdOn'; reverseSort = !reverseSort">Created On</th>

                                            <th ng-class="{ sorting_desc: reverseSort == true && orderByField == 'adminUserType' , sorting_asc: reverseSort == false && orderByField == 'adminUserType' , sorting : orderByField != 'adminUserType' }" ng-click="orderByField = 'adminUserType'; reverseSort = !reverseSort">Admin Type</th>

                                            <th ng-class="{ sorting_desc: (reverseSort == true && orderByField == 'adminFullName'), sorting_asc: (reverseSort == false && orderByField == 'adminFullName'), sorting : orderByField != 'adminFullName' }" ng-click="orderByField = 'adminFullName'; reverseSort = !reverseSort">Name</th>

                                            <th ng-class="{ sorting_desc: reverseSort == true && orderByField == 'adminUsername', sorting_asc: reverseSort == false && orderByField == 'adminUsername' , sorting : orderByField != 'adminUsername' }" ng-click="orderByField = 'adminUsername'; reverseSort = !reverseSort">Email</th>

                                            <th ng-class="{ sorting_desc: reverseSort == true && orderByField == 'adminPhoneNumber' , sorting_asc: reverseSort == false && orderByField == 'adminPhoneNumber' , sorting : orderByField != 'adminPhoneNumber' }" ng-click="orderByField = 'adminPhoneNumber'; reverseSort = !reverseSort">Phone Number</th>
                                            <th colspan="3">Action</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody>
                                        <tr ng-show="showLoader"><td colspan="8" style="text-align:center;"><img src="<?= base_url() ?>assets/admin/images/load.gif" style="border:none;"></td></tr>
                                        <tr dir-paginate="user in users | orderBy:orderByField:reverseSort | filter:userSearch | itemsPerPage:itemsPerPage" total-items="total_count" ng-show="showUserList">
                                            <td>{{user.createdOn| dateToISO | date:'longDate' }}</td>
                                            <td>{{adminUserType[user.adminUserType]}}</td>
                                            <td>{{user.adminFullName}}</td>
                                            <td>{{user.adminUsername}}</td>
                                            <td>{{user.adminPhoneNumber}}</td>
                                            <td><a href="<?php echo base_url(); ?>admin/adminuser/userPermission/{{user.id}}" class="fa fa-x fa-hand-o-up" > Permissions</a>
                                            </td>
                                            <td>
                                              <?php if(checkActionPermission($adminMenuSlug,3)){?>
                                             <a href="javascript:void(0);" class="fa fa-x fa-edit editmodal" data-toggle="modal" data-target="#modal-lg-primary" ng-click="editUser(user)"></a> 
                                             <?php } ?>
                                             </td>
                                             <td>
                                              <?php if(checkActionPermission($adminMenuSlug,4)){?>
                                             <a href="javascript:void(0);" class="fa fa-x fa-trash" ng-click="deleteUser(user)" ></a>
                                             <?php } ?>
                                             </td>
                                        </tr>
                                        <tr ng-show="showNoRecord">
                                            <td colspan="8" style="text-align:center;">No Records found</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div ng-if="showUserList" class="dataTables_info" id="datatable-example-2_info" role="status" aria-live="polite">Showing {{((currentPage - 1) * itemsPerPage) + 1}} to {{((currentPage - 1) * itemsPerPage) + users.length}} of {{ total_count}}</div>

                                <dir-pagination-controls class="dataTables_paginate paging_simple_numbers"
                                                         max-size="5"
                                                         direction-links="true"
                                                         boundary-links="true"
                                                         template_url="<?= base_url() ?>assets/admin/components/pagination.tpl.html"
                                                         on-page-change="getRecords(newPageNumber)" >
                                </dir-pagination-controls>

                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>