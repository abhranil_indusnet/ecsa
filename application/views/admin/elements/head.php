<?php 
require_once 'theme_head.php';
$controller = $this->controller;
define('ASSET_URL', base_url() .'assets/admin/');
define('SITE_URL', base_url() .'admin/');
?>
<script type = "text/javascript">
    var base_url = "<?= base_url(); ?>";
    var IMAGE_URL = '<?= base_url(); ?>assets/admin/images/';
    var UPLOAD_URL = '<?= base_url(); ?>/uploads/';
    var config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
</script>
<?php if ($controller == 'manage_jobs') { ?>
<?php }?>

<!-- Theme jQuery -->
<script src="<?= ASSET_URL ?>bower_components/jquery/dist/jquery.min.js"></script>

<!-- Angular -->
<script src="<?= ASSET_URL ?>scripts/angular.min.js"></script>
<script src="<?= ASSET_URL ?>scripts/angular-route.js"></script>
<script src="<?= ASSET_URL ?>scripts/dirPagination.js"></script>
<script src="<?= ASSET_URL ?>scripts/ui-bootstrap-tpls-0.7.0.js"></script>

<!-- jQuery ui 1.12.1-->
<link href="<?= ASSET_URL ?>js/ui/1.12.1/jquery-ui.css" rel="stylesheet" media="all" type="text/css" >
<script src="<?= ASSET_URL ?>js/jquery-1.12.4.js"></script>
<script src="<?= ASSET_URL ?>js/ui/1.12.1/jquery-ui.js"></script>

<!-- multiple select -->
<script src="<?= ASSET_URL ?>js/multiple-select/multiple-select.min.js"></script>
<link href="<?= ASSET_URL ?>js/multiple-select/multiple-select.min.css" rel="stylesheet" type="text/css">

<!-- datetimepicker -->  	
<link href="<?= ASSET_URL ?>js/datetimepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script src="<?= ASSET_URL ?>js/datetimepicker/jquery-ui-timepicker-addon.js"></script>		
<script src="<?= ASSET_URL ?>js/datetimepicker/jquery-ui-sliderAccess.js"></script>		

<!-- TinyMCE common for both front-end/backend-->
<script src="<?= base_url() ?>assets/admin/editor/tinymce/tinymce.min.js"></script>
<!--<script src="<?= base_url() ?>assets/editor/tinymce/tinymce.js"></script>-->
<script src="<?= base_url() ?>assets/admin/editor/tinymce/tinymce.spec.js"></script>

<!-- waitMe css -->
<link href="<?= ASSET_URL; ?>js/waitMe/waitMe.min.css" type="text/css" rel="stylesheet"/>

<!-- isteven-multi-select -->
<link rel="stylesheet" href="<?= ASSET_URL; ?>js/isteven/isteven-multi-select.css">
<script src="<?= ASSET_URL; ?>js/isteven/isteven-multi-select.js"></script> 

  
<!--<link rel="stylesheet" href="<?= ASSET_URL; ?>js/intl-tel-input/intlTelInput.css">
<script src="<?= ASSET_URL; ?>js/intl-tel-input/intlTelInput.js"></script>-->
 