<meta charset="utf-8">
<title>Welcome to ECSA admin Panel</title>
<meta name="description" content="dashboard, admin, template, templates">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/admin/images/favicon.png">
<link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>assets/admin/icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>assets/admin/icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>assets/admin/icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>assets/admin/icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>assets/admin/icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>assets/admin/icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>assets/admin/icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>assets/admin/icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>assets/admin/icons/apple-icon-180x180.png">
<!--<link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>assets/admin/icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>assets/admin/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>assets/admin/icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/admin/icons/favicon-16x16.png">-->
<link rel="manifest" href="<?= base_url() ?>assets/admin/icons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?= base_url() ?>assets/admin/icons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/roboto.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/material-icons.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/font-awesome/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/flag-icon-css.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/weather-icons.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/weather-icons-wind.css" />
<!-- build:css css/vendor.css -->
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/bower_components/chartist/dist/chartist.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/bower_components/morris.js/morris.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/bower_components/nvd3/build/nv.d3.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/bower_components/sweetalert2/dist/sweetalert2.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/bower_components/toastr/toastr.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/bower_components/summernote/dist/summernote.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/components/css/zoom.css" />
<!-- endbuild -->
<!-- build:css css/styles.css -->
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/main.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/global.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/custom.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/colors.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/box-shadows.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/layouts/homepage.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/layouts/default-sidebar-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/layouts/default-sidebar-2.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/layouts/empty-view-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/layouts/empty-view-2.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/layouts/empty-view-3.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/left-sidebars/left-sidebar-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/navbars/navbar-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/right-sidebars/right-sidebar-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/dashboards/analytics.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/helpers/margin.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/helpers/padding.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/helpers/text.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/helpers/border.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/helpers/height.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/helpers/width.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/helpers/other.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/color-system/material-design-colors.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/icons/flags.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/icons/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/icons/weather-icons.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/icons/material-design-icons.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/charts/chartist.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/charts/easy-pie-chart.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/charts/morris.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/charts/nvd3.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/charts/echarts.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/extras/crop.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/extras/invoice.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/extras/mousetrap.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/extras/pricing-tables.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/extras/syntax-highlighting.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/extras/zoom.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/editors/summernote.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/jumbotron/jumbotron-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/forms/basic.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/forms/checkboxes.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/forms/radios.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/forms/sliders.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/forms/toggles.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/tables/default.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/tables/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/pages/index.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/pages/banners.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/pages/error.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/pages/sign-in.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/pages/sign-up.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/alerts.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/badges.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/breadcrumbs.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/buttons.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/cards.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/dropdowns.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/grid.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/images.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/lists.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/modals.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/overlays.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/pagination.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/popovers.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/progress-bars.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/social-media-buttons.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/sweet-alert.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/tabs.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/tags.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/tooltips.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/ui/typography.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-10.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-11.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-2.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-6.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-7.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-8.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/user-widgets/user-widget-9.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/maps/google-maps.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/maps/vector-maps.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/text-widgets/text-widget-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/text-widgets/text-widget-2.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/text-widgets/text-widget-7.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/activity-widgets/activity-widget-1.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/activity-widgets/activity-widget-3.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/activity-widgets/activity-widget-4.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/activity-widgets/activity-widget-5.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/activity-widgets/activity-widget-6.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/documentation/customization.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/documentation/code-structure.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/documentation/credits.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/documentation/layout.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/documentation/styles.css">
<!-- endbuild -->
