 <nav class="navbar-1">
        <ul class="nav nav-inline navbar-left">
            <li class="nav-item">
                <a class="nav-link toggle-layout" href="#">
                    <i class="material-icons menu">menu</i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link toggle-fullscreen" href="#">
                    <i class="material-icons">fullscreen</i>
                </a>
            </li>
        </ul>
        <span class="welcome">Hello! <?php echo $this->session->userdata('admin_username'); ?></span>
        <div class="dropdown user-dropdown">
            <a class="dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="badge badge-40">
                    <span class="tag tag-sm tag-rounded tag-warning">1</span>
                    <img src="<?=base_url()?>assets/admin/faces/user-icon.png" class="max-w-40 img-circle" alt="badge" />
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right from-right">
                <!--<a class="dropdown-item" href="#">
                    <i class="material-icons icon">email</i>
                    <span class="title">Inbox</span>
                    <span class="tag tag-pill tag-raised tag-danger tag-xs">New</span>
                </a>-->
                <a class="dropdown-item" href="#">
                    <i class="material-icons icon">settings</i>
                    <span class="title">Profile</span>
                </a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>admin/administrative/logout">
                    <i class="material-icons icon">power_settings_new</i>
                    <span class="title">Logout</span>
                </a>
            </div>
        </div>
        
    </nav>