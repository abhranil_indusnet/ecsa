

<script src="<?= ASSET_URL ?>app/directives/commonDirectives.js"></script>
<script src="<?= ASSET_URL ?>app/directives/fileModel.js"></script>
<script src="<?= ASSET_URL ?>app/services/multipartForm.js"></script>


<script src="<?= base_url() ?>assets/admin/bower_components/lodash/dist/lodash.min.js"></script>
<script src="<?= base_url() ?>assets/admin/components/scripts/modernizr.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/tether/dist/js/tether.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/jquery-storage-api/jquery.storageapi.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/moment/moment.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/d3/d3.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/peity/jquery.peity.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/mousetrap/mousetrap.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/bootstrap-material-design/dist/bootstrap-material-design.iife.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/chartist/dist/chartist.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/morris.js/morris.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/nvd3/build/nv.d3.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/echarts/dist/echarts.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/topojson/topojson.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/datamaps/dist/datamaps.all.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/jquery.countdown/dist/jquery.countdown.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="<?= base_url() ?>assets/admin/components/scripts/highlight.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/table-export/tableExport.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/table-export/jquery.base64.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/table-export/jspdf/libs/sprintf.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/table-export/jspdf/jspdf.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/table-export/jspdf/libs/base64.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/toastr/toastr.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/jquery-fullscreen/jquery.fullscreen-min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/summernote/dist/summernote.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/counter-up/jquery.counterup.min.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/velocity/velocity.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/velocity/velocity.ui.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/elevatezoom/jquery.elevatezoom.js"></script>
<script src="<?= base_url() ?>assets/admin/components/scripts/enhance.js"></script>
<script src="<?= base_url() ?>assets/admin/bower_components/particles.js/particles.min.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/jumbotron/jumbotron-3.js"></script>

<!--<script src="<?= base_url() ?>assets/admin/scripts/approvejs/approve.min.js"></script>-->
<script src="<?= base_url() ?>assets/admin/scripts/functions.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/app.js"></script>
<!--<script src="<?= base_url() ?>assets/admin/scripts/left-sidebar.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/navbar-1.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/navbar-3.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/charts/peity.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/charts/chart-js.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/charts/chartist.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/charts/morris.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/charts/nvd3.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/charts/echarts.js"></script>

<script src="<?= base_url() ?>assets/admin/scripts/maps/vector-maps.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/maps/google-maps.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/dashboards/analytics.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/pages/index.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/charts/easy-pie-chart.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/editors/summernote.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/extras/elevate-zoom.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/extras/syntax-highlighting.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/forms/validation.js"></script>-->
<script src="<?= base_url() ?>assets/admin/scripts/icons/material-design-icons.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/icons/font-awesome.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/icons/flags.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/icons/ionicons.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/icons/weather-icons.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/tables/default.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/tables/table-export.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/tables/datatable.js"></script>    

<script src="<?= base_url() ?>assets/admin/scripts/ui/counters.js"></script>

<script src="<?= base_url() ?>assets/admin/scripts/ui/notify.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/ui/tooltips.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/ui/popovers.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/ui/sweet-alert.js"></script>
<script src="<?= base_url() ?>assets/admin/scripts/ui/toastr.js"></script>


<script src="<?= base_url() ?>assets/admin/js/common.js"></script>

<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
</script>


<div class="right-sidebar-backdrop"></div>

<script type="text/javascript">
    function run_waitMe(effect, v, text) {
        text = text || "Please wait...";
        $(v).waitMe({
            effect: effect,
            text: text,
            bg: 'rgba(255,255,255,0.7)',
            color: '#000',
            maxSize: '',
            source: 'img.svg',
            onClose: function () {
            }
        });
    }

</script>
<script src="<?= ASSET_URL; ?>js/waitMe/waitMe.min.js"></script>