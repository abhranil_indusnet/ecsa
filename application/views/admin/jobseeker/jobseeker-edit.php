<script src="<?= base_url() ?>assets/admin/app/controllers/jobseekerController.js"></script>
<style type="text/css">
    .uploaded {
        padding: 0;
        height: 14px;
        border-radius: 10px;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#66cc00), to(#4b9500));
        border-image: initial;
    }
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>

<div class="jumbotron-1">
    <div class="jumbotron jumbotron-fluid">
        <div class="container-fluid">       
            <h1 class="display-3">User Management </h1>         
            <ol class="breadcrumb icon-home icon-angle-right no-bg">
                <li><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>admin/jobseeker">Jobseekers</a></li>
                <li><?php //echo $employerdata['employerCompanyName'];     ?> Edit</li>
            </ol>
        </div>
    </div>
</div>
<div class="col-xs-12 main" ng-cloak>
    <div class="page-on-top">
        <div class="row">
            <div class="col-xs-12">
                <div class="widget">
                    <div class="row m-b-20">
                        <div class="col-xs-12">
                            <h4 id="modal-lg-primary-label">Jobseeker</h4>
                            <!-- <p class="color-grey-900 m-b-5">Jobseeker</p> -->
                            <div id="messages"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="nav-tabs-vertical">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link active" ng-click="activeTab('personal')" data-toggle="tab" data-target="#vertical-tabs-1-1">Personal Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-2">Work Experience</a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-3">Portfolio</a>
                                    </li>-->
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-4">Education Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-5" >Key Skill</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-6" >Certification</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-7" >Achievements</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-8" >Languages Known</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" ng-click="activeTab('hobbies')" data-target="#vertical-tabs-1-9" >Hobbies</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="tab" data-target="#vertical-tabs-1-10" >Resume</a>
                                    </li>
                                </ul>
                                <div class="tab-content" style="width:100%">
                                    <div role="tabpanel" class="tab-pane active" id="vertical-tabs-1-1">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <img ng-src="<?php echo base_url() ?>uploads/<?php echo ABSPATH_JOBSEKER_PROFILE ?>{{tempUserData.jobseekerImage}}" width="100px" height="100px" err-src="{{imgpath + 'no-image-found.jpg'}}" >

                                                    <input type="file" class="filestyle" data-iconName="glyphicon glyphicon-inbox" onchange="angular.element(this).scope().setFiles(this)"/>
                                                </div>

                                                <div class="col-md-8">
                                                    <div ng-show="progressVisible">
                                                        <div class="percent-bar">{{progress}}%</div>
                                                        <div class="progress-bar">
                                                            <div class="uploaded" ng-style="{'width': progress + '%'}"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                               
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <form method="post" name="personalForm" class="form" id="personalForm" ng-submit="editSeeker(personalForm.$valid, 'personal')" novalidate ng-init="editUserdata('<?php echo $jobseekerdata['userId']; ?>');tempUserData.formName = 'personal'">

                                                 

                                                    <div class="col-md-12"> 
                                                        <div class="form-group">
                                                            <label>First Name<span class="red">*</span></label>
                                                            <input type="text" class="form-control" name="jobseekerFirstName" id="jobseekerFirstName" ng-model="tempUserData.jobseekerFirstName"  required>
                                                            <small class="text-danger" ng-show="submitted && personalForm.jobseekerFirstName.$error.required || personalForm.jobseekerFirstName.$touched && personalForm.jobseekerFirstName.$invalid">Please enter First name</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Last Name<span class="red">*</span></label>
                                                            <input type="text" class="form-control" name="jobseekerLastName" id="jobseekerLastName" ng-model="tempUserData.jobseekerLastName"   required >
                                                            <small class="text-danger" ng-show="submitted && personalForm.jobseekerLastName.$error.required || personalForm.jobseekerLastName.$touched && personalForm.jobseekerLastName.$invalid">Please enter Last name</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Email<span class="red">*</span></label>
                                                            <input type="email" class="form-control" name="jobseekerUsername" id="jobseekerUsername" ng-model="tempUserData.userEmail" required >
                                                            <small class="text-danger" ng-show="submitted && personalForm.jobseekerUsername.$error.required || personalForm.jobseekerUsername.$touched && personalForm.jobseekerUsername.$invalid">Please enter Email</small>
                                                        </div>


             <div class="form-group">
             <label>Nationality<span class="red">*</span></label>

             <select class="form-control" name="jobseekerNationality" id="jobseekerNationality"  ng-model="tempUserData.jobseekerNationality" required>
             <option value="">Select One</option>
             <option ng-repeat="option in getCountryList" value="{{option.id}}" ng-selected="tempUserData.jobseekerNationality==option.id">{{option.name}}</option>
             </select>  
             <small class="text-danger" ng-show="submitted && personalForm.jobseekerNationality.$error.required || personalForm.jobseekerNationality.$touched && personalForm.jobseekerNationality.$invalid">Please select Current Country of Residence</small>
              <p>(This is the country for which you hold a passport)</p>
             </div> 

             <div class="form-group">
             <label>Current Country of Residence<span class="red">*</span></label>
             <select class="form-control" name="jobseekerCurrentCountry" id="jobseekerCurrentCountry"  ng-model="tempUserData.jobseekerCurrentCountry" required  ng-change="changeCountry(tempUserData.jobseekerCurrentCountry);">
             <option value="">Select One</option>
             <option ng-repeat="option in getCountryList" value="{{option.id}}" ng-selected="tempUserData.jobseekerCurrentCountry==option.id">{{option.name}}</option>
             </select>  
             <small class="text-danger" ng-show="submitted && personalForm.jobseekerCurrentCountry.$error.required || personalForm.jobseekerCurrentCountry.$touched && personalForm.jobseekerCurrentCountry.$invalid">Please select Current Country of Residence</small>
             </div> 

              <div class="form-group">
              <label>Select City<span class="red">*</span></label>
              <select class="form-control" name="jobseekerCity" ng-model="tempUserData.jobseekerCity" required>
              <option ng-repeat="option in getCityList" value="{{option.id}}"  ng-selected="tempUserData.jobseekerCity === option.id">{{option.name}}</option>
              </select> 
              <small class="text-danger" ng-show="submitted && personalForm.jobseekerCity.$error.required || personalForm.jobseekerCity.$touched && personalForm.jobseekerCity.$invalid">Please select a city.</small>
              </div> 

                                                        <div class="form-group">
                                                            <label>Current Location<span class="red">*</span></label>
                                                            <input type="text" class="form-control" name="jobseekerCurrentLocation" id="jobseekerCurrentLocation" ng-model="tempUserData.jobseekerCurrentLocation" required >

                                                            <small class="text-danger" ng-show="submitted && personalForm.jobseekerCurrentLocation.$error.required || personalForm.jobseekerCurrentLocation.$touched && personalForm.jobseekerCurrentLocation.$invalid">Please enter Current Location</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Prefered Location<span class="red">*</span></label>
                                                            <input type="text" class="form-control" name="jobseekerPreferedLocation" id="jobseekerPreferedLocation" ng-model="tempUserData.jobseekerPreferedLocation" required>

                                                            <small class="text-danger" ng-show="submitted && personalForm.jobseekerPreferedLocation.$error.required || personalForm.jobseekerPreferedLocation.$touched && personalForm.jobseekerPreferedLocation.$invalid">Please enter Current Location</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Date of Birth<span class="red">*</span></label>
                                                            <input class="form-control" type="text" name="dob" id="dob" jqdatepicker  return-dob-date="selectDob(dob)" ng-model="tempUserData.jobseekerDob" readonly/>
                                                            <small class="text-danger" ng-show="personalForm.dob.$invalid && personalForm.dob.$touched">Please Select Date of Birth</small>
                                                        </div>

                                                        <div class="form-group">
                                                            <label> Gender <span class="red">*</span></label>
                                                            <select class="form-control" name="proficiency" id="proficiency" ng-model="tempUserData.jobseekerGender" required>
                                                                <option value="">Select one</option>
                                                                <option ng-repeat="(key,option) in getGenderList" value="{{key}}" ng-selected="tempUserData.jobseekerGender == key">{{option}}</option>
                                                            </select>
                                                            <small class="text-danger" ng-show="submitted && personalForm.proficiency.$error.required || personalForm.proficiency.$touched && personalForm.proficiency.$invalid">Please select Gender</small>
                                                        </div>
                <div class="form-group">
                <label> Marital Status <span class="red">*</span></label>
                <select class="form-control" name="jobseekerMaritalStatus" id="jobseekerMaritalStatus" ng-model="tempUserData.jobseekerMaritalStatus" required>
                <option value="">Select one</option>
                <option ng-repeat="(key,option) in getMaritalStatusList" value="{{key}}" ng-selected="tempUserData.jobseekerMaritalStatus == key">{{option}}</option>
                </select>
                <small class="text-danger" ng-show="submitted && personalForm.jobseekerMaritalStatus.$error.required || personalForm.jobseekerMaritalStatus.$touched && personalForm.jobseekerMaritalStatus.$invalid">Please select Marital Status</small>
                </div>
                                        


                                                        <div class="form-group">
                                                            <label> Total Experiences </label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select class="form-control" name="jobseekerExperienceYear" id="jobseekerExperienceYear" ng-model="tempUserData.jobseekerExperienceYear">
                                                                        <option value="">Select Years</option>
                                                                        <?php for ($i = 1; $i < 41; $i++) { ?>
                                                                            <option value="<?php echo $i; ?>">
                                                                                <?php echo $i; ?>
                                                                            </option>
                                                                        <?php } ?>
                                                                    </select>                                       
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <select class="form-control" name="jobseekerExperienceMonth" id="jobseekerExperienceMonth" ng-model="tempUserData.jobseekerExperienceMonth">
                                                                        <option value="">Select Months</option>
                                                                        <?php for ($i = 1; $i < 12; $i++) { ?>
                                                                            <option value="<?php echo $i; ?>">
                                                                                <?php echo $i; ?>
                                                                            </option>
                                                                        <?php } ?>
                                                                    </select>                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Notice Period </label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select class="form-control" name="jobseekerNoticePeriod" id="jobseekerNoticePeriod" ng-model="tempUserData.jobseekerNoticePeriod">
                                                                        <option value="">Select Days</option>
                                                                        <?php for ($i = 1; $i < 93; $i++) { ?>
                                                                            <option value="<?php echo $i; ?>">
                                                                                <?php echo $i; ?>
                                                                            </option>
                                                                        <?php } ?>
                                                                    </select>                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Notice Period Negotiable</label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select class="form-control" name="jobseekerNoticePeriodNeg" id="jobseekerNoticePeriodNeg" ng-model="tempUserData.jobseekerNoticePeriodNeg">
                                                                        <option value="">Select</option>
                                                                        <option value="1">YES</option>
                                                                        <option value="0">NO</option> 
                                                                    </select>                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Current Serving Notice Period</label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select class="form-control" name="jobseekerNoticePeriodSer" id="jobseekerNoticePeriodSer" ng-model="tempUserData.jobseekerNoticePeriodSer">
                                                                        <option value="">Select</option>
                                                                        <option value="1">YES</option>
                                                                        <option value="0">NO</option> 
                                                                    </select>                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Industry</label>
                                                            <multiple-autocomplete ng-model="tempUserData.jobseekerPreferedIndustry" object-property="industryName"  suggestions-arr="getIndustryList">
                                                            </multiple-autocomplete>

                                                        </div>

                                                        <div class="form-group">
                                                            <label>Role</label>
                                                            <multiple-autocomplete ng-model="tempUserData.jobseekerPreferedRole" object-property="roleName" suggestions-arr="getAllRoleList">
                                                            </multiple-autocomplete>

                                                        </div>
                    <div class="form-group ">
                        <label>Mobile Number Code<span class="red">*</span></label>
                        <input type="text" class="form-control" name="contactCode" id="contactCode"  ng-model="tempUserData.jobseekerMobileCode" placeholder="+ 966"  required>
                        <small class="text-danger" ng-show="submitted && personalForm.contactCode.$error.required || personalForm.contactCode.$touched && personalForm.contactCode.$invalid">Please Enter the Mobile Number Code</small>
                       
                         </div>
                                                        <div class="form-group">
                                                            <label>Mobile Number<span class="red">*</span></label>
                                                            <input type="text" class="form-control" name="jobseekerMobile" id="jobseekerMobile" ng-model="tempUserData.jobseekerMobile" number-only  required>

                                                          
                                                             <small class="text-danger" ng-show="submitted && personalForm.contactno.$error.required || personalForm.contactno.$touched && personalForm.contactno.$invalid">Please Enter the Mobile Number</small>


                                                        </div>

                        <div class="form-group">
                        <label>Skype Id<span class="red"></span></label>
                        <input type="text" class="form-control" name="jobseekerSkypeId" id="jobseekerSkypeId" ng-model="tempUserData.jobseekerSkypeId">
                       </div>

                        <div class="form-group">
                        <label>LinkedIn Profile URL<span class="red"></span></label>
                        <input type="url" class="form-control" name="jobseekerLinkedInprofile" id="jobseekerLinkedInprofile" ng-model="tempUserData.jobseekerLinkedInprofile">
                      
                        <small class="text-danger" ng-show="submitted && personalForm.jobseekerLinkedInprofile.$touched && personalForm.jobseekerLinkedInprofile.$error.url">Please Enter valid LinkedIn Profile URL</small>
                        </div>

                        <div class="form-group">
                        <label>ECSA.com Reference<span class="red">*</span></label><br clear="all">
                        <select class="form-control" name="jobseekerReferenceId" id="jobseekerReferenceId" ng-model="tempUserData.jobseekerReferenceId" required >

                        <option value="">Select one</option>
                        <option ng-repeat="option in getRefernceList" value="{{option.id}}" ng-selected="tempUserData.jobseekerReferenceId == option.id">{{option.source}}</option>

                        </select>
                        <small class="text-danger" ng-show="submitted && personalForm.jobseekerReferenceId.$error.required || personalForm.jobseekerReferenceId.$touched && personalForm.jobseekerReferenceId.$invalid">Please Select ECSA.com Reference</small>                
                        </div>

                        <div class="form-group">

                        <input type="checkbox" name="jobseekerSubscribeEmail" id="jobseekerSubscribeEmail" ng-model="tempUserData.jobseekerSubscribeEmail"  ng-checked="tempUserData.jobseekerSubscribeEmail"> Subscribe to weekely emails
                         </div>
                      





                                                        <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">save</button>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-2">

                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8"><h4>Work Experience</h4></div>
                                                <div class="col-md-4 pull-right">
                                                    <a href="#" class="btn btn-raised btn-flat-primary btn-icon">
                                                        <span class="btn-text" data-toggle="modal" data-target="#modal-lg-primary-addWorkExp" id="addmodal">Add Work Experience</span>
                                                    </a>
                                                    <br clear="all">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" ng-repeat="(key, item) in SeekerTotalWorkExp" id="divID">

                                                    <form method="post" name="seekerWorkExpFormEdit" class="form" ng-submit="addeditWorkExp(seekerWorkExpFormEdit.$valid, 'edit',item)" novalidate>

                                                        

                                                        <div class="col-md-12"> 
                                                            <div class="form-group">
                                                                <label>Designation<span class="red">*</span></label>
                                                                <input type="text" class="form-control" name="designation" id="designation" ng-model="item.designation" required>

                                                                <small class="text-danger" ng-show="submitted && seekerWorkExpFormEdit.designation.$error.required || seekerWorkExpFormEdit.designation.$touched && seekerWorkExpFormEdit.designation.$invalid">Please enter Designation</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Company Name<span class="red">*</span></label>
                                                                <input type="text" class="form-control" name="company" id="company" ng-model="item.company" required>
                                                                <small class="text-danger" ng-show="submitted && seekerWorkExpFormEdit.company.$error.required || seekerWorkExpFormEdit.company.$touched && seekerWorkExpFormEdit.company.$invalid">Please enter Company Name</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Select Industry<span class="red">*</span></label><br clear="all">
                                                                <select class="form-control" name="industry" id="industry" ng-model="item.industry" required>
                                                                    <option ng-repeat="option in getIndustryList" value="{{option.id}}" ng-selected="item.industry == option.id">{{option.industryName}}</option>
                                                                </select>
                                                                <small class="text-danger" ng-show="submitted && seekerWorkExpFormEdit.industry.$error.required || seekerWorkExpFormEdit.industry.$touched && seekerWorkExpFormEdit.industry.$invalid">Please Select Industry</small>                
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Select Function<span class="red">*</span></label>
                                                                <select class="form-control jobfunctionDrop" name="job_function" id="job_function_edit{{item.id}}" ng-model="item.job_function" ng-change="changeRole(item.job_function, key);" required>
                                                                    <option ng-repeat="option in getFunctionList" value="{{option.id}}" ng-selected="item.job_function == option.id">{{option.functionName}}</option>
                                                                </select> 
                                                                <small class="text-danger" ng-show="submitted && seekerWorkExpFormEdit.job_function.$error.required || seekerWorkExpFormEdit.job_function.$touched && seekerWorkExpFormEdit.job_function.$invalid">Please Select Function</small>                
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Select Role<span class="red">*</span> </label>
                                                                <select class="form-control" name="job_role" id="job_role_edit{{item.id}}" ng-model="item.job_role" required>
                                                                    <option ng-repeat="option in getRoleList[key]" value="{{option.id}}" ng-selected="item.job_role == option.id">{{option.roleName}}</option>
                                                                </select>
                                                                <small class="text-danger" ng-show="submitted && seekerWorkExpFormEdit.job_role.$error.required || seekerWorkExpFormEdit.job_role.$touched && seekerWorkExpFormEdit.job_role.$invalid">Please Select Function</small>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>From</label>
                                                                <input class="form-control" type="text"  name="fromdate"  jqdatepicker  return-dob-date="selectFrom(dob)" ng-model="item.job_from" readonly=""/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>To</label>
                                                                <input class="form-control" type="text"  name="todate"  jqdatepicker  return-dob-date="selectTo(dob)" ng-model="item.job_to" readonly=""/>
                                                            </div>

                                                            <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">save</button>
                                                            <button type="button" id="btnmode" class="btn btn-danger btn-secondary btn-flat" ng-click="deleteWorkExp(item.id, item.user_id)">Delete</button>

                                                        </div>                                                   
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-3">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8">Portfolio</div>
                                                <div class="col-md-4 pull-right">
                                                    <a href="#" class="btn btn-raised btn-flat-primary btn-icon">
                                                        <span class="btn-text" data-toggle="modal" data-target="#modal-lg-primary-addPortfolio" id="addmodal">Add Portfolio</span>
                                                    </a>
                                                    <br clear="all">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" ng-repeat="(key, item) in SeekerTotalPortfolio" id="divID">
                                                    <ng-form method="post" name="seekerEducationDetailFormEdittest{{item.id}}" class="form" id="seekerPortfolioFormEdit">
                                                        <input type="text" class="form-control" id="user_id" name="user_id"  ng-model="item.user_id"  ng-show="showUserId">
                                                        <input type="text" class="form-control" id="id" name="id"  ng-model="item.id"  ng-show="showUserId">
                                                        <div class="col-md-12"> 
                                                            <div class="form-group">
                                                                <label>Title</label>
                                                                <input type="text" class="form-control" name="title" id="title" ng-model="item.title">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Client</label>
                                                                <input type="text" class="form-control" name="client" id="client" ng-model="item.client">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>Project Url</label>
                                                                <input type="text" class="form-control" name="projectUrl" id="projectUrl" ng-model="item.projectUrl">

                                                            </div>

                                                            <div class="form-group">
                                                                <label>Duration</label><br>
                                                                <div class="row">
                                                                    <div class="col-md-2">From</div>
                                                                    <div class="col-md-5">
                                                                        <select class="form-control" name="monthnofrom" id="monthnofrom" ng-model="item.monthnofrom" required><option value="">Select Month</option><option value="January">January</option><option value="Febraury">Febraury</option><option value="March">March</option><option value="April">April</option><option value="May">May</option><option value="June">June</option><option value="July">July</option><option value="August">August</option><option value="September">September</option><option value="October">October</option><option value="November">November</option><option value="December">December</option></select>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <select class="form-control" name="yearnofrom" id="yearnofrom" ng-model="item.yearnofrom" required><option value="">Select Year</option><?php for ($i = 1970; $i < 2006; $i++) { ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?></select>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-2">To</div>
                                                                    <div class="col-md-5">
                                                                        <select class="form-control" name="monthnoto" id="monthnoto" ng-model="item.monthnoto" required><option value="">Select Month</option><option value="January">January</option><option value="Febraury">Febraury</option><option value="March">March</option><option value="April">April</option><option value="May">May</option><option value="June">June</option><option value="July">July</option><option value="August">August</option><option value="September">September</option><option value="October">October</option><option value="November">November</option><option value="December">December</option></select>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <select class="form-control" name="yearnoto" id="yearnoto" ng-model="item.yearnoto" required><option value="">Select Year</option><?php for ($i = 1970; $i < 2006; $i++) { ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Role</label>
                                                                <input type="text" class="form-control" name="role" id="role" ng-model="item.role">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>Project Details</label>
                                                                <input type="text" class="form-control" name="projectDetail" id="projectDetail" ng-model="item.projectDetail">

                                                            </div>

                                                            <button type="button" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="addeditPortfolio(item, 'edit')">save</button>
                                                            <button type="button" id="btnmode" class="btn btn-danger btn-secondary btn-flat" ng-click="deletePortfolio(item.id, item.user_id)">Delete</button>

                                                        </div>                                                   
                                                    </ng-form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-4">

                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8"><h4>Education Details</h4></div>
                                                <div class="col-md-4 pull-right">
                                                    <a href="#" class="btn btn-raised btn-flat-primary btn-icon">
                                                        <span class="btn-text" data-toggle="modal" data-target="#modal-lg-primary-addEduDetail" id="addmodal">Add Education Detail</span>
                                                    </a>
                                                    <br clear="all">
                                                </div>
                                            </div>
                                            <div class="row">
                                              

                                                  
                                                <div class="col-md-12" ng-repeat="(key, item) in SeekerTotaleducationDetail">
                                                    <form method="post" name="seekerEducationDetailFormEdit" class="form"  ng-submit="addeditEduDetail(seekerEducationDetailFormEdit.$valid, 'edit',item)" novalidate>
                                                    
                                                        <!--<input type="text" class="form-control" id="user_id" name="user_id"  ng-model="item.user_id"  ng-show="showUserId">

                                                        <input type="text" class="form-control" id="id" name="id"  ng-model="item.id"  ng-show="showUserId">-->

                                                        <div class="col-md-12"> 
                                                            <div class="form-group">
                                                                <label>Qualification<span class="red">*</span></label>
                                                                <select class="form-control" name="qualification" id="qualification" ng-model="item.qualification" ng-change="changeSpec(item.qualification, key);" required>
                                                                    <option ng-repeat="option in getqualificationList" value="{{option.id}}" ng-selected="item.qualification == option.id">{{option.educationLevelName}}</option>
                                                                </select> 
                                                                <small class="text-danger" ng-show="submitted && seekerEducationDetailFormEdit.qualification.$error.required 
                                                                || seekerEducationDetailFormEdit.qualification.$touched && seekerEducationDetailFormEdit.qualification.$invalid">Please Select Qualification</small>                
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Course Type<span class="red">*</span></label>
                                                                <select class="form-control" name="courseType" id="courseType" ng-model="item.courseType" required>
                                                                    <option value="1">Full Time</option>
                                                                    <option value="2">Part Time</option>
                                                                </select>
                                                                <small class="text-danger" ng-show="submitted && seekerEducationDetailFormEdit.courseType.$error.required || seekerEducationDetailFormEdit.courseType.$touched && seekerEducationDetailFormEdit.courseType.$invalid">Please Select Course Type</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Specialization<span class="red">*</span></label>
                                                                <select class="form-control" name="specialization" id="specialization" ng-model="item.specialization" required>
                                                                    <option ng-repeat="option in getSpecializationListEdit[key]" value="{{option.id}}" ng-selected="item.specialization == option.id">{{option.educationLevelName}}</option>
                                                                </select>
                                                                <small class="text-danger" ng-show="submitted && seekerEducationDetailFormEdit.specialization.$error.required || seekerEducationDetailFormEdit.specialization.$touched && seekerEducationDetailFormEdit.specialization.$invalid">Please Select Specialization</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Institute<span class="red">*</span></label>
                                                                <input type="text" class="form-control" name="institute" id="institute" ng-model="item.institute" required>
                                                                <small class="text-danger" ng-show="submitted && seekerEducationDetailFormEdit.institute.$error.required 
                                                                || seekerEducationDetailFormEdit.institute.$touched && seekerEducationDetailFormEdit.institute.$invalid">Please Enter Institute</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Study<span class="red">*</span></label>
                                                                <input type="text" class="form-control" name="study" id="study" ng-model="item.study" required>
                                                                <small class="text-danger" ng-show="submitted && 
                                                                seekerEducationDetailFormEdit.study.$error.required || 
                                                                seekerEducationDetailFormEdit.study.$touched && 
                                                                seekerEducationDetailFormEdit.study.$invalid">Please Enter Study</small>

                                                            </div>
                                                            <div class="form-group">
                                                                <label>Year Of Passing<span class="red">*</span></label>
                                                                <select class="form-control" name="yearOfPass" id="yearOfPass" ng-model="item.yearOfPass" required>
                                                                    <option value="">Select Year</option>
                                                                    <?php for ($i = 1970; $i < 2016; $i++) { ?>
                                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <small class="text-danger" ng-show="submitted && seekerEducationDetailFormEdit.yearOfPass.$error.required
                                                                 || seekerEducationDetailFormEdit.yearOfPass.$touched && 
                                                                 seekerEducationDetailFormEdit.yearOfPass.$invalid">Please Select Year</small>
                                                             
                                                               
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Percentage Scored<span class="red">*</span></label>
                                                                <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" class="form-control" name="scored" id="scored" ng-model="item.scored" required>
                                                                <small class="text-danger" ng-show="submitted && seekerEducationDetailFormEdit.scored.$error.required 
                                                                || seekerEducationDetailFormEdit.scored.$touched && seekerEducationDetailFormEdit.scored.$invalid && !seekerEducationDetailFormEdit.scored.$error.pattern">
                                                                Please Enter Percentage Scored</small>
                                                                <small class="text-danger" ng-show="seekerEducationDetailFormEdit.scored.$touched 
                                                                && seekerEducationDetailFormEdit.scored.$error.pattern">Please enter Valid Percentage Scored</small>
                                                                 <small class="form-text text-muted">upto 2 decimal places</small>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Location<span class="red">*</span></label>
                                                                <input type="text" class="form-control" name="location" id="location" ng-model="item.location" required>
                                                                <small class="text-danger" ng-show="submitted && seekerEducationDetailFormEdit.location.$error.required || seekerEducationDetailFormEdit.location.$touched && seekerEducationDetailFormEdit.location.$invalid">Please Enter Location</small>
                                                            </div> 

                                                            <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">save</button>
                                                            <button type="button" id="btnmode" class="btn btn-danger btn-secondary btn-flat" ng-click="deleteEduDetail(item.id, item.user_id)">Delete</button>

                                                        </div>                                                   
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-5">
                                        <h4>Skill</h4>
                                        <form method="post" name="seekerSkillForm" class="form" id="seekerSkillForm" ng-submit="addeditSkill(seekerSkillForm.$valid, 'add', '')" novalidate>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Skill<span class="red">*</span></label>
                                                        <input type="text" class="form-control" id="userid" name="userid" ng-model="tempUserSkillAdd.userid" ng-show="showUserId" required>

                                                        <select class="form-control" name="skill" id="skill" ng-model="tempUserSkillAdd.skill" required>
                                                            <option ng-repeat="option in getskillList" value="{{option.id}}" ng-selected="tempUserSkillAdd.skill == option.id">{{option.skillTitle}}</option>
                                                        </select>
                                                        <small class="text-danger" ng-show="submitted && seekerSkillForm.skill.$error.required || seekerSkillForm.skill.$touched && seekerSkillForm.skill.$invalid">Please Select Skill</small>                                                    
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Experience<span class="red">*</span></label>
                                                        <input type="text" class="form-control" placeholder="Experience" name="experience" id="experience" ng-model="tempUserSkillAdd.experience" required>
                                                        <small class="text-danger" ng-show="submitted && seekerSkillForm.experience.$error.required || seekerSkillForm.experience.$touched && seekerSkillForm.experience.$invalid">Please Enter Experience</small>
                                                    </div> 
                                                </div> 
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>     

                                        <br clear="all">
                                        <div class="row">
                                            <div class="table-responsive">
                                                <table  class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Skill</th>
                                                            <th>Applied</th>
                                                            <th>Studied</th>
                                                            <th>Experience</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="item in SeekerTotalSkill">
                                                            <th scope="row">{{item.skill}}</th>
                                                            <td>   
                                                                <input type="checkbox" ng-model="item.applied" ng-true-value="'1'" ng-false-value="'0'" ng-click="changeSkillapplied(item.applied, item.id)">
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" ng-model="item.studied" ng-true-value="'1'" ng-false-value="'0'" ng-click="changeSkillstudied(item.studied, item.id)">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control" placeholder="Experience" name="experience" id="experience" ng-model="item.experience" ng-blur="changeExperience(item.experience, item.id)">
                                                            </td>
                                                            <td><a href="javascript:void(0);" class="fa fa-x fa-trash" ng-click="deleteJobSeekerSkill(item.id, item.user_id)"></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-6">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8"><h4>Certification</h4></div>
                                                <div class="col-md-4 pull-right">
                                                    <a href="#" class="btn btn-raised btn-flat-primary btn-icon">
                                                        <span class="btn-text" data-toggle="modal" data-target="#modal-lg-primary-addCertification" id="addmodal">Add Certification</span>
                                                    </a>
                                                    <br clear="all">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" ng-repeat="(key, item) in SeekerTotalCertification">
                                                    <form method="post" name="seekerCertificationFormEdit" class="form" ng-submit="addeditCertification(seekerCertificationFormEdit.$valid,'edit',item)" novalidate>
                                                        
                                                        <div class="col-md-12"> 
                                                            <div class="form-group">
                                                                <label>Certification<span class="red">*</span></label>
                                                                <input type="text" class="form-control" name="certification" id="certification" ng-model="item.certification" required>
                                                                <small class="text-danger" ng-show="submitted && seekerCertificationFormEdit.certification.$error.required || seekerCertificationFormEdit.certification.$touched && seekerCertificationFormEdit.certification.$invalid">Please Enter Certification</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Certification Year<span class="red">*</span></label>
                                                                <select class="form-control" name="certifyYear" id="certifyYear" ng-model="item.certifyYear" required>
                                                                    <option value="">Select Year</option>
                                                                    <?php for ($i = 1970; $i < 2006; $i++) { ?>
                                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <small class="text-danger" ng-show="submitted && seekerCertificationFormEdit.certifyYear.$error.required || seekerCertificationFormEdit.certifyYear.$touched && seekerCertificationFormEdit.certifyYear.$invalid">Please Select Certification Year</small>
                                                            </div>                                                           
                                                            <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">save</button>
                                                            <button type="button" id="btnmode" class="btn btn-danger btn-secondary btn-flat" ng-click="deleteCertification(item.id, item.user_id)">Delete</button>

                                                        </div>                                                   
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-7">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8"><h4>Achievements</h4></div>
                                                <div class="col-md-4 pull-right">
                                                    <a href="#" class="btn btn-raised btn-flat-primary btn-icon">
                                                        <span class="btn-text" data-toggle="modal" data-target="#modal-lg-primary-addAchievement" id="addmodal">Add Achievements</span>
                                                    </a>
                                                    <br clear="all">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" ng-repeat="(key, item) in SeekerTotalAchievement">
                                                    <form method="post" name="seekerAchievementFormEdit" class="form" ng-submit="addeditAchievement(seekerAchievementFormEdit.$valid, 'edit',item)" novalidate>

                                                        <input type="text" class="form-control" id="user_id" name="user_id"  ng-model="item.user_id"  ng-show="showUserId">
                                                        <input type="text" class="form-control" id="id" name="id"  ng-model="item.id"  ng-show="showUserId">

                                                        <div class="col-md-12"> 
                                                            <div class="form-group">
                                                                <label>Achievements<span class="red">*</span></label>
                                                                <input type="text" class="form-control" name="achievement" id="achievement" ng-model="item.achievement" required>
                                                                <small class="text-danger" ng-show="submitted && seekerAchievementFormEdit.achievement.$error.required || seekerAchievementFormEdit.achievement.$touched && seekerAchievementFormEdit.achievement.$invalid">Please Enter Achievements</small>
                                                            </div>
                                                            <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">save</button>
                                                            <button type="button" id="btnmode" class="btn btn-danger btn-secondary btn-flat" ng-click="deleteAchievement(item.id, item.user_id)">Delete</button>

                                                        </div>                                                   
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-8">
                                        <h4>Language Known</h4>

                                        <form method="post" name="seekerLanguageForm" class="form" ng-submit="addeditLanguage(seekerLanguageForm.$valid, 'add')" novalidate>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label> Language<span class="red">*</span> </label>
                                                        <input type="text" class="form-control" id="userid" name="userid" ng-model="tempUserLangAdd.userid" ng-show="showUserId">
                                                        <select class="form-control" name="language" id="language" ng-model="tempUserLangAdd.languageId"  required>
                                                            <option ng-repeat="option in getLanguageList" value="{{option.id}}" ng-selected="tempUserLangAdd.languageId == option.id">{{option.name}}</option>
                                                        </select>
                                                        <small class="text-danger" ng-show="submitted && seekerLanguageForm.language.$error.required || seekerLanguageForm.language.$touched && seekerLanguageForm.language.$invalid">Please Enter Language</small>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label> Proficiency<span class="red">*</span> </label>
                                                        <select class="form-control" name="proficiency" id="proficiency" ng-model="tempUserLangAdd.proficiency" required>
                                                            <option ng-repeat="(key,option) in getProficiencyList" value="{{key}}" ng-selected="tempUserLangAdd.proficiency == key">{{option}}</option>
                                                        </select>
                                                        <small class="text-danger" ng-show="submitted && seekerLanguageForm.proficiency.$error.required || seekerLanguageForm.proficiency.$touched && seekerLanguageForm.proficiency.$invalid">Please Select Proficiency</small>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted = true">Add</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>     

                                        <br clear="all">
                                        <div class="row">
                                            <div class="table-responsive">
                                                <table  class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Language</th>
                                                            <th>Read</th>
                                                            <th>write</th>
                                                            <th>Speak</th>
                                                            <th>Proficiency</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="item in SeekerTotalLanguage">
                                                            <th scope="row">{{item.languageId}}</th>
                                                            <td>   
                                                                <input type="checkbox" ng-model="item.languageRead" ng-true-value="'1'" ng-false-value="'0'" ng-click="changeSkillLanguageRead(item.languageRead, item.id)">
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" ng-model="item.languageWrite" ng-true-value="'1'" ng-false-value="'0'" ng-click="changeSkillLanguageWrite(item.languageWrite, item.id)">
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" ng-model="item.languageSpeak" ng-true-value="'1'" ng-false-value="'0'" ng-click="changeSkillLanguageSpeak(item.languageSpeak, item.id)">
                                                            </td>
                                                            <td>{{item.proficiency}}</td>
                                                            <td><a href="javascript:void(0);" class="fa fa-x fa-trash" ng-click="deleteJobSeekerLanguage(item.id, item.user_id)"></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-9">
                                        <form method="post" name="userHobbiesForm" class="form" id="userForm" ng-submit="editSeeker(userHobbiesForm, 'hobbies')" novalidate>
                                           
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">Hobbies</div>
                                                    <div class="col-md-12">

                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="hobbies" id="hobbies" ng-model="temphobbies.hobbies">
                                                        </div>


                                                        <button type="submit" id="btnmode" class="btn btn-raised btn-secondary btn-flat" >save</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="vertical-tabs-1-10">
                                        <form method="post" name="userForm" class="form" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Upload Resume</label>
                                                <p>Microsoft word, doc, docx or PDF only (Max 4MB)</p>
                                                <p></p>
                                                <!--<input data-edit-Resume return-file-name="afterSubmitOnEdit(resumeupload)" type="file" name="resumeupload" jobseeker-id="<?php echo $jobseekerdata['id']; ?>">-->

                                                 <input type="file" class="filestyle" data-iconName="glyphicon glyphicon-inbox"  onchange="angular.element(this).scope().setResume(this)"/>
                                                 
                                            </div>
                                            <div class="form-group" ng-if="editUserdataStatus == true">
                                                <label>Latest Resume</label>
                                                : {{tempUserData.jobseekerResumeFile}}
                                            </div>
                                             <div class="form-group" ng-if="editUserdataStatus == true">
                                                <label>download Resume</label>
                                                <a href="<?php echo base_url()?>uploads/jobseeker/{{tempUserData.jobseekerResume}}" target="_blank">Click here</a></div>
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Model section -->
<!-- Work Experience section (start)-->
<div class="modal modal-primary fade scale" id="modal-lg-primary-addWorkExp" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary-addWorkExp" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">

        <form method="post" name="seekerWorkExpForm" class="form" enctype="multipart/form-data" ng-submit="addeditWorkExp(seekerWorkExpForm.$valid, 'add')" novalidate>
           
            <input type="text" class="form-control" id="userid" name="userid" ng-model="tempSeekerWorkExp.userid" ng-init="tempSeekerWorkExp.userid = '<?php echo $jobseekerdata['id']; ?>'" value="<?= isset($jobseekerdata['id']) ? $jobseekerdata['id'] : '' ?>" ng-show="showUserId">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Jobseeker</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <div class="form-group">
                        <span class="text-muted">
                            <em>
                                <span style="color:red;">*</span> Indicates required field
                            </em>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Designation<span class="red">*</span></label>
                        <input type="text" class="form-control" name="designation" id="designation" ng-model="tempSeekerWorkExp.designation" required>
                        <small class="text-danger" ng-show="submitted && seekerWorkExpForm.designation.$error.required || seekerWorkExpForm.designation.$touched && seekerWorkExpForm.designation.$invalid">Please enter Designation</small>
                    </div>
                    <div class="form-group">
                        <label>Company<span class="red">*</span></label>
                        <input type="text" class="form-control" name="company" id="company" ng-model="tempSeekerWorkExp.company" required>
                        <small class="text-danger" ng-show="submitted && seekerWorkExpForm.company.$error.required || seekerWorkExpForm.company.$touched && seekerWorkExpForm.company.$invalid">Please enter Company</small>
                    </div>
                    <div class="form-group">
                        <label>Select Industry<span class="red">*</span></label><br clear="all">
                        <select class="form-control" name="industry" id="industry" ng-model="tempSeekerWorkExp.industry" required>
                            <?php
                            if (isset($getIndustryList) && !empty($getIndustryList)) {
                                foreach ($getIndustryList as $val) {
                                    ?>
                                    <option value="<?php echo $val['id']; ?>"><?php echo $val['industryName']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <small class="text-danger" ng-show="submitted && seekerWorkExpForm.industry.$error.required || seekerWorkExpForm.industry.$touched && seekerWorkExpForm.industry.$invalid">Please Select Industry</small>                 
                    </div>
                    <div class="form-group">
                        <label>Select Function<span class="red">*</span></label>
                        <select class="form-control" name="job_function" id="job_function" ng-model="tempSeekerWorkExp.job_function" ng-change="changeRole(tempSeekerWorkExp.job_function, 'add');">
                            <option ng-repeat="option in getFunctionList" value="{{option.id}}" ng-selected="tempSeekerWorkExp.job_function == option.id">{{option.functionName}}</option>
                        </select>
                        <small class="text-danger" ng-show="submitted && seekerWorkExpForm.job_function.$error.required || seekerWorkExpForm.job_function.$touched && seekerWorkExpForm.job_function.$invalid">Please Select Function</small>                  
                    </div>
                    <div class="form-group">
                        <label>Select Role <span class="red">*</span></label>
                        <select class="form-control" name="job_role" id="job_role" ng-model="tempSeekerWorkExp.job_role" required>
                            <option ng-repeat="option in getRoleListAdd" value="{{option.id}}" ng-selected="tempSeekerWorkExp.job_role == option.id">{{option.roleName}}</option>
                        </select>

                        <small class="text-danger" ng-show="submitted && seekerWorkExpForm.job_role.$error.required || seekerWorkExpForm.job_role.$touched && seekerWorkExpForm.job_role.$invalid">Please Select Role</small>
                    </div>
                    <div class="form-group">
                        <label>From</label>
                        <input class="form-control" type="text"  name="fromdate"  jqdatepicker  return-dob-date="selectFrom(dob)" ng-model="tempSeekerWorkExp.job_from" readonly="">
                    </div>
                    <div class="form-group">
                        <label>To</label>
                        <input class="form-control" type="text"  name="tempSeekerWorkExp.todate"  jqdatepicker  return-dob-date="selectTo(dob)" ng-model="job_to" readonly="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="submit"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">Submit</button>
                </div>

            </div>
        </form>


    </div>
</div>
<!-- Work Experience section (end)-->

<!-- Portfolio section (start)-->
<!--<div class="modal modal-primary fade scale" id="modal-lg-primary-addPortfolio" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary-addPortfolio" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">

        <form method="post" name="seekerPortfolioForm" class="form" enctype="multipart/form-data">
         
            <input type="text" class="form-control" id="userid" name="userid" ng-model="tempSeekerPortfolio.userid" ng-show="showUserId">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalPortfoliobtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Add Portfolio</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" id="title" ng-model="tempSeekerPortfolio.title" required>
                        <small class="text-danger" ng-show="seekerPortfolioForm.title.$invalid && seekerPortfolioForm.title.$touched">Please enter Title</small>
                    </div>
                    <div class="form-group">
                        <label>Client</label>
                        <input type="text" class="form-control" name="client" id="client" ng-model="tempSeekerPortfolio.client">
                    </div>
                    <div class="form-group">
                        <label>Project URL</label>
                        <input type="text" class="form-control" name="projectUrl" id="projectUrl" ng-model="tempSeekerPortfolio.projectUrl">
                    </div>           
                    <div class="form-group">
                        <label>Duration</label><br>
                        <div class="row">
                            <div class="col-md-2">From</div>
                            <div class="col-md-5">
                                <select class="form-control" name="monthnofrom" id="monthnofrom" ng-model="tempSeekerPortfolio.monthnofrom" required><option value="" ng-init="tempSeekerPortfolio.monthnofrom = '<?php isset($datefrom[1]) ? $datefrom[1] : '' ?>'">Select Month</option><option value="January">January</option><option value="Febraury">Febraury</option><option value="March">March</option><option value="April">April</option><option value="May">May</option><option value="June">June</option><option value="July">July</option><option value="August">August</option><option value="September">September</option><option value="October">October</option><option value="November">November</option><option value="December">December</option></select>
                                <small class="text-danger" ng-show="seekerWorkExpForm.monthnofrom.$invalid && seekerWorkExpForm.monthnofrom.$touched">Please Select Month</small>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" name="yearnofrom" id="yearnofrom" ng-model="tempSeekerPortfolio.yearnofrom" required><option value="" ng-init="tempSeekerPortfolio.yearnofrom = '<?php isset($datefrom[2]) ? $datefrom[2] : '' ?>'">Select Year</option><?php for ($i = 1970; $i < 2006; $i++) { ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?></select>
                                <small class="text-danger" ng-show="seekerWorkExpForm.yearnofrom.$invalid && seekerWorkExpForm.yearnofrom.$touched">Please enter Year</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">To</div>
                            <div class="col-md-5">
                                <select class="form-control" name="monthnoto" id="monthnoto" ng-model="tempSeekerPortfolio.monthnoto" required><option value="" ng-init="tempSeekerPortfolio.monthnoto = '<?php isset($dateto[1]) ? $dateto[1] : '' ?>'">Select Month</option><option value="January">January</option><option value="Febraury">Febraury</option><option value="March">March</option><option value="April">April</option><option value="May">May</option><option value="June">June</option><option value="July">July</option><option value="August">August</option><option value="September">September</option><option value="October">October</option><option value="November">November</option><option value="December">December</option></select>
                                <small class="text-danger" ng-show="seekerWorkExpForm.monthnoto.$invalid && seekerWorkExpForm.monthnoto.$touched">Please Select Month</small>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" name="yearnoto" id="yearnoto" ng-model="tempSeekerPortfolio.yearnoto" required><option value="" ng-init="tempSeekerPortfolio.yearnoto = '<?php isset($dateto[2]) ? $dateto[2] : '' ?>'">Select Year</option><?php for ($i = 1970; $i < 2006; $i++) { ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?></select>
                                <small class="text-danger" ng-show="seekerWorkExpForm.yearnoto.$invalid && seekerWorkExpForm.yearnoto.$touched">Please enter Year</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <input type="text" class="form-control" name="role" id="role" ng-model="tempSeekerPortfolio.role">
                    </div>
                    <div class="form-group">
                        <label>Project Details</label>
                        <input type="text" class="form-control" name="projectDetail" id="projectDetail" ng-model="tempSeekerPortfolio.projectDetail">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="button"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="addeditPortfolio(seekerPortfolioForm.$valid, 'add')">Submit</button>
                </div>

            </div>
        </form>


    </div>
</div>-->
<!-- Portfolio section (end)-->

<!-- Education Detail section (start)-->
<div class="modal modal-primary fade scale" id="modal-lg-primary-addEduDetail" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary-addEduDetail" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">

        <form method="post" name="seekerEduDetailForm" class="form" enctype="multipart/form-data" ng-submit="addeditEduDetail(seekerEduDetailForm.$valid, 'add')" novalidate>
           
            <input type="text" class="form-control" id="userid" name="userid" ng-model="tempSeekerEduDetail.userid" ng-show="showUserId">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalEduDetailbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Add Education Detail</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <div class="form-group">
                        <span class="text-muted">
                            <em>
                                <span style="color:red;">*</span> Indicates required field
                            </em>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Qualification<span class="red">*</span></label>
                        <select class="form-control" name="qualification" id="qualification" ng-model="tempSeekerEduDetail.qualification" ng-change="changeSpec(tempSeekerEduDetail.qualification, 'add');">
                            <option ng-repeat="option in getqualificationList" value="{{option.id}}" ng-selected="tempSeekerEduDetail.qualification == option.id">{{option.educationLevelName}}</option>
                        </select>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.qualification.$error.required || seekerEduDetailForm.qualification.$touched && seekerEduDetailForm.qualification.$invalid">Please Select Qualification</small>                 
                    </div>
                    <div class="form-group">
                        <label>Course Type<span class="red">*</span></label><br>
                        <select class="form-control" name="courseType" id="courseType" ng-model="tempSeekerEduDetail.courseType" required>
                            <option value="1">Full Time</option><option value="2">Part Time</option>
                        </select>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.courseType.$error.required || seekerEduDetailForm.courseType.$touched && seekerEduDetailForm.courseType.$invalid">Please Select Course Type</small>
                    </div>
                    <div class="form-group">
                        <label>Specialization<span class="red">*</span></label>
                        <select class="form-control" name="specialization" id="specialization" ng-model="tempSeekerEduDetail.specialization">
                            <option ng-repeat="option in getSpecializationListAdd" value="{{option.id}}" ng-selected="tempSeekerEduDetail.specialization == option.id">{{option.educationLevelName}}</option>
                        </select>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.specialization.$error.required || seekerEduDetailForm.specialization.$touched && seekerEduDetailForm.specialization.$invalid">Please Select Specialization</small>
                    </div>
                    <div class="form-group">
                        <label>Institute Name<span class="red">*</span></label>
                        <input type="text" class="form-control" name="institute" id="institute" ng-model="tempSeekerEduDetail.institute" required>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.institute.$error.required || seekerEduDetailForm.institute.$touched && seekerEduDetailForm.institute.$invalid">Please Enter Institute Name</small>
                    </div>
                    <div class="form-group">
                        <label>Field of Study<span class="red">*</span></label>
                        <input type="text" class="form-control" name="study" id="study" ng-model="tempSeekerEduDetail.study" required>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.study.$error.required || seekerEduDetailForm.study.$touched && seekerEduDetailForm.study.$invalid">Please Enter Field of Study</small>
                    </div>
                    <div class="form-group">
                        <label>Year Of Passing<span class="red">*</span></label>
                        <select class="form-control" name="yearOfPass" id="yearOfPass" ng-model="tempSeekerEduDetail.yearOfPass" required>
                            <option>Select Year</option>
                            <?php for ($i = 1970; $i < 2006; $i++) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.yearOfPass.$error.required || seekerEduDetailForm.yearOfPass.$touched && seekerEduDetailForm.yearOfPass.$invalid">Please Select Year Of Passing</small>
                    </div>
                    <div class="form-group">
                        <label>Percentage Scored<span class="red">*</span></label>
                        <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" class="form-control" name="scored" id="scored" ng-model="tempSeekerEduDetail.scored" required>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.scored.$error.required 
                        || seekerEduDetailForm.scored.$touched && seekerEduDetailForm.scored.$invalid && !seekerEduDetailForm.scored.$error.pattern">
                        Please Enter Percentage Scored</small>
                        <small class="text-danger" ng-show="seekerEduDetailForm.scored.$touched 
                        && seekerEduDetailForm.scored.$error.pattern">Please enter Valid Percentage Scored</small>
                         <small class="form-text text-muted">upto 2 decimal places</small>

                    </div>
                    <div class="form-group">
                        <label>Location<span class="red">*</span></label>
                        <input type="text" class="form-control" name="location" id="location" ng-model="tempSeekerEduDetail.location" required>
                        <small class="text-danger" ng-show="submitted && seekerEduDetailForm.location.$error.required || seekerEduDetailForm.location.$touched && seekerEduDetailForm.location.$invalid">Please Enter Location</small>
                    </div>           
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="submit"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted =true">Submit</button>
                </div>

            </div>
        </form>


    </div>
</div>
<!-- Education Detail section (end)-->

<!-- Certification section (start)-->
<div class="modal modal-primary fade scale" id="modal-lg-primary-addCertification" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary-addCertification" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">

        <form method="post" name="seekerCertificationForm" class="form" ng-submit="addeditCertification(seekerCertificationForm.$valid, 'add')" novalidate>
            
            <input type="text" class="form-control" id="userid" name="userid" ng-model="tempSeekerCertification.userid" ng-show="showUserId">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalCertificationbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Add Certification</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <span class="text-muted">
                        <em>
                            <span style="color:red;">*</span> Indicates required field
                        </em>
                    </span>
                    <div class="form-group">
                        <label>Certification<span class="red">*</span></label>
                        <input type="text" class="form-control" name="certification" id="certification" ng-model="tempSeekerCertification.certification" required>
                        <small class="text-danger" ng-show="submitted && seekerCertificationForm.certification.$error.required || seekerCertificationForm.certification.$touched && seekerCertificationForm.certification.$invalid">Please Enter Certification</small>
                    </div>
                    <div class="form-group">
                        <label>Certification Year<span class="red">*</span></label>
                        <select class="form-control" name="certifyYear" id="certifyYear" ng-model="tempSeekerCertification.certifyYear" required>
                            <option>Select Year</option>
                            <?php for ($i = 1970; $i < 2006; $i++) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                        <small class="text-danger" ng-show="submitted && seekerCertificationForm.certifyYear.$error.required || seekerCertificationForm.certifyYear.$touched && seekerCertificationForm.certifyYear.$invalid">Please Select Certification Year</small>
                    </div>           
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="submit"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted = true">Submit</button>
                </div>

            </div>
        </form>


    </div>
</div>
<!-- Certification section (end)-->

<!-- Achievement section (start)-->
<div class="modal modal-primary fade scale" id="modal-lg-primary-addAchievement" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary-addAchievement" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">

        <form method="post" name="seekerAchievementForm" class="form" ng-submit="addeditAchievement(seekerAchievementForm.$valid, 'add')" novalidate>
            <input type="text" class="form-control" id="userid" name="userid" ng-model="tempSeekerAchievement.userid" ng-show="showUserId">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalAchievementbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Add Achievement</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <span class="text-muted">
                        <em>
                            <span style="color:red;">*</span> Indicates required field
                        </em>
                    </span>
                    <div class="form-group">
                        <label>Achievement<span class="red">*</span></label>
                        <input type="text" class="form-control" name="achievement" ng-model="tempSeekerAchievement.achievement" required>
                        <small class="text-danger" ng-show="submitted && seekerAchievementForm.achievement.$error.required || seekerAchievementForm.achievement.$touched && seekerAchievementForm.achievement.$invalid">Please Enter Achievement</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="submit"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted = true">Submit</button>
                </div>

            </div>
        </form>


    </div>
</div>
<!-- Achievement section (end)-->

<!-- Model section -->
<script type="text/javascript">

    $('.jobfunctionDrop').on('change', function () {
        ///alert($(this).attr("id")+ $(this).val());
        var dataID = $(this).attr("id");
        var arr = dataID.split('_');
//arr[2]
        var functionId = $(this).val();//$('select[name="job_function"]').val();
        $('#job_role_' + arr[2]).html('<option></option>');
        $.ajax({
            type: 'post',
            url: '<?php echo base_url(); ?>admin/manage_jobs/getRoleName',
            data: {'functionId': functionId},
            dataType: 'json',
            success: function (data)
            {
                $.each(data, function (index, value) {
                    $('#job_role_' + arr[2]).append($('<option>').text(value['roleName']).val(value['id']));
                });

            }
        });
    });
</script>