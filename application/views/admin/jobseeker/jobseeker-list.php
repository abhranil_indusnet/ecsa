<script src="<?= base_url() ?>assets/admin/app/controllers/jobseekerController.js"></script>
<div class="jumbotron-1">
    <div class="jumbotron jumbotron-fluid">
        <div class="container-fluid">                
            <h1 class="display-3">Job Seekers</h1>
            <ol class="breadcrumb icon-home icon-angle-right no-bg">
                <li><a href="<?= base_url() ?>admin/dashboard">Dashboard</a></li>
                <li class="active">Jobseekers</li>
            </ol>
        </div>
    </div>
</div>
<div class="col-xs-12 main">
    <div class="page-on-top">
        <div class="row">
            <div class="col-xs-12">
                <div class="widget">
                    <div class="row m-b-20">
                        <div class="col-xs-12">
                            <p class="color-grey-900 m-b-5">Jobseekers Users</p>
                            <div id="messages"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="datatable-example-2_wrapper" class="dataTables_wrapper">
                                <div class="dataTables_length" id="datatable-example-2_length">
                                    <label>Show 
                                        <select ng-options="x for x in itemsPage" class="form-control" ng-model="selectedName" ng-change="changeNoOfItem()">
                                        </select>
                                        entries
                                    </label>
                                </div>

                                <div class="dataTables_length" id="datatable-example-2_length"><a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#modal-lg-primary" id="addmodal">Add Jobseeker</a></div>

                                <div id="datatable-example-2_filter1" class="dataTables_filter"><label>Search:<input class="form-control" name="userSearch" ng-model="userSearch" placeholder="" aria-controls="datatable-example-2" type="search"></label>
                                <!--<br/><a href="#" onclick="$('#advanceFilter').slideDown()">Advance Filter</a>-->
                                </div>
                                <!--<div id="advanceFilter" style="display:none; float:left;"  class="dataTables_filter">
                                    <label>Name:
                                        <input class="form-control" name="nameSearch" ng-model="nameSearch" placeholder="" aria-controls="datatable-example-2" type="search">
                                    </label>
                                    <label>&nbsp;&nbsp;&nbsp;Email:
                                        <input class="form-control" name="emailSearch" ng-model="emailSearch" placeholder="" aria-controls="datatable-example-2" type="search">
                                    </label>

                                </div>-->

                               <table class="table table-hover table-striped table-bordered dataTable" role="grid" aria-describedby="datatable-example-2_info">

                                    <thead>
                                        <tr role="row"> 
                                             <th ng-class="{ sorting_desc: reverseSort == true && orderByField == 'createdOn' , sorting_asc: reverseSort == false && orderByField == 'createdOn' , sorting : orderByField != 'createdOn' }" ng-click="orderByField = 'createdOn'; reverseSort = !reverseSort">Created On</th>

                                            <th ng-class="{sorting_desc:reverseSort==true && orderByField=='jobseekerFirstName',
                                                           sorting_asc:reverseSort==false && orderByField=='jobseekerFirstName',
                                                           sorting: orderByField !='jobseekerFirstName'}"
                                                           ng-click="orderByField='jobseekerFirstName';reverseSort=!reverseSort"
                                                           >Name</th>

                                            <th ng-class="{sorting_desc:reverseSort==true && orderByField=='userEmail',
                                                           sorting_asc:reverseSort==false && orderByField=='userEmail',
                                                           sorting: orderByField !='userEmail'}"
                                                           ng-click="orderByField='userEmail';reverseSort=!reverseSort">Email</th>

                                            <th ng-class="{sorting_desc:reverseSort==true && orderByField=='jobseekerMobile',
                                                           sorting_asc:reverseSort==false && orderByField=='jobseekerMobile',
                                                           sorting: orderByField !='jobseekerMobile'}"
                                                           ng-click="orderByField='jobseekerMobile';reverseSort=!reverseSort">Phone Number</th>               

                                            

                                            <th colspan="2">Action</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <tr ng-show="showLoader"><td colspan="6" style="text-align:center;"><img src="<?= base_url() ?>assets/admin/images/load.gif" style="border:none;"></td></tr>
                                        <tr dir-paginate="user in users | orderBy:orderByField:reverseSort | filter:userSearch | itemsPerPage:itemsPerPage" total-items="total_count" ng-show="showUserList">
                                            <td>{{user.createdOn}}</td>
                                            <td>{{user.jobseekerFirstName}} {{user.jobseekerLastName}}</td>
                                            <td>{{user.userEmail}}</td>
                                            <td>{{user.jobseekerMobile}}</td>
                                            <td> <a href="<?php echo base_url(); ?>admin/jobseeker/edit/{{user.userId}}" class="fa fa-x fa-edit editmodal"></a> </td>
                                            <td><a href="javascript:void(0);" class="fa fa-x fa-trash" ng-click="deleteJobSeeker(user)" ></a></td>
                                        </tr>
                                        <tr ng-show="showNoRecord">
                                            <td colspan="6" style="text-align:center;">No Records found</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div ng-if="showUserList" class="dataTables_info" id="datatable-example-2_info" role="status" aria-live="polite">Showing {{((currentPage - 1) * itemsPerPage) + 1}} to {{((currentPage - 1) * itemsPerPage) + users.length}} of {{ total_count}}</div>
                                <dir-pagination-controls class="dataTables_paginate paging_simple_numbers"
                                                         max-size="5"
                                                         direction-links="true"
                                                         boundary-links="true"
                                                         template_url="<?= base_url() ?>assets/admin/components/pagination.tpl.html"
                                                         on-page-change="getRecords(newPageNumber)" >
                                </dir-pagination-controls>

                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>