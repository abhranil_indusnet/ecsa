<style type="text/css">
    .bmd-form-group {
        padding-top: 0.75rem;
        position: relative;
    }
</style>
<div class="modal modal-primary fade scale" id="modal-lg-primary" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" name="userForm" class="form" enctype="multipart/form-data" autocomplete="on" ng-submit="Submit(userForm.$valid)" novalidate>
            <input type="text" class="form-control" id="usereditid" name="usereditid" value="0" ng-model="tempUserData.id" ng-show="showUserId">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Jobseeker</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <div class="form-group">
                        <span class="text-muted">
                            <em>
                                <span style="color:red;">*</span> Indicates required field
                            </em>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>First Name<span class="red">*</span></label>
                        <input type="text" class="form-control" name="firstname" ng-model="tempUserData.firstname" id="firstname" required>
                        <small class="text-danger" ng-show="submitted && userForm.firstname.$error.required || 
                        userForm.firstname.$touched && userForm.firstname.$invalid">Please enter First name</small> 
                    </div>
                    <div class="form-group">
                        <label>last Name<span class="red">*</span></label>
                        <input type="text" class="form-control" name="lastname" ng-model="tempUserData.lastname" id="lastname" required>
                        <small class="text-danger" ng-show="submitted && userForm.lastname.$error.required || userForm.lastname.$touched && userForm.lastname.$invalid">Please enter Last name</small>
                    </div>
                    <div class="form-group">
                        <label>Date of Birth<span class="red">*</span></label>
                        <input class="form-control" type="text"  name="dob" id="dob" jqdatepicker  return-dob-date="selectDob(dob)" ng-model="dob" readonly="" />
                         <!--<small class="text-danger" ng-show="submitted && userForm.dob.$error.required || userForm.dob.$touched && userForm.dob.$invalid">Please select Date of Birth</small>-->

                    </div>
                    <div class="form-group">
                        <label>Email<span class="red">*</span></label>
                        <input type="email" class="form-control" name="useremail" id="useremail" ng-model="tempUserData.useremail" required>
                        <small class="text-danger" ng-show="submitted && userForm.useremail.$error.required">Please enter email   </small>
                        <small class="text-danger" ng-show="userForm.useremail.$touched && userForm.useremail.$invalid">Please enter valid email</small>

                    </div>
                    <div class="form-group">
                        <label>Password<span class="red">*</span></label>
                        <input type="password" class="form-control" name="userpassword" id="userpassword" ng-model="tempUserData.userpassword" ng-minlength="6" required>
                        <small class="text-danger" ng-show="submitted && userForm.userpassword.$error.required || userForm.userpassword.$touched && userForm.userpassword.$invalid && !userForm.userpassword.$error.minlength">Please enter password</small>
                        <small class="text-danger" ng-show="userForm.userpassword.$error.minlength">Password should be minimum 6 character long</small>
                    </div>
                    <a href="javascript:void(0);" id="changepass" class="changepass" ng-show="showedit">Change password</a>
                    <a href="javascr ipt:void(0);" id="canceltext" style="display:none;" class="changepass">Cancel</a> 

                     <div class="form-group ">
                        <label>Mobile Number Code<span class="red">*</span></label>
                        <input type="text" class="form-control" name="contactCode" id="contactCode" value="<?php echo isset($userdata['jobseekerMobileCode']) ? $userdata['jobseekerMobileCode'] : ""; ?>" ng-model="tempUserData.contactCode"   placeholder="+ 966"  required>
                        <small class="text-danger" ng-show="submitted && userForm.contactCode.$error.required || userForm.contactCode.$touched && userForm.contactCode.$invalid">Please Enter the Mobile Number Code</small>
                       
                    </div>

                    <div class="form-group ">
                        <label>Mobile Number<span class="red">*</span></label>
                        <input type="text" class="form-control" name="contactno" id="contactno" value="<?php echo isset($userdata['jobseekerMobile']) ? $userdata['jobseekerMobile'] : ""; ?>" ng-model="tempUserData.contactno"  number-only  required>
                        <small class="text-danger" ng-show="submitted && userForm.contactno.$error.required || userForm.contactno.$touched && userForm.contactno.$invalid">Please Enter the Mobile Number</small>
                       
                    </div>
                 

                    <div class="form-group">
                        <label for="resumeInputFile">Upload Resume</label>
                        <p>Microsoft word, doc, docx or PDF only (Max 4MB)</p>
                        <input type="file" id="resumeInputFile" file-model="tempUserData.resumeupload">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
<!--                    <button type="button"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="addUser2(userForm.$valid)">Submit</button>
                    <button type="button" ng-show="showedit" class="btn btn-raised btn-secondary btn-flat" ng-click="updateUser(userForm.$valid)">Update</button>-->

                    <button type="submit" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true">Submit</button>
                </div>

            </div>
        </form>


    </div>
</div>
<script>

    $(document).on('click', '.changepass', function () {

        $('#userpassword').val('');
        $('#canceltext').show();
        $('#changepass').hide();


    });
    $(document).on('click', '#canceltext', function () {
        $('#userpassword').val('******');
        $('#canceltext').hide();
        $('#changepass').show();


    });

</script>
