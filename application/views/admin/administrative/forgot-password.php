<div class="container-fluid">
        <div class="row">
                        <div class="message">
                        <?php if($this->session->userdata('success_msg')){
                            echo '<span class="succ_msg">'.$this->session->userdata('success_msg').'</span>';
                            $this->session->set_userdata('success_msg');
                        }elseif($this->session->userdata('error_msg')){
                            echo '<span class="err_msg">'.$this->session->userdata('error_msg').'</span>';
                            $this->session->set_userdata('error_msg');
                        }?>
                    </div>
            <div class="col-xs-12 main">
                <form class="sign-in" action="<?php echo $action; ?>" method="post">
                    <h3>Reset Password</h3>
                    <div class="form-group">
                        <label for="sign-in-2-email" class="bmd-label-floating">Password</label>
                        <input class="form-control" name="password" id="password" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Must have at least 6 characters' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" autofocus>
                    </div>  
                    <div class="form-group">
                        <label for="sign-in-2-email" class="bmd-label-floating">Re-enter Password</label>
                        <input class="form-control" id="password_two" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');" autofocus>
                    </div>               
                    <input type="submit" class="btn btn-raised btn-lg btn-secondary btn-block" name="login_submit" id="submit" value="Submit">
                    
                    <p class="sign-up-link"><a href="<?php echo base_url(); ?>admin">Back to Login</a></p>
                </form>
            </div>
        </div>
    </div>