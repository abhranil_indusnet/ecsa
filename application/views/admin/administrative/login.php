<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 main">

           <!--<div class="panel-heading" align="center">
              <img src="<?= base_url() ?>assets/admin/images/ecsa_logo.png" width="192" height="50" alt="">
           </div>-->

            <form class="sign-in" method="post">
                <h3>Sign in</h3>
                <?php
                if ($this->session->flashdata('success_msg')) {
                    echo '<div class="alert alert-success">' . $this->session->flashdata('success_msg') . '</div>';
                }
                ?>
                <?php
                if ($this->session->flashdata('error_msg')) {
                    echo '<div class="alert alert-danger">' . $this->session->flashdata('error_msg') . '</div>';
                }
                ?>
                <p>
                    Please enter your email address and password to login
                </p>
                <div class="form-group">
                    <label for="sign-in-2-email" class="bmd-label-floating">Email address</label>
                    <input class="form-control" name="username" type="email" autofocus>
                    <span class="bmd-help"><?php echo form_error('username', '<p class="help-block error">', '</p>'); ?></span>
                </div>
                <div class="form-group">
                    <label for="sign-in-1-password" class="bmd-label-floating">Password</label>
                    <input class="form-control" name="password" id="sign-in-1-password" type="password" value="">
                    <span class="bmd-help"><?php echo form_error('password', '<p class="help-block error">', '</p>'); ?></span>
                </div>
                <div class="checkbox checkbox-secondary">
                    <label>
                        <input type="checkbox" value="remember-me" name="remember">Remember me
                    </label>
                </div>                   
                <input type="submit" class="btn btn-raised btn-lg btn-secondary btn-block" name="login_submit" id="submit" value="Sign In">

                <!--<p class="sign-up-link"><a href="<?php echo base_url(); ?>admin/forgetPassword">Forget Password</a></p>-->
                <p class="copyright">&copy; Copyright <?php echo date('Y'); ?></p>
            </form>
        </div>
    </div>
</div>