<div class="container-fluid">
        <div class="row">
                        <div class="message">
                        <?php if($this->session->userdata('success_msg')){
                            echo '<span class="succ_msg">'.$this->session->userdata('success_msg').'</span>';
                            $this->session->set_userdata('success_msg');
                        }elseif($this->session->userdata('error_msg')){
                            echo '<span class="err_msg">'.$this->session->userdata('error_msg').'</span>';
                            $this->session->set_userdata('error_msg');
                        }?>
                    </div>
            <div class="col-xs-12 main">
                <form class="sign-in" action="" method="post">
                    <p class="success"></p> 
                    <h3>Forget Password</h3> 
                    <div class="form-group">
                        <label for="sign-in-2-email" class="bmd-label-floating">Email address</label>
                        <input class="form-control" name="username" id="username" type="email" autofocus>
                    </div>                
                    <input type="submit" class="btn btn-raised btn-lg btn-secondary btn-block" name="login_submit" id="submit" value="Submit" readonly="">
                    <p class="sign-up-link"><a href="<?php echo base_url(); ?>admin">Back to Login</a></p>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
     $('#submit').click(function(e){
        e.preventDefault();

        var email = $('#username').val(); 
        var url = '<?php echo base_url(); ?>admin/administrative/checkEmail/'+email;

        $.ajax({
               url: url,
               type: 'get',
               success : function(data)
               {
                if(data == 1){ 
                    $('.success').html('<span class="alert alert-success">Email has been sent to you</span>');
                } else { 
                    $('.success').html('<span class="alert alert-danger">Please enter the valid email address</span>');
                }
                
               }
            })
    });   
</script>