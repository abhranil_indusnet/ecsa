 <div id="page-wrapper" class="page-wrapper-cls">
    <div id="page-inner">
        
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Update Profile</h1>
            </div>
        </div>

        <div class="row">
         <div class="col-lg-12">
            <?php 
            if(!empty($msg['success_msg'])){
             echo '<div class="alert alert-success">'.$msg['success_msg'].'</div>';
         }
         
         if(!empty($msg['error_msg'])){
          echo '<div class="alert alert-danger">'.$msg['error_msg'].'</div>';
      }
      ?>
  </div>
</div>





<div class="row">
	<div class="col-lg-4">
       <div class="panel panel-success">
        <div class="panel-heading">
            Profile Information
        </div>
        <form role="form" method="post">
            <div class="panel-body">
                <div class="form-group">
                    <label>First Name</label>
                    <input class="form-control" id="firstname" type="text" name="firstname" value="<?php echo !empty($firstname)?$firstname:''; ?>">
                    <?php echo form_error('firstname','<p class="help-block error">','</p>'); ?>
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input class="form-control" id="lastname" type="text" name="lastname" value="<?php echo !empty($lastname)?$lastname:''; ?>">
                    <?php echo form_error('lastname','<p class="help-block error">','</p>'); ?>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" id="email" type="text" name="email" value="<?php echo !empty($email)?$email:''; ?>">
                    <?php echo form_error('email','<p class="help-block error">','</p>'); ?>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input class="form-control" id="phone" type="text" name="phone" value="<?php echo !empty($phone)?$phone:''; ?>">
                    <?php echo form_error('phone','<p class="help-block error">','</p>'); ?>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" rows="3" name="address" id="address"><?php echo !empty($address)?$address:''; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Postcode</label>
                    <input class="form-control" id="postcode" type="text" name="postcode" value="<?php echo !empty($postcode)?$postcode:''; ?>">
                    <?php echo form_error('postcode','<p class="help-block error">','</p>'); ?>
                </div>
                <input type="submit" class="btn btn-default" name="update_profile_submit" id="submit" value="Update Profile">
            </div>
        </form>

        
    </div>
</div>
</div>


</div>
</div>





