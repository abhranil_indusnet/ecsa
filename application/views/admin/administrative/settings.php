                 <div id="page-wrapper" class="page-wrapper-cls">
                 <div id="page-inner">
					
					<div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Settings</h1>
                    </div>
                   </div>
					
     <div class="row">
	          <div class="col-lg-12">
		<?php 
			if(!empty($msg['success_msg'])){
				echo '<div class="alert alert-success">'.$msg['success_msg'].'</div>';
			}

			if(!empty($msg['error_msg'])){
				echo '<div class="alert alert-danger">'.$msg['error_msg'].'</div>';
			}
		?>
			</div>
		</div>
	 
	 
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Update Email Address</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            	<div class="row">
            		<div class="col-lg-6">
                        <form role="form" method="post">
                            <div class="form-group">
                                <label>New Email Address</label>
                                <input class="form-control" id="email" type="text" name="email" value="" >
                                <?php echo form_error('Email Address','<p class="help-block error">','</p>'); ?>
                            </div>
                            <!--<div class="form-group">
                                <label>Password</label>
                                <input class="form-control" id="password" type="password" name="password" value="">
                                <?php echo form_error('password','<p class="help-block error">','</p>'); ?>
                            </div>-->
                            <input type="submit" class="btn btn-default" name="update_username_submit" id="submit" value="Update">
                        </form>
                    </div>
            	</div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        
        <div class="panel panel-default">
            <div class="panel-heading">Update Password</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            	<div class="row">
            		<div class="col-lg-6">
                        <form role="form" method="post">
                            <div class="form-group">
                                <label>Old Password</label>
                                <input class="form-control" id="old_password" type="password" name="old_password" value="">
                                <?php echo form_error('old_password','<p class="help-block error">','</p>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="form-control" id="password" type="password" name="password" value="">
                                <?php echo form_error('password','<p class="help-block error">','</p>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input class="form-control" id="conf_password" type="password" name="conf_password" value="">
                                <?php echo form_error('conf_password','<p class="help-block error">','</p>'); ?>
                            </div>
                            <input type="submit" class="btn btn-default" name="update_password_submit" id="submit" value="Update">
                        </form>
                    </div>
            	</div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!--<a href="<?php echo base_url(); ?>admin/adminuser/" class="btn btn-primary btn-lg btn-block">Back</a>-->
    </div>
    <!-- /.col-lg-12 -->
</div>
 </div>
