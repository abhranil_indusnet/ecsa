    <div class="jumbotron-1">
        <div class="jumbotron jumbotron-fluid">
            <div class="container-fluid">
                <h1 class="display-3">Site Management </h1>
                <ol class="breadcrumb icon-home icon-angle-right no-bg">
                    <li><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/settings">Settings</a> </li>
                    <li class="active">Default Type Permission </li>
                </ol>
            </div>
        </div>
    </div>  
        <div class="col-xs-12 main">
                <div class="page-on-top">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget" ng-init="getRecords()">
                                <div class="row m-b-20">
                                    <div class="col-xs-12">
                                        <p class="color-grey-900 m-b-5">Default Type Permission</p>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                      <form name="bulk_action_form" action="" method="post" />
                        <div class="panel-body">
                            <div class="table-responsive">
                                <?php
                                if(!empty($fullMenu)) 
                                {
                                ?>
                               <table class="table table-striped table-bordered table-hover">
                                <thead>
                                        <tr>
                                        <th width="30%">Menus</th>
                                        <th colspan="<?php echo count($actionList); ?>" width="70%">Actions</th>
                                        </tr>

                                        <tr>
                                        <th width="30%">&nbsp;</th>
                                        <?php 
                                        foreach($actionList as $action)
                                        {
                                        ?>
                                        <th><?php echo $action['adminActionName'] ; ?></th>
                                        <?php
                                        }
                                        ?>
                                        </tr>
                                </thead>
                                <tbody>
                                <?php   
        foreach($fullMenu as $menu)
        {  
                                
        if(isset($menu['adminMenuPermission']['adminMenuPermission']) && $menu['adminMenuPermission']['adminMenuPermission']!="" &&  $menu['adminMenuPermission']['adminMenuPermission']!='null')
         {
          $access=json_decode($menu['adminMenuPermission']['adminMenuPermission'],TRUE); 
         } else{
          $access=array();
         }
         ?>
              <tr>
              <td style="padding-left:10px;">
              <?php if(!isset($menu['childMenu'])){?>
              <input type="checkbox" class="me" id="<?php echo $menu['adminMenuSlug'];?>"value="<?php echo $menu['adminMenuSlug'];?>" <?php if(!empty($access)) echo "checked"; ?> /> 
              <?php } ?>
              <?php echo $menu['adminMenuName']; ?>
              </td>

              <?php if($menu['adminMenuSlug']!="#"){
              foreach($actionList as $action)
              { ?>
              <td>
              <input type="checkbox" class="<?php echo $menu['adminMenuSlug'];?>" value="<?php echo $action['id'] ; ?>" name="<?php echo $menu['adminMenuSlug'];?>_action[]" <?php if(in_array($action['id'],$access)) echo "checked"; ?> onclick="check_btn('<?php echo $menu['adminMenuSlug'];?>');">
              </td>
              <?php }}
              else{ ?>
              <td colspan="<?php echo count($actionList)?>"></td>
              <?php } ?>
              </tr>

              <?php 
              if(isset($menu['childMenu']) && !empty($menu['childMenu'])){

              foreach($menu['childMenu'] as $child)
              {  
            
                  if(isset($child['adminMenuPermission']['adminMenuPermission']) && $child['adminMenuPermission']['adminMenuPermission']!="" and $child['adminMenuPermission']['adminMenuPermission']!='null')
                    {
                      $access=json_decode($child['adminMenuPermission']['adminMenuPermission'],TRUE); 
                     }
                    else
                    {
                      $access=array();
                    }?>

               <tr>
               <td style="padding-left:10px;">&nbsp;&nbsp;|__&nbsp;&nbsp;<input type="checkbox" class="me" id="<?php echo $child['adminMenuSlug'];?>"   value="<?php echo $child['adminMenuSlug'];?>" <?php if(!empty($access)) echo "checked"; ?> /> <?php echo $child['adminMenuName']; ?>
              
               </td>

               <?php foreach($actionList as $action){ ?>
               <td>
               <input type="checkbox" class="<?php echo $child['adminMenuSlug'];?>" value="<?php echo $action['id'] ; ?>" name="<?php echo $child['adminMenuSlug'];?>_submenu_action[]" <?php if(in_array($action['id'],$access)) echo "checked"; ?> onclick="check_btn('<?php echo $child['adminMenuSlug'];?>');">
               </td>
               <?php } ?>

               </tr> 
               <?php  }} ?>

              
                                <?php
                                        }
                                ?>
                                </tbody>
                                </table>
                            <?php
                                    }
                            ?>
                            </div>
                            
             <input type="hidden" name="user_type" value="<?php echo $user_type;  ?>" />
               <?php if(checkActionPermission($adminMenuSlug,3)){?>
                <input type="submit" class="btn btn-outline btn-success" style="margin-bottom: 1px;" name="submit" value="{{Submit}}"/>
               <?php } ?> 
          
            <!-- /.panel-body -->
            </form> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
<script>
   $('.me').click(function(e){
        var v1=$(this).val(); 
        if(this.checked==true && v1!='#')
        {
            $('.'+v1).prop('checked', true);
            
        }
        else
        {
             if($(".me:checked").length <1)
            {
              e.preventDefault();
              alert('You have to give some access to the user group');
            }
            else if(v1!='#')
            {
                $('.'+v1).prop('checked', false);
            }  
        }

    });
      

        
        
    
function check_btn(cls)
{
        if($("input[type='checkbox']:checked").length == 1)
        {
          e.preventDefault();
          alert('You have to give some access to the user group');
        }
        else
        {
            
            if($('.'+cls+':checked').length > 0)
             {
            
            $('#'+cls).prop('checked',true);

             }
             else
             {
            $('#'+cls).prop('checked',false);
           
             }
       }
}

/*function showHideMenuItems(id,action)
{
    
    if(action=='close')
    {
        $('#isubmenus_'+id).hide();
        $('#iopen_'+id).hide();
        $('#iclose_'+id).show();
    }else if(action=='open')
    {
        $('#isubmenus_'+id).show();
        $('#iopen_'+id).show();
        $('#iclose_'+id).hide();
    }
}*/
</script>