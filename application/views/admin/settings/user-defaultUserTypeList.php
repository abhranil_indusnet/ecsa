    <div class="jumbotron-1">
        <div class="jumbotron jumbotron-fluid">
            <div class="container-fluid">
                <h1 class="display-3">Site Management</h1>
                <ol class="breadcrumb icon-home icon-angle-right no-bg">
                    <li><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                    <li>Settings </li>
                    <li class="active">Default Permission </li>
                </ol>
            </div>
        </div>
    </div>        
        <div class="col-xs-12 main">
                <div class="page-on-top">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget" ng-init="getRecords()">
                                <div class="row m-b-20">
                                    <div class="col-xs-12">

                                        <p class="color-grey-900 m-b-5">Default Permission </p>
                                        
                                    </div>
                                </div>
                        <div class="row m-b-20">
                            <div class="col-xs-12">
                               <div id="datatable-example-2_wrapper" class="dataTables_wrapper">      
                                <div class="row">
                                    <div class="col-xs-12">
                                   
                                        <table id="datatable-example-2_wrapper" class="table table-hover table-striped table-bordered dataTables_wrapper">
                                            <thead>
                                                <tr>
                                                    <th>User Type</th>
                                                    <th colspan="2">Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot class="hidden">
                                                <tr>
                                                    <th>User Type</th>
                                                    <th colspan="2">Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <tr ng-repeat="usertype in usertypes">
                                                    <!--<td>{{usertype.adminUserType}}</td>
                                                    <td><a href="<?php echo base_url(); ?>admin/settings/defaultPermission/{{usertype.id}}" class="fa fa-x fa-hand-o-up" > Permissions</a></td>-->

                                        <?php switch ($this->session->userdata('admin_user_type_id')) {
                                        case '1': ?>
                                             <td>{{usertype.adminUserType}}</td>
                                                    <td><a href="<?php echo base_url(); ?>admin/settings/defaultPermission/{{usertype.id}}" class="fa fa-x fa-hand-o-up" > Permissions</a>
                                            </td>
                                        <?php break;

                                        case '2': ?>
                                             <td ng-show="usertype.id!=2">{{usertype.adminUserType}}</td>
                                             <td ng-show="usertype.id!=2"><a href="<?php echo base_url(); ?>admin/settings/defaultPermission/{{usertype.id}}" class="fa fa-x fa-hand-o-up" > Permissions</a>
                                            </td>
                                        <?php break;
                                        
                                         case '3':
                                           ?>
                                             <td ng-show="usertype.id!=2 && usertype.id!=3">{{usertype.adminUserType}}</td>
                                             <td ng-show="usertype.id!=2 && usertype.id!=3"><a href="<?php echo base_url(); ?>admin/settings/defaultPermission/{{usertype.id}}" class="fa fa-x fa-hand-o-up" > Permissions</a>
                                            </td>
                                        <?php break;
                                    }?>


                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
    
<script src="<?=base_url()?>assets/admin/app/controllers/userTypeController.js"></script>   