<div class="modal modal-primary fade scale" id="modal-lg-primary" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" name="departmentForm" class="form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Job Category/Department</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <div class="form-group">
                        <label>Job Category/Department Name</label>
                        <input type="text" class="form-control" name="departmentName" id="department" ng-model="tempDepartmentData.departmentName" required>
                        <small class="text-danger" ng-show="submitted && departmentForm.departmentName.$error.required || departmentForm.departmentName.$touched && departmentForm.departmentName.$invalid">Please enter a Job Category/Department name</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="button"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="addDepartment(departmentForm.$valid);submitted=true;">Submit</button>
                    <button type="button" ng-show="showedit" class="btn btn-raised btn-secondary btn-flat" ng-click="updateDepartment(departmentForm.$valid)">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

