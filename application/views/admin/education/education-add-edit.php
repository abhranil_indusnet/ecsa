<div class="modal modal-primary fade scale" id="modal-lg-primary" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
        <form  method="post" name="educationForm" class="form" ng-submit="addEditEducation(educationForm.$valid, action)">
   
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Educational Level</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>

                    <div class="form-group">
                        <label>Educational Level Name <span class="red">*</span></label>
                        <input type="text" class="form-control" name="educationLevelName" id="educationLevelName" ng-model="tempEducationData.educationLevelName" required>
                <small class="text-danger" ng-show="submitted && educationForm.educationLevelName.$error.required || educationForm.educationLevelName.$touched && educationForm.educationLevelName.$invalid">Please enter Educational Level Name</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="submit"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true;action = 'add'">Submit</button>
                    <button type="submit" ng-show="showedit" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true;action = 'edit'">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

