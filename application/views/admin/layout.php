<!DOCTYPE html>
<html lang="en" <?php if (!empty($ngController)) { ?> ng-app="admin" <?php } ?>>

    <head>
        <?php echo $head; ?>
    </head>

    <body class="animated fadeIn" id="dashboards-analytics" data-layout="default-sidebar-1" data-sidebar="success" data-navbar="white" data-controller="dashboards"  data-view="analytics" <?php if (!empty($ngController)) { ?> ng-controller="<?php echo $ngController; ?>" <?php } ?>>

        <?php if (isset($modalWindow)) {
            echo $modalWindow; 
        } ?>

<?php echo $header; ?>

        <div class="container-fluid" >
            <div class="row">		
                <?php echo $navigation_menu; ?>


<?php  echo $maincontent; ?> 
            </div></div>

        <!-- /#wrapper -->
<?php echo $footer; ?>
    </body>

