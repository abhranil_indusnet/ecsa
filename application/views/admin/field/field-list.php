<script src="<?= base_url() ?>assets/admin/app/controllers/FieldCtrl.js"></script>
<div class="jumbotron-1">
    <div class="jumbotron jumbotron-fluid">
        <div class="container-fluid">                
            <h1 class="display-3">Field Of Study Management</h1>
            <ol class="breadcrumb icon-home icon-angle-right no-bg">
                <li>
                    <a href="<?= base_url() ?>admin/dashboard">Dashboard</a>
                </li>
                <li class="active">Field Of Study List</li>
            </ol>
        </div>
    </div>
</div>
<div class="col-xs-12 main" ng-cloak>
    <div class="page-on-top">
        <div class="row">
            <div class="col-xs-12">
                <div class="widget">
                    <div class="row m-b-20">
                        <div class="col-xs-12">
                            <p class="color-grey-900 m-b-5">Field Of Study List</p>
                            <div id="messages"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="datatable-example-2_wrapper" class="dataTables_wrapper">
                                <div class="dataTables_length" id="datatable-example-2_length">
                                    <label>Show 
                                        <select ng-options="y for (x, y) in itemsPage"  class="form-control"
                                                ng-model="selectedName" 
                                                ng-change="changeNoOfItem()">
                                        </select>
                                        entries
                                    </label>
                                </div>

                                <div class="dataTables_length" id="datatable-example-2_length">
                                    <a href="javascript:void(0);" ng-click="addField();" class="btn btn-success" data-toggle="modal" data-target="#modal-lg-primary" id="addmodal">Add Field Of Study</a>
                                </div>

                                <div id="datatable-example-2_filter1" class="dataTables_filter">
                                    <label>Search:
                                        <input class="form-control" name="fieldSearch" ng-model="fieldSearch" placeholder="Search" aria-controls="datatable-example-2" type="search">
                                    </label>

                                </div>
                                <table class="table table-hover table-striped table-bordered dataTable" role="grid" aria-describedby="datatable-example-2_info">

                                    <thead>
                                        <tr role="row"> 

                                            <th ng-class="{ sorting_desc: (reverseSort == true && orderByField == 'fieldName'), sorting_asc: (reverseSort == false && orderByField == 'fieldName'), sorting : orderByField != 'fieldName' }" ng-click="orderByField = 'fieldName'; reverseSort = !reverseSort">Field Name</th>

                                            <th ng-class="{ sorting_desc: (reverseSort == true && orderByField == 'parentId'), sorting_asc: (reverseSort == false && orderByField == 'parentId'), sorting : orderByField != 'parentId' }" ng-click="orderByField = 'parentId'; reverseSort = !reverseSort">Parent Field Name</th>

                                            
                                            <th colspan="3">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-show="showLoader"><td colspan="6" style="text-align:center;"><img src="<?= base_url() ?>assets/admin/images/load.gif" style="border:none;"></td></tr>
                                        <tr dir-paginate="field in fieldDataAll | orderBy:orderByField:reverseSort | filter:fieldSearch | itemsPerPage:itemsPerPage" total-items="total_count" ng-show="showFieldList">
                                            <td>{{field.fieldName}}</td>
                                            <td>{{field.parentId == 0 ? 'Parent Field' : parentFieldOb[field.parentId]}} </td>
                                            <td><a href="javascript:void(0);" class="fa fa-x fa-edit editmodal" data-toggle="modal" data-target="#modal-lg-primary" ng-click="editField(field)"></a>
                                            </td>
                                            <td><a href="javascript:void(0);" class="fa fa-x fa-trash" ng-click="deleteField(field)" ></a></td>
                                            <td>
                                                <a href="javascript:void(0);" ng-click="changeStatus(field)"  ng-init="succClass = field.status == '1'?'btn btn-success' : 'btn btn-danger'" ng-class="succClass" >{{field.status == '1'?'Active' : 'Inactive'}}</a>

                                            </td>
                                        </tr>
                                        <tr ng-show="showNoRecord">
                                            <td colspan="6" style="text-align:center;">No Records found</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div ng-hide="showNoRecord" class="dataTables_info" id="datatable-example-2_info" role="status" aria-live="polite">Showing {{((currentPage - 1) * itemsPerPage) + 1}} to {{((currentPage - 1) * itemsPerPage) + fieldDataAll.length}} of {{ total_count}}</div>
                                <dir-pagination-controls class="dataTables_paginate paging_simple_numbers"
                                                         max-size="5"
                                                         direction-links="true"
                                                         boundary-links="true"
                                                         template_url="<?= base_url() ?>assets/admin/components/pagination.tpl.html"
                                                         on-page-change="getRecords(newPageNumber)" >
                                </dir-pagination-controls>

                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>