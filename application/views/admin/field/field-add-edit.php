<div class="modal modal-primary fade scale" id="modal-lg-primary" tabindex="-1" role="dialog" aria-labelledby="modal-lg-primary" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
        <form  method="post" name="fieldForm" class="form" ng-submit="addEditField(fieldForm.$valid, action)" novalidate>
   
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalbtn" ng-click="cancel()" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-lg-primary-label">Field Of Study</h4>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>

                    <div class="form-group bmd-form-group">
                        <label>Field Name <span class="red">*</span></label>
                        <input type="text" class="form-control" name="fieldName" id="fieldName" ng-model="tempFieldData.fieldName" required>
                    <small class="text-danger" ng-show="submitted && fieldForm.fieldName.$error.required || fieldForm.fieldName.$touched && fieldForm.fieldName.$invalid">Please enter Field Name</small>
                    </div>

            <div class="form-group" ng-show="add==true || (tempFieldData.isParent==2 && edit==true) || checkedYes==true ">
                <label>Is it a Parent Field? </label>

                <div class="radio"><br clear="all">

                <label class="radio-inline radio-primary"><input class="form-control" type="radio" name="isParent" id="isParent1" ng-model="tempFieldData.isParent" value="1" ng-click="showParentField=false;checkedYes=true" checked>Yes</label>

                 <label class="radio-inline radio-primary"><input class="form-control" type="radio" name="isParent" id="isParent2" ng-click="showParentField=true;checkedYes=false" ng-model="tempFieldData.isParent" value="2" >No</label>

            </div>

                               

                    <div class="form-group bmd-form-group" ng-show="showParentField">
                    <label>Parent Field <span class="red">*</span></label>
                    <select class="form-control"  id="parentId" name="parentId" ng-model="tempFieldData.parentId"  aria-controls="datatable-example-2">
                    <option value="">Select One</option>
                    <option ng-repeat="option in parentFieldList" value="{{option.id}}" ng-selected="option.id == tempFieldData.parentId">{{option.fieldName}}</option> 
                    </select>
                    <small class="text-danger" ng-show="submitted && showParentField && fieldForm.parentId.$error.required || fieldForm.parentId.$touched && fieldForm.parentId.$invalid && showParentField">Please enter Parent Field Name</small>
                                    
                                </div>



                            </div>

                  




                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="cancel()" class="btn btn-danger"  data-dismiss="modal">Close</button>
                    <div class="alert-danger" ng-if="error.length > 0" ng-bind="error"></div>
                    <button type="submit"  ng-show="showadd" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true;action = 'add'">Submit</button>
                    <button type="submit" ng-show="showedit" class="btn btn-raised btn-secondary btn-flat" ng-click="submitted=true;action = 'edit'">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

