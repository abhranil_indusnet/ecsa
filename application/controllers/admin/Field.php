<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Field extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'field';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        $this->load->model('admin/Modelfield');
        $this->load->model('admin/common');
        $this->load->model('admin/menu');
        $this->data['ngController'] = 'FieldCtrl';
    }

    public function index() {
        $data = array();
        $this->data['modalWindow'] = $this->load->view('admin/field/field-add-edit', $data, true);
        $this->data['maincontent'] = $this->load->view('admin/field/field-list', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getField($start = '', $noOfRecord = '') {
        $numRecords = $this->Modelfield->getTotalRows();
        if ($numRecords) {
            $data['records'] = $this->Modelfield->getRows($field_id = '', $start, $noOfRecord, 'id',array());
            $data['numrecords'] = $numRecords;
            $data['status'] = 'OK';
            //$params=array('conditions'=>array('parentId'=>0));
            //$data['parentFieldrecords']=$this->Modelfield->getRows($field_id = '', $start='', $noOfRecord='', 'id',$params); 
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function getParentField($id='') {
        $params=array('conditions'=>array('parentId'=>0));
        if($id){
        $params['conditions']['id !=']=$id;    
        }
        $result=$this->Modelfield->getRows($field_id = '', $start='', $noOfRecord='', 'id',$params); 
        if (!empty($result)) {
            $data['records'] = $result;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }


    public function addField() {
        $data = array();
        if (!empty($_POST['data'])) {
            $fieldName = trim($_POST['data']['fieldName']);
            $parentId = $_POST['data']['isParent']==1 ? 0 : $_POST['data']['parentId'];
            $checkFieldExists=$this->Modelfield->fieldValueCheck(array('fieldName'=>$fieldName,'deleted'=>'0'));
            if(!$checkFieldExists){
         
            $insertData = array(
                'fieldName' => $fieldName,
                'parentId' => $parentId,
                'createdBy'=>$this->session->userdata('admin_user_id')
                
            );
            $insert = $this->Modelfield->insert($insertData);

            if ($insert) {
                $data['fieldData'] = $insertData;
                $data['data'] = $insert;
                $data['status'] = 'OK';
                $data['msg'] = 'Added Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Field Of Study already exists'; 
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/field/field-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function editField() {
        $data = array();
      
        if (!empty($_POST['data'])) {
            $id = $_POST['data']['id'];
            $fieldName = trim($_POST['data']['fieldName']);
            $parentId = $_POST['data']['isParent']==1 ? 0 : $_POST['data']['parentId'];
            $checkFieldExists=$this->Modelfield->fieldValueCheck(array('fieldName'=>$fieldName,'deleted'=>'0','id!= '=>$id));
            if(!$checkFieldExists){
            
            $updateData = array(
                'fieldName' => $fieldName,
                'parentId' => $parentId,
                'updatedBy'=>$this->session->userdata('admin_user_id'),
                'updatedOn'=>date('Y-m-d H:i:s')
            );
            $condition = array('id' => $id);
            $update = $this->Modelfield->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Field Of Study already exists';
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Edit';
            $this->data['maincontent'] = $this->load->view('admin/field/field-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }
    
    public function changeStatus() {
       
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (!empty($id)) {
            $updateData = array(
                'status' => $status == '1' ? "0" : "1"
            );
            $condition = array('id' => $id);
            $update = $this->Modelfield->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Status Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
            
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Invalid Data.';
            echo json_encode($data);
            exit;
        }
    }
    
    public function deleteField() {
        $data = array(); 
        if (!empty($_POST['id'])) {
            $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
            $condition = array('id' => $id);
            $delete = $this->Modelfield->delete($condition);
            $condition = array('parentId' => $id);
            $delete1 = $this->Modelfield->delete($condition);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = 'Deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }
    

}
