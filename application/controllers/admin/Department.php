<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Department extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'department';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        $this->load->model('admin/Modeldepartment');
        $this->load->model('admin/common');
        $this->load->model('admin/menu');
        $this->data['ngController'] = 'DepartmentCtrl';
    }

    public function index() {
        $data = array();
        $this->data['modalWindow'] = $this->load->view('admin/department/department-add-edit', $data, true);
        $this->data['maincontent'] = $this->load->view('admin/department/department-list', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getDepartment($start = '', $noOfRecord = '') {
        $numRecords = $this->Modeldepartment->getTotalRows();
        if ($numRecords) {
            $data['records'] = $this->Modeldepartment->getRows($dapartment_id = '', $start, $noOfRecord, 'id');
            $data['numrecords'] = $numRecords;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function addDepartment() {
        $data = array();
        if (!empty($_POST['data'])) {
            $departmentName = trim($_POST['data']['departmentName']);
            $checkDepartmentExists=$this->Modeldepartment->fieldValueCheck(array('departmentName'=>$departmentName,'deleted'=>'0'));
            if(!$checkDepartmentExists){
         
            $departmentNameArr = array_map('trim', explode('/', $departmentName));
            $departmentSlugImplodeDash = implode("-",$departmentNameArr);
            $departmentNameArrSpace = array_map('trim', explode(" ", $departmentSlugImplodeDash));
            $departmentSlugImplode = implode("",$departmentNameArrSpace);
            
            $departmentSlug = strtolower($departmentSlugImplode);
            $insertData = array(
                'departmentName' => $departmentName,
                'departmentSlug' => $departmentSlug,
            );
            $insert = $this->Modeldepartment->insert($insertData);

            if ($insert) {
                $data['departmentData'] = $insertData;
                $data['data'] = $insert;
                $data['status'] = 'OK';
                $data['msg'] = 'Added Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Job Category/Department already exists'; 
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/department/department-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function editDepartment() {
        $data = array();
       // p($_POST,false);
        if (!empty($_POST['data'])) {
            $id = $_POST['data']['id'];
            $departmentName = trim($_POST['data']['departmentName']);
            $checkDepartmentExists=$this->Modeldepartment->fieldValueCheck(array('departmentName'=>$departmentName,'deleted'=>'0','id!= '=>$id));
            if(!$checkDepartmentExists){
            
            $departmentNameArr = array_map('trim', explode('/', $departmentName));
            $departmentSlugImplodeDash = implode("-",$departmentNameArr);
            
            $departmentNameArrSpace = array_map('trim', explode(" ", $departmentSlugImplodeDash));
            $departmentSlugImplode = implode("",$departmentNameArrSpace);
            
            $departmentSlug = strtolower($departmentSlugImplode);
            
            $updateData = array(
                'departmentName' => $departmentName,
                'departmentSlug' => $departmentSlug,
            );
            $condition = array('id' => $id);
            $update = $this->Modeldepartment->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Job Category/Department already exists';
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Edit';
            $this->data['maincontent'] = $this->load->view('admin/department/department-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }
    
    public function changeStatus() {
        //p($_POST);
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (!empty($id)) {
            $updateData = array(
                'status' => $status == '1' ? "0" : "1"
            );
            $condition = array('id' => $id);
            $update = $this->Modeldepartment->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Status Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
            
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Invalid Data.';
            echo json_encode($data);
            exit;
        }
    }
    
    public function deleteDepartment() {
        $data = array();
        if (!empty($_POST['id'])) {
            $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
            $condition = array('id' => $id);
            $delete = $this->Modeldepartment->delete($condition);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = 'Deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }
    

}
