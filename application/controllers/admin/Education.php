<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Education extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'education';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        $this->load->model('admin/Modeleducation');
        $this->load->model('admin/common');
        $this->load->model('admin/menu');
        $this->data['ngController'] = 'EducationCtrl';
    }

    public function index() {
        $data = array();
        $this->data['modalWindow'] = $this->load->view('admin/education/education-add-edit', $data, true);
        $this->data['maincontent'] = $this->load->view('admin/education/education-list', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getEducation($start = '', $noOfRecord = '') {
        $numRecords = $this->Modeleducation->getTotalRows();
        if ($numRecords) {
            $data['records'] = $this->Modeleducation->getRows($education_id = '', $start, $noOfRecord, 'id');
            $data['numrecords'] = $numRecords;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function addEducation() {
        $data = array();
        if (!empty($_POST['data'])) {
            $educationLevelName = trim($_POST['data']['educationLevelName']);
            $checkEducationExists=$this->Modeleducation->fieldValueCheck(array('educationLevelName'=>$educationLevelName,'deleted'=>'0'));
            if(!$checkEducationExists){
         
            $insertData = array(
                'educationLevelName' => $educationLevelName,
                'createdBy'=>$this->session->userdata('admin_user_id')
                
            );
            $insert = $this->Modeleducation->insert($insertData);

            if ($insert) {
                $data['educationData'] = $insertData;
                $data['data'] = $insert;
                $data['status'] = 'OK';
                $data['msg'] = 'Added Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Educational Level Name already exists'; 
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/education/education-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function editEducation() {
        $data = array();
      
        if (!empty($_POST['data'])) {
            $id = $_POST['data']['id'];
            $educationLevelName = trim($_POST['data']['educationLevelName']);
            $checkEducationExists=$this->Modeleducation->fieldValueCheck(array('educationLevelName'=>$educationLevelName,'deleted'=>'0','id!= '=>$id));
            if(!$checkEducationExists){
            
            $updateData = array(
                'educationLevelName' => $educationLevelName,
                'updatedBy'=>$this->session->userdata('admin_user_id'),
                'updatedOn'=>date('Y-m-d H:i:s')
            );
            $condition = array('id' => $id);
            $update = $this->Modeleducation->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Educational Level Name already exists';
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Edit';
            $this->data['maincontent'] = $this->load->view('admin/education/education-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }
    
    public function changeStatus() {
        //p($_POST);
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (!empty($id)) {
            $updateData = array(
                'status' => $status == '1' ? "0" : "1"
            );
            $condition = array('id' => $id);
            $update = $this->Modeleducation->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Status Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
            
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Invalid Data.';
            echo json_encode($data);
            exit;
        }
    }
    
    public function deleteEducation() {
        $data = array(); 
        if (!empty($_POST['id'])) {
            $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
            $condition = array('id' => $id);
            $delete = $this->Modeleducation->delete($condition);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = 'Deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }
    

}
