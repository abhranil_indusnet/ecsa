<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adminuser extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'adminuser';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        //if($this->session->userdata('admin_user_type_id')!=1){
            checkPermission($this->controller);
        //}
        $this->load->model('admin/Modeladminuser');
        $this->load->model('admin/common');
        $this->load->model('admin/menu');
        $this->data['ngController'] = 'adminUserController';

    }

    public function index() {
        $data = array();
        $data['adminMenuSlug'] = $this->controller;
        $data['usertype'] = $this->menu->get_typerows();
   
        $this->data['modalWindow'] = $this->load->view('admin/adminuser/user-add-edit', $data, true);
        $this->data['maincontent'] = $this->load->view('admin/adminuser/user-list', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getusers($start = '', $noOfRecord = '') {
        $params['conditions']=array('id !=' =>$this->session->userdata('admin_user_id'));
        
        if($this->session->userdata('admin_user_type_id')==3){
        $params['notInCondition']['adminUserType']=array(1,2);    
        }else{
        $params['conditions']['adminUserType !=']=1;    
        }


        $numRecords = $this->Modeladminuser->get_num_rows('',$params); 
        if ($numRecords) {
            
            $data['records'] = $this->Modeladminuser->get_rows($user_id = '', $start, $noOfRecord, 'createdOn',$params);

            $data['numrecords'] = $numRecords;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function addusers() {
        $data = array(); 
        $data['adminMenuSlug'] = $this->controller;
        $data['usertype'] = $this->menu->get_typerows();

        if (!empty($_POST['data'])) {
            $userData = array(
                'createdOn' => date('Y-m-d'),
                'adminFullName' => $_POST['data']['fullname'],
                'adminUsername' => $_POST['data']['useremail'],
                'adminPassword' => md5($_POST['data']['userpassword']),
                'adminPhoneNumber' => $_POST['data']['contactno'],
                'adminUserType' => $_POST['data']['usertype'],
                'status' => '1',
                'createdBy' => $this->session->userdata('admin_user_id'),
            );
            $checkEmail = $this->Modeladminuser->get_admin_email($_POST['data']['useremail']);
            if ($checkEmail > 0) {
                $data['status'] = 'emailExists';
                $data['msg'] = 'Email already exists.';
            } else {

                $insert = $this->Modeladminuser->insertindb(ADM_USERS, $userData);

                if ($insert) {

                $condition=array('adminTypeId'=>$userData['adminUserType']);
                $getDefaultPermission=$this->menu->get_default_permission($condition); 
                if(!empty($getDefaultPermission)){
                foreach ($getDefaultPermission as $val) {
                        $userPermission['adminMenuId']=$val['adminMenuId'];
                        $userPermission['adminUserId']=$insert;
                        $userPermission['adminMenuPermission']=$val['adminMenuPermission'];
                        $this->common->insert_data(ADM_USER_MENU_PERMISSION, $userPermission);
                    }    
                }
                    $data['userData'] = $userData;
                    $data['data'] = $insert;
                    $data['status'] = 'OK';
                    $data['msg'] = 'User added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }
            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/adminuser/user-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function editusers($user_id = "") {
        $data = array();
        $data['adminMenuSlug'] = $this->controller;
        $data['usertype'] = $this->menu->get_typerows();

        $data['userdata'] = $this->Modeladminuser->get_rows($user_id);

        if (!empty($_POST['data'])) {

            if ($_POST['data']['usertype'] == '') {
                $data['usereditdata'] = $this->Modeladminuser->get_rows($_POST['data']['usereditid']);
                $usertype = $data['usereditdata']['adminUserType'];
            } else {
                $usertype = $_POST['data']['usertype'];
            }

            if ($_POST['data']['userpassword'] == '******') {
                $userData = array(
                    'adminFullName' => $_POST['data']['fullname'],
                    'adminUsername' => $_POST['data']['useremail'],
                    'adminPhoneNumber' => $_POST['data']['contactno'],
                    'adminUserType' => $usertype,
                    'updatedBy'=>$this->session->userdata('admin_user_id'),
                    'updatedOn' => date('Y-m-d'),
                );
            } else {
                $userData = array(
                    'adminFullName' => $_POST['data']['fullname'],
                    'adminUsername' => $_POST['data']['useremail'],
                    'adminPassword' => md5($_POST['data']['userpassword']),
                    'adminPhoneNumber' => $_POST['data']['contactno'],
                    'adminUserType' => $usertype,
                    'updatedBy'=>$this->session->userdata('admin_user_id'),
                    'updatedOn' => date('Y-m-d'),
                );
            }

            $checkEmail = $this->Modeladminuser->get_admin_email($_POST['data']['useremail'], $_POST['data']['id']);
            if ($checkEmail > 0) {
                $data['status'] = 'emailExists';
                $data['msg'] = 'Email already exists.';
            } else {

                $condition = array('id' => $_POST['data']['id']);

                $update = $this->Modeladminuser->updatedb(ADM_USERS, $userData, $condition);
                if ($update) {
                    $data['data'] = $update;
                    $data['status'] = 'OK';
                    $data['msg'] = 'User updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }
            echo json_encode($data);
        } else {

            $data['btn'] = 'Edit';
            $this->data['maincontent'] = $this->load->view('admin/adminuser/user-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function deleteusers() {
        $data = array();
        if (!empty($_POST['id'])) {
            $condition = array('id' => $_POST['id']);
            $updatedData=array(
                         'deletedBy'=>$this->session->userdata('admin_user_id'),
                         'deleted'=>'1'
                         );
            $delete = $this->Modeladminuser->updatedb(ADM_USERS,$updatedData,$condition);
            if ($delete) {
            $condition = array('adminUserId' => $_POST['id']);
            $this->common->delete_data(ADM_USER_MENU_PERMISSION, $condition);

                $data['status'] = 'OK';
                $data['msg'] = 'Deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }

    public function userPermission() {
        $this->data['ngController'] = '';
        $data = array();
        $data['adminMenuSlug'] = $this->controller;
        $user_id = $this->uri->segment(4);
        $data['userData'] = $this->Modeladminuser->get_rows($user_id);

        if ($this->input->post('submit')) {
            //pr($_POST);
            $condition = array(
                'adminUserId' => $this->input->post('user_id')
            );

            $this->common->delete_data(ADM_USER_MENU_PERMISSION, $condition);

            $Menus = $this->menu->get_rows();
            $parentMenu=array();
            foreach ($Menus as $menu) {
             if(!empty($this->input->post($menu['adminMenuSlug'] . '_action'))){

                $actions = $this->input->post($menu['adminMenuSlug'] . '_action');
                $json_actions = json_encode($actions); 
                $user_data = array(
                    'adminUserId' => $this->input->post('user_id'),
                    'adminMenuId' => $menu['id'],
                    'adminMenuPermission' => $json_actions
                );
                //$user_data1[]=$user_data;
                 $insert = $this->common->insert_data(ADM_USER_MENU_PERMISSION, $user_data);
               }

                if(!empty($this->input->post($menu['adminMenuSlug'] . '_submenu_action'))){
                $actions = $this->input->post($menu['adminMenuSlug'] . '_submenu_action');
                $json_actions = json_encode($actions); 
                $user_data = array(
                    'adminUserId' => $this->input->post('user_id'),
                    'adminMenuId' => $menu['id'],
                    'adminMenuPermission' => $json_actions
                );
                //$user_data1[]=$user_data;
                 $insert = $this->common->insert_data(ADM_USER_MENU_PERMISSION, $user_data);
                 $parentMenu[]=$menu['adminMenuParentId'];
               }
               
             }
              $parentMenu= array_unique($parentMenu);
               foreach ($parentMenu as $key => $value) {
                 $user_data = array(
                    'adminUserId' => $this->input->post('user_id'),
                    'adminMenuId' => $value,
                    
                );
                  $insert = $this->common->insert_data(ADM_USER_MENU_PERMISSION, $user_data);
               } 
               
           
        }



        $data['user_id'] = $user_id;
        $fullMenu = array();
        $condition=array('id!='=>1);
        $parentMenus = $this->menu->get_parent_menus($condition);
      
        $menuItems = array();
       
        foreach ($parentMenus as $key=>$menu) {
            $permissions = $this->menu->get_user_permission($user_id, $menu['id']);
            $menu['adminMenuPermission'] = $permissions;
            $menuItems[$key] = $menu;

            $subMenu = $this->menu->get_child_menus($menu['id']);

            if (!empty($subMenu)) {
                foreach ($subMenu as $subs) {
                    $permissions = $this->menu->get_user_permission($user_id, $subs['id']);
                    $subs['adminMenuPermission'] = $permissions;
                    $menuItems[$key]['childMenu'][] = $subs;
                }
            }
        }
    
        //pr($menuItems);
        $data['fullMenu'] = $menuItems;



        $data['actionList'] = $this->common->get_data(ADM_ACTION_MASTER);

        $data['btn_label'] = 'Update';

        $this->data['maincontent'] = $this->load->view('admin/adminuser/user-permission', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    

}
