<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Dashboad class created by Rakesh
 */
class Settings extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'settings';
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        $this->load->model('admin/menu');
        $this->load->model('admin/common');
        $this->data['ngController'] = 'userTypeController';
    }

    public function index() {
        $data = array();


        $this->data['maincontent'] = $this->load->view('admin/settings/user-defaultUserTypeList', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getusertypes() {

        $records = $this->menu->get_typerows();
        if ($records) {
            $data['records'] = $records;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function defaultPermission() {
        $data = array();

        $user_type_id = $this->uri->segment(4);
        if ($this->input->post('submit')) {
         
            
            $condition = array(
                'adminTypeId' => $this->input->post('user_type')
            );

            $this->common->delete_data(ADM_USERTYPE_DEFAULT_PERMISSION, $condition);

            $parent_menus = $this->menu->get_rows();
            $parentMenu=array();
            foreach ($parent_menus as $menu) {
                if(!empty($this->input->post($menu['adminMenuSlug'] . '_action'))){
                $actions = $this->input->post($menu['adminMenuSlug'] . '_action');
                $json_actions = json_encode($actions); 
                $user_data = array(
                    'adminTypeId' => $this->input->post('user_type'),
                    'adminMenuId' => $menu['id'],
                    'adminMenuPermission' => $json_actions
                );
                //$user_data1[]=$user_data;
                 $insert = $this->common->insert_data(ADM_USERTYPE_DEFAULT_PERMISSION, $user_data);
               }

                if(!empty($this->input->post($menu['adminMenuSlug'] . '_submenu_action'))){
                $actions = $this->input->post($menu['adminMenuSlug'] . '_submenu_action');
                $json_actions = json_encode($actions); 
                $user_data = array(
                    'adminTypeId' => $this->input->post('user_type'),
                    'adminMenuId' => $menu['id'],
                    'adminMenuPermission' => $json_actions
                );
                //$user_data1[]=$user_data;
                 $insert = $this->common->insert_data(ADM_USERTYPE_DEFAULT_PERMISSION, $user_data);
                 $parentMenu[]=$menu['adminMenuParentId'];
               }
               

               
            }

              $parentMenu= array_unique($parentMenu);
               foreach ($parentMenu as $key => $value) {
                 $user_data = array(
                    'adminTypeId' => $this->input->post('user_type'),
                    'adminMenuId' => $value,
                    
                );
                  $insert = $this->common->insert_data(ADM_USERTYPE_DEFAULT_PERMISSION, $user_data);
                 } 
               
           
        }

        $data['user_type'] = $user_type_id;
        $full_menu = array();
        $parent_menus = $this->menu->get_parent_menus();
      
        $submenus = array();
        $full_childmenu = array();
        $childmenus = array();
        foreach ($parent_menus as $menu) {
            $permissions = $this->menu->get_permission($user_type_id, $menu['id']);
            $menu['adminMenuPermission'] = $permissions;
            $submenus[] = $menu;

            $submenu = $this->menu->get_child_menus($menu['id']);

            if (!empty($submenu)) {
                foreach ($submenu as $subs) {
                    $permissions = $this->menu->get_permission($user_type_id, $subs['id']);
                    $subs['adminMenuPermission'] = $permissions;
                    $childmenus[] = $subs;
                }
            }
        }
    
        
        $data['childsub'] = $childmenus;
        $full_childmenu[] = $subs;
        $data['full_childmenu'] = $full_childmenu;
       

        $full_menu[] = $menu;
        $data['full_menu'] = $submenus;
         //pr($data);
        $data['action_list'] = $this->common->get_data(ADM_ACTION_MASTER);

        $data['btn_label'] = 'Update';

        $this->data['maincontent'] = $this->load->view('admin/settings/user-defaultPermission', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

}
