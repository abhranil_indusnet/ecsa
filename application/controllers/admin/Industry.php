<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Industry extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'industry';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        $this->load->model('admin/Modelindustry');
        $this->load->model('admin/common');
        $this->load->model('admin/menu');
        $this->data['ngController'] = 'IndustryCtrl';
    }

    public function index() {
        $data = array();
        $this->data['modalWindow'] = $this->load->view('admin/industry/industry-add-edit', $data, true);
        $this->data['maincontent'] = $this->load->view('admin/industry/industry-list', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getindustry($start = '', $noOfRecord = '') {
        $numRecords = $this->Modelindustry->getTotalRows();
        if ($numRecords) {
            $data['records'] = $this->Modelindustry->getRows($industry_id = '', $start, $noOfRecord, 'id');
            $data['numrecords'] = $numRecords;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function addindustry() {
        $data = array();
        if (!empty($_POST['data'])) {
            $industryName = trim($_POST['data']['industryName']);
            $checkIndustryExists=$this->Modelindustry->fieldValueCheck(array('industryName'=>$industryName,'deleted'=>'0'));
            if(!$checkIndustryExists){
            $industryNameArr = array_map('trim', explode('/', $industryName));
            $industrySlugImplodeDash = implode("-",$industryNameArr);
            $industryNameArrSpace = array_map('trim', explode(" ", $industrySlugImplodeDash));
            $industrySlugImplode = implode("",$industryNameArrSpace);
            
            $industrySlug = strtolower($industrySlugImplode);
            $insertData = array(
                'industryName' => $industryName,
                'industrySlug' => $industrySlug,
            );
            $insert = $this->Modelindustry->insert($insertData);

            if ($insert) {
                $data['industryData'] = $insertData;
                $data['data'] = $insert;
                $data['status'] = 'OK';
                $data['msg'] = 'Added Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Industry Name already exists'; 
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/industry/industry-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function editindustry() {
        $data = array();
       // p($_POST,false);
        if (!empty($_POST['data'])) {
            $id = $_POST['data']['id'];
            $industryName = trim($_POST['data']['industryName']);
            $checkIndustryExists=$this->Modelindustry->fieldValueCheck(array('industryName'=>$industryName,'deleted'=>'0','id!= '=>$id));
            if(!$checkIndustryExists){
            
            $industryNameArr = array_map('trim', explode('/', $industryName));
            $industrySlugImplodeDash = implode("-",$industryNameArr);
            
            $industryNameArrSpace = array_map('trim', explode(" ", $industrySlugImplodeDash));
            $industrySlugImplode = implode("",$industryNameArrSpace);
            
            $industrySlug = strtolower($industrySlugImplode);
            
            $updateData = array(
                'industryName' => $industryName,
                'industrySlug' => $industrySlug,
            );
            $condition = array('id' => $id);
            $update = $this->Modelindustry->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Industry Name already exists';
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Edit';
            $this->data['maincontent'] = $this->load->view('admin/industry/industry-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }
    
    public function changeStatus() {
        //p($_POST);
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (!empty($id)) {
            $updateData = array(
                'status' => $status == '1' ? "0" : "1"
            );
            $condition = array('id' => $id);
            $update = $this->Modelindustry->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Status Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
            
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Invalid Data.';
            echo json_encode($data);
            exit;
        }
    }
    
    public function deleteindustry() {
        $data = array();
        if (!empty($_POST['id'])) {
            $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
            $condition = array('id' => $id);
            $delete = $this->Modelindustry->delete($condition);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = 'Deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }
    

}
