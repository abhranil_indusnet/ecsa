<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jobseeker extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'jobseeker';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/'); 
        $this->load->model('admin/Modeljobseeker');
        $this->load->model('admin/common');
        $this->load->model('admin/menu');
        $this->load->helper(array('form', 'url'));
        $this->data['ngController'] = 'jobseekerController';
    }

    public function index() {
        $data = array();
        
   
        $data['usertype'] = $this->menu->get_typerows(); 
      
        $this->data['modalWindow'] = $this->load->view('admin/jobseeker/jobseeker-add', $data, true);
        // $this->data['modalWindow'] ='';

        $this->data['maincontent'] = $this->load->view('admin/jobseeker/jobseeker-list', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getusers($start = '', $noOfRecord = '') {

        $numRecords = $this->Modeljobseeker->get_num_rows(); 
        if ($numRecords) {
            $data['records'] = $this->Modeljobseeker->getSeekerList($start, $noOfRecord);
            //print_r($data['records']);exit;
            //$this->Modeljobseeker->get_rows($user_id = '', $start, $noOfRecord, 'createdOn');
            $data['numrecords'] = $numRecords;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function addusersOld() {
        $data = array();

        $data['usertype'] = $this->menu->get_typerows();
        p($_FILES, false);
        p($_POST);
        if (!empty($_POST['data'])) {
            $checkEmail = $this->functions->emailExist(TYPE_JOB, $_POST['data']['useremail']);
            if ($checkEmail > 0) {
                $data['status'] = 'emailExists';
                $data['msg'] = 'Email already in use please use different one';
            } else {
                //p($_POST,FALSE);
                /* if ($_POST['data']['resumeName'] == 0) {
                  $data['status'] = 'ERR';
                  $data['msg'] = 'Please uplaod your resume first.';
                  } else if ($_POST['data']['resumeName'] == 1) {
                  $data['status'] = 'ERR';
                  $data['msg'] = 'Please uplaod resume in microsoft word, doc, docx or PDF format only.';
                  } else { */

                if ($_POST['data']['dob'] == '') {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Please Select Date of Birth.';
                } else {


                    $userData = array(
                        'userEmail' => $_POST['data']['useremail'],
                        'userPassword' => md5($_POST['data']['userpassword']),
                        'userTypeId' => '1',
                        'userCreatedOn' => CURRENTDATE,
                        'flag' => 'safe'
                    );
                    $userId = $this->Modeljobseeker->insertindb(USER_MASTER, $userData);

                    $jobseekerData = array(
                        'userId' => $userId,
                        'jobseekerFirstName' => $_POST['data']['firstname'],
                        'jobseekerLastName' => $_POST['data']['lastname'],
                        'jobseekerUsername' => $_POST['data']['useremail'],
                        //'jobseekerDob' => $_POST['data']['dateno'] . '-' . $_POST['data']['monthno'] . '-' . $_POST['data']['yearno'],
                        'jobseekerDob' => $_POST['data']['dob'],
                        'jobseekerMobile' => $_POST['data']['contactno'],
                        'jobseekerResume' => $_POST['data']['resumeName'],
                        'resumeUpdatedOn' => CURRENTDATE,
                        'createdOn' => CURRENTDATE,
                    );

                    $insert = $this->Modeljobseeker->insertindb(JOB_SEEKER_MASTER, $jobseekerData);

                    if ($insert) {
                        $data['data'] = $insert;
                        $data['status'] = 'OK';
                        $data['msg'] = 'Job Seeker has been added successfully.';
                    } else {
                        $data['status'] = 'ERR';
                        $data['msg'] = 'Some problem occurred, please try again.';
                    }
                }
            }
            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/jobseeker/jobseeker-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function addusers() {
        $data = array();
        //p($_FILES, false);p($_POST);
        if (!empty($_POST)) {
            $checkEmail = $this->functions->emailExist(TYPE_JOB, $_POST['useremail']);
            if ($checkEmail == 0) {
                $attachment = '' ; $attachmentName ='';
                if (isset($_POST['dob']) && $_POST['dob'] != 'undefined') {
                    if (isset($_FILES) && !empty($_FILES)) {
                        // file upload
                        $FileExtension = $_FILES['resumeupload']['type'];
                        $token = false;
                        switch ($FileExtension) {
                            case 'application/msword':
                            case 'application/rtf':
                            case 'application/pdf':
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                                $token = true;
                                break;
                            default:
                                $token = false;
                        }
                        if ($token === true) {
                            if ($_FILES['resumeupload']['size'] < RESUMESIZE) {
                                if ($_FILES['resumeupload']['name']) {
                                    list($attachment, $error) = $this->functions->uploadAll('resumeupload', UPLOAD . ABSPATH_JOBSEKER);
                                    $attachmentName = $_FILES['resumeupload']['name'];
                                }
                                $userData = array(
                                    'userEmail' => $_POST['useremail'],
                                    'userPassword' => md5($_POST['userpassword']),
                                    'userTypeId' => '1',
                                    'userCreatedOn' => CURRENTDATE,
                                    'flag' => 'safe'
                                );
                                $userId = $this->Modeljobseeker->insertindb(USER_MASTER, $userData);

                                $jobseekerData = array(
                                    'userId' => $userId,
                                    'jobseekerFirstName' => $_POST['firstname'],
                                    'jobseekerLastName' => $_POST['lastname'],
                                    'jobseekerDob' => $_POST['dob'],
                                    'jobseekerMobileCode' => $_POST['contactCode'],
                                    'jobseekerMobile' => $_POST['contactno'],
                                    'jobseekerResume' => $attachment,
                                    'jobseekerResumeFile' => $attachmentName,
                                    'resumeUpdatedOn' => CURRENTDATE,
                                    'createdOn' => CURRENTDATE,
                                );
                               
                                $insert = $this->Modeljobseeker->insertindb(JOB_SEEKER_MASTER, $jobseekerData);
                                 
                                if ($insert) {
                                    $data['data'] = $insert;
                                    $data['status'] = 'OK';
                                    $data['msg'] = 'Job Seeker has been added successfully.';
                                } else {
                                    $data['status'] = 'ERR';
                                    $data['msg'] = 'Some problem occurred, please try again.';
                                }
                            } else {
                                $data['status'] = 'ERR';
                                $data['msg'] = 'Resume size will be not more than 4MB.';
                            }
                        } else {
                            $data['status'] = 'ERR';
                            $data['msg'] = 'Resume format will be microsoft word, doc, docx or PDF only';
                        }
                    } else {
                        $data['status'] = 'ERR';
                        $data['msg'] = 'Please uplaod your resume.';
                    }
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Please Select Date of Birth.';
                }
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Email already exist!';
            }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/jobseeker/jobseeker-add', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function editusers($user_id = "") {
        $data = array();
        $data['usertype'] = $this->menu->get_typerows();

        $data['userdata'] = $this->Modeljobseeker->get_rows($user_id);

        if (!empty($_POST['data'])) {

            if ($_POST['data']['userpassword'] == '******') {
                $userData = array(
                    'jobseekerUpdatedOn' => date('Y-m-d H:i:s'),
                    'jobseekerFirstName' => $_POST['data']['firstname'],
                    'jobseekerLastName' => $_POST['data']['lastname'],
                    'jobseekerUsername' => $_POST['data']['useremail'],
                    //'jobseekerDob' => $_POST['data']['dateno'] . '-' . $_POST['data']['monthno'] . '-' . $_POST['data']['yearno'],
                    'dob' => $_POST['data']['dob'],
                    'jobseekerMobile' => $_POST['data']['contactno'],
                    'jobseekerResume' => $_POST['data']['resumeName'],
                    'jobseekerUpdatedOn' => CURRENTDATE
                );

                $jobuserData = array(
                    'userEmail' => $_POST['data']['useremail'],
                    'userUpdatedOn' => CURRENTDATE
                );
            } else {
                $userData = array(
                    'jobseekerUpdatedOn' => date('Y-m-d H:i:s'),
                    'jobseekerFirstName' => $_POST['data']['firstname'],
                    'jobseekerLastName' => $_POST['data']['lastname'],
                    'jobseekerUsername' => $_POST['data']['useremail'],
                    'jobseekerDob' => $_POST['data']['dateno'] . '-' . $_POST['data']['monthno'] . '-' . $_POST['data']['yearno'],
                    'jobseekerMobile' => $_POST['data']['contactno'],
                    'jobseekerResume' => $_POST['data']['resumeName'],
                    'jobseekerUpdatedOn' => CURRENTDATE
                );

                $jobuserData = array(
                    'userEmail' => $_POST['data']['useremail'],
                    'userPassword' => md5($_POST['data']['userpassword']),
                    'userUpdatedOn' => CURRENTDATE
                );
            }

            $checkEmail = $this->functions->emailExist(TYPE_JOB, $_POST['data']['useremail'], $_POST['data']['id']);
            //$this->Modeljobseeker->chkEmail($_POST['data']['useremail'], $_POST['data']['id']);
            if ($checkEmail > 0) {
                $data['status'] = 'emailExists';
                $data['msg'] = 'Email already in use please use different one';
            } else {

                $condition = array('id' => $_POST['data']['id']);
                $condition1 = array('userTypeId' => '1', 'referenceId' => $_POST['data']['id']);

                $update = $this->Modeljobseeker->updatedb('mrc_jobseekerMaster', $userData, $condition);
                if ($update) {
                    $this->Modeljobseeker->updatedb('mrc_userMaster', $jobuserData, $condition1);
                    $data['data'] = $update;
                    $data['status'] = 'OK';
                    $data['msg'] = 'User data has been updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
            }
            echo json_encode($data);
        } else {

            $data['btn'] = 'Edit';
            $this->data['maincontent'] = $this->load->view('admin/jobseeker/jobseeker-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function edit() {
        $data = array();

        $jobseeker_id = $this->uri->segment('4');

        $data['usertype'] = $this->menu->get_typerows();

        $data['jobseekerdata'] = $this->Modeljobseeker->getSeekerDetail($jobseeker_id); 
        $data['getIndustryList'] = $this->Modeljobseeker->get_rows('', '', '', 'id', INDUSTRY_MASTER);
        $data['getFunctionList'] = $this->Modeljobseeker->get_rows('', '', '', 'id', FUNCTION_MASTER);
        $data['workExperienceData'] = $this->functions->getMultiRow(WORK_EXPERIENCE, 'user_id', $jobseeker_id, '', '', '');
        //print_r($data['workExperienceData']);
        //$data['userAll'] = $this->functions->getListtable(USERMASTER, 'status', '1', '', '', 'company_id =' . $companyID . ' AND id <>' . $userID);
        //$data['indusctryrows'] = $this->employers->get_industryRows();$this->Modeljobseeker->get_rows($jobseeker_id, '', '', 'id', WORK_EXPERIENCE);
        //$data['countryrows'] = $this->common->get_data('mrc_countries');
        //echo "<pre>";print_r($data['employerdata']);
        /*
          if($user_id == '')
          {
          $user_id=$_POST['data']['usereditid'];
          }
         */


        $data['btn'] = 'Edit';

        $this->data['maincontent'] = $this->load->view('admin/jobseeker/jobseeker-edit', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

   public function dataEdit() {
        $jobseeker_id = $this->uri->segment('4');
        if ($jobseeker_id) {
            $data['personal'] = $this->Modeljobseeker->getSeekerDetail($jobseeker_id);    

             $data['seekerSkillData'] = $this->functions->getMultiRow(JOB_SEEKER_SKILL, 'user_id', $jobseeker_id, '', '', '');      //echo $data['seekerSkillData'];die;
              
            $data['getCountryList'] = $this->common->get_data(COUNTRY);
            $data['getRefernceList'] = $this->Modeljobseeker->get_rows('', '', '', '', JOBSEEKER_REFERENCE_MASTER);
            $data['getIndustryList'] = $this->Modeljobseeker->get_rows('', '', '', '', INDUSTRY_MASTER);
            $data['getAllRoleList'] = $this->Modeljobseeker->get_rows('', '', '', '', ROLE_MASTER);
            //print_r($data['getIndustryList']);
            $data['getFunctionList'] = $this->Modeljobseeker->get_rows('', '', '', 'id', FUNCTION_MASTER);
            $data['getskillList'] = $this->functions->getMultiRow(SKILL_MASTER, 'status', '1', '', '', '');
            $data['getLanguageList'] = $this->functions->getMultiRow(LANGUAGE_MASTER, 'status', '1', '', '', 'deleted = "0"');
            $data['getProficiencyList'] = unserialize(PROFICIENCYTYPE);
            $data['getGenderList'] = unserialize(GENDERTYPE);
            $data['getMaritalStatusList'] = unserialize(MARITAL_STATUS_TYPE);
            // /print_r($data['getLanguageList']);exit;
            //echo $this->db->last_query();
            //print_r($data['getskillList']);exit;
            $data['getqualificationList'] = $this->functions->getMultiRow(EDUCATION_LVL_MASTER, 'parentId', '0', '', '', '');

            $data['workExperienceData'] = $this->functions->getMultiRow(WORK_EXPERIENCE, 'user_id', $jobseeker_id, '', '', '');

            if (isset($data['personal']) && !empty($data['personal'])) {
                foreach ($data['personal'] as $key => $value) {

                    if ($key == 'jobseekerPreferedIndustry') {
                        $sub = array();
                        $child = array();
                        $exData = explode(',', $value);
                        foreach ($exData as $valueSub) {
                            if (isset($data['getIndustryList']) && !empty($data['getIndustryList'])) {
                                foreach ($data['getIndustryList'] as $valueInd) {
                                    if ($valueInd['id'] == $valueSub) {
                                        $child[] = array('id' => $valueSub, 'industryName' => $valueInd['industryName']);
                                    }
                                }
                            }
                        }
                        $data['personal']['jobseekerPreferedIndustry'] = $child;
                        unset($child);
                    }

                    if ($key == 'jobseekerPreferedRole') {
                        $sub = array();
                        $child = array();
                        $exData = explode(',', $value);
                        foreach ($exData as $valueSub) {
                            if (isset($data['getAllRoleList']) && !empty($data['getAllRoleList'])) {
                                foreach ($data['getAllRoleList'] as $valueInd) {
                                    if ($valueInd['id'] == $valueSub) {
                                        $child[] = array('id' => $valueSub, 'roleName' => $valueInd['roleName']);
                                    }
                                }
                            }
                        }
                        $data['personal']['jobseekerPreferedRole'] = $child;
                        unset($child);
                    }
                }
            }
            //print_r($data['personal']);exit;
            if (isset($data['workExperienceData']) && !empty($data['workExperienceData'])) {
                foreach ($data['workExperienceData'] as $key => $value) {
                    $data['getRoleList'][$key] = $this->getRoleName($value['job_function']);

                    if (isset($value['job_from']) && !empty($value['job_from'])) {
                        $datefrom = explode('-', $value['job_from']);
                        $data['workExperienceData'][$key]['monthnofrom'] = $datefrom[0];
                        $data['workExperienceData'][$key]['yearnofrom'] = $datefrom[1];
                    }
                    if (isset($value['job_to']) && !empty($value['job_to'])) {
                        $dateto = explode('-', $value['job_to']);
                        $data['workExperienceData'][$key]['monthnoto'] = $dateto[0];
                        $data['workExperienceData'][$key]['yearnoto'] = $dateto[1];
                    }
                }
            }
           /* $data['portfolioData'] = $this->functions->getMultiRow(PORTFOLIO, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['portfolioData']) && !empty($data['portfolioData'])) {
                foreach ($data['portfolioData'] as $key => $value) {

                    if (isset($value['project_from']) && !empty($value['project_from'])) {
                        $datefrom = explode('-', $value['project_from']);
                        $data['portfolioData'][$key]['monthnofrom'] = $datefrom[0];
                        $data['portfolioData'][$key]['yearnofrom'] = $datefrom[1];
                    }
                    if (isset($value['project_to']) && !empty($value['project_to'])) {
                        $dateto = explode('-', $value['project_to']);
                        $data['portfolioData'][$key]['monthnoto'] = $dateto[0];
                        $data['portfolioData'][$key]['yearnoto'] = $dateto[1];
                    }
                }
            }*/
            $data['educationDetailData'] = $this->functions->getMultiRow(EDUCATION_DETAIL, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['educationDetailData']) && !empty($data['educationDetailData'])) {
                foreach ($data['educationDetailData'] as $key => $value) {
                    $data['getSpecializationListEdit'][$key] = $this->getSpecializationList($value['qualification']);
                }
            }
            $data['certificationData'] = $this->functions->getMultiRow(CERTIFICATION, 'user_id', $jobseeker_id, '', '', '');
            $data['achievementData'] = $this->functions->getMultiRow(JOB_SEEKER_ACHIEVEMENT, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['seekerSkillData']) && !empty($data['seekerSkillData'])) {
                foreach ($data['seekerSkillData'] as $key => $value) {
                    $data['seekerSkillData'][$key]['skill'] = $this->functions->getSingleColumn(SKILL_MASTER, 'skillTitle', 'id', $value['skill'], '`status` = "1"');
                }
            }

           

            $data['seekerLanguageData'] = $this->functions->getMultiRow(JOB_SEEKER_LANGUAGE, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['seekerLanguageData']) && !empty($data['seekerLanguageData'])) {
                foreach ($data['seekerLanguageData'] as $key => $value) {
                    $data['seekerLanguageData'][$key]['languageId'] = $this->functions->getSingleColumn(LANGUAGE_MASTER, 'name', 'id', $value['languageId'], '`status` = "1"');
                    if (isset($data['getProficiencyList'])) {
                        foreach ($data['getProficiencyList'] as $subkey => $subvalue) {

                            if ($subkey == $value['proficiency']){
                                $data['seekerLanguageData'][$key]['proficiency'] = $subvalue;
                            }
                            
                        }
                    }
                }
            }
            if ($data) {
                $data['status'] = 'OK';
                $data['msg'] = 'Jobseeker data has been updated successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
        }
    }


    public function editSeeker() {
        
        if (!empty($_POST['personal'])) { 
            //print_r($_POST['personal']);exit();
            $emailExist = $this->functions->emailExist(TYPE_JOB,$_POST['personal']['userEmail'],$_POST['personal']['userId']);
            if($emailExist == '0'){

                $industryData = '';
                $roleData = '';
                if (isset($_POST['personal']['jobseekerPreferedIndustry']) && !empty($_POST['personal']['jobseekerPreferedIndustry'])) {
                    $industryIdArray = array();
                    foreach ($_POST['personal']['jobseekerPreferedIndustry'] as $value) {
                        $industryIdArray[] = $value['id'];
                    }
                    $industryData = implode(',', $industryIdArray);
                }

                if (isset($_POST['personal']['jobseekerPreferedRole']) && !empty($_POST['personal']['jobseekerPreferedRole'])) {
                    $roleIdArray = array();
                    foreach ($_POST['personal']['jobseekerPreferedRole'] as $value) {
                        $roleIdArray[] = $value['id'];
                    }
                    $roleData = implode(',', $roleIdArray);
                }
                //echo $roleData; exit;
                
                $userData = array(
                    'jobseekerFirstName' => $_POST['personal']['jobseekerFirstName'],
                    'jobseekerLastName' => $_POST['personal']['jobseekerLastName'],
                    'jobseekerCurrentLocation' => $_POST['personal']['jobseekerCurrentLocation'],
                    'jobseekerPreferedLocation' => $_POST['personal']['jobseekerPreferedLocation'],
                    'jobseekerExperienceYear' => $_POST['personal']['jobseekerExperienceYear'],
                    'jobseekerExperienceMonth' => $_POST['personal']['jobseekerExperienceMonth'],
                    'jobseekerDob' => $_POST['personal']['jobseekerDob'],
                    'jobseekerMobileCode' =>$_POST['personal']['jobseekerMobileCode'],
                    'jobseekerMobile' => $_POST['personal']['jobseekerMobile'],
                    'jobseekerSkypeId' => $_POST['personal']['jobseekerLinkedInprofile'],
                    'jobseekerLinkedInprofile' => $_POST['personal']['jobseekerLinkedInprofile'],
                    'jobseekerPreferedIndustry' => $industryData,
                    'jobseekerPreferedRole' => $roleData,
                    'jobseekerGender' => $_POST['personal']['jobseekerGender'],
                    'jobseekerMaritalStatus' => $_POST['personal']['jobseekerMaritalStatus'],
                    'jobseekerNoticePeriod' => $_POST['personal']['jobseekerNoticePeriod'],
                    'jobseekerNoticePeriodNeg' => $_POST['personal']['jobseekerNoticePeriodNeg'],
                    'jobseekerNoticePeriodSer' => $_POST['personal']['jobseekerNoticePeriodSer'],
                    'jobseekerReferenceId' => $_POST['personal']['jobseekerReferenceId'],
                    'jobseekerCurrentCountry' => $_POST['personal']['jobseekerCurrentCountry'],
                    'jobseekerNationality' => $_POST['personal']['jobseekerNationality'],
                    'jobseekerSubscribeEmail' => $_POST['personal']['jobseekerSubscribeEmail']
                );
                 
                //Usermaster Email Update
                $userMarter = array('userEmail' => $_POST['personal']['userEmail']);
                $updateUserMarter = $this->Modeljobseeker->updatedb(USER_MASTER, $userMarter, array('id' => $_POST['personal']['userId']));

                $condition = array('id' => $_POST['personal']['id']);
                $update = $this->Modeljobseeker->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
                if ($update) {
                    $editdata['data'] = $update;
                    $editdata['status'] = 'OK';
                    $editdata['msg'] = 'Jobseeker data has been updated successfully.';
                } else {
                    $editdata['status'] = 'ERR';
                    $editdata['msg'] = 'Some problem occurred, please try again.';
                }               
            }else{
                $editdata['status'] = 'ERR';
                $editdata['msg'] = 'Email Already Exist';                
            }
            
            //print_r($data);exit;
            echo json_encode($editdata);
        }
        if (!empty($_POST['hobbies'])) {
            $userData = array(
                'hobbies' => $_POST['hobbies']['hobbies'],
            );

            $condition = array('userId' => $_POST['hobbies']['userid']);
            $update = $this->Modeljobseeker->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
            if ($update) {
                $editdata['data'] = $update;
                $editdata['status'] = 'OK';
                $editdata['msg'] = 'Hobbies data has been updated successfully.';
            } else {
                $editdata['status'] = 'ERR';
                $editdata['msg'] = 'Some Hobbies occurred, please try again.';
            }
            //print_r($data);exit;
            echo json_encode($editdata);
        }
    }

       public function deletejobseeker() {
        $data = array();
       
        if (!empty($_POST['userId'])) {
            $conditionUserMaster = array('id' => $_POST['userId']);
            $deleteUserMaster = $this->Modeljobseeker->deletefromdb(USER_MASTER, $conditionUserMaster);

            $condition = array('userId' => $_POST['userId']);
            $delete = $this->Modeljobseeker->deletefromdb(JOB_SEEKER_MASTER, $condition);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = 'Jobseeker has been deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Jobseeker Details not Found'.$_POST['userId'];
        }
        echo json_encode($data);
    }

    public function deleteCommon() {
        $data = array();
        if (!empty($_POST['id']) && !empty($_POST['type'])) {
            if ($_POST['type'] == 'workexp') {
                $tableName = WORK_EXPERIENCE;
                $message = 'Work Experience';
            }
            if ($_POST['type'] == 'portfolio') {
                $tableName = PORTFOLIO;
                $message = 'Portfolio';
            }
            if ($_POST['type'] == 'eduDetail') {
                $tableName = EDUCATION_DETAIL;
                $message = 'Education Qualification';
            }
            if ($_POST['type'] == 'certify') {
                $tableName = CERTIFICATION;
                $message = 'Certification';
            }
            if ($_POST['type'] == 'achievement') {
                $tableName = JOB_SEEKER_ACHIEVEMENT;
                $message = 'Achievement';
            }
            if ($_POST['type'] == 'skill') {
                $tableName = JOB_SEEKER_SKILL;
                $message = 'Skill';
            }
            if ($_POST['type'] == 'language') {
                $tableName = JOB_SEEKER_LANGUAGE;
                $message = 'Language';
            }

            $condition = array('id' => $_POST['id']);
            $delete = $this->Modeljobseeker->deletefromdb($tableName, $condition);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = $message . ' has been deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = $message . ' Details not Found';
        }
        echo json_encode($data);
    }

    public function addeditSeekerWorkExp() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                if(isset($_POST['add']['job_from']) && isset($_POST['add']['job_to'])){
                    $dateValidate = $this->functions->validateFromToDate($_POST['add']['job_from'],$_POST['add']['job_to']);
                    if($dateValidate == true){
                        $addData = array(
                            'user_id' => $_POST['add']['userid'],
                            'designation' => $_POST['add']['designation'],
                            'company' => $_POST['add']['company'],
                            'industry' => $_POST['add']['industry'],
                            'job_function' => $_POST['add']['job_function'],
                            'job_role' => $_POST['add']['job_role'],
                            'job_from' => $_POST['add']['job_from'],
                            'job_to' => $_POST['add']['job_to']
                        );
                        //$insert = $this->Modeljobseeker->insertindb(WORK_EXPERIENCE, $addData);
                        $last_insert_id = $this->db->insert(WORK_EXPERIENCE, $addData);
                        $last_insert_id = $this->db->insert_id();
                        if ($last_insert_id > 0) {
                            $addData['id'] = $last_insert_id;
                            $data['data'] = $addData;
                            $data['status'] = 'OK';
                            $data['msg'] = 'Work Experience has been Added successfully.';
                        } else {
                            $data['status'] = 'ERR';
                            $data['msg'] = 'Some problem occurred, please try again.';
                        }
                    }else{
                        $data['status'] = 'ERR';
                        $data['msg'] = 'From date must be smaller than To date';
                    }    
                }else{
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Date Cannot be Blank';
                }    
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
            /* (START) Edit section */
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'designation' => $_POST['edit']['designation'],
                    'company' => $_POST['edit']['company'],
                    'industry' => $_POST['edit']['industry'],
                    'job_function' => $_POST['edit']['job_function'],
                    'job_role' => $_POST['edit']['job_role'],
                    'job_from' => $_POST['edit']['job_from'],
                    'job_to' => $_POST['edit']['job_to']
                );
                $dateValidate = $this->functions->validateFromToDate($_POST['edit']['job_from'],$_POST['edit']['job_to']);
                if($dateValidate == true){

                    $condition = array('id' => $_POST['edit']['id']);
                    $update = $this->Modeljobseeker->updatedb(WORK_EXPERIENCE, $updateData, $condition);
                    if ($update) {
                        $data['data'] = $updateData;
                        $data['status'] = 'OK';
                        $data['msg'] = 'Work Experience has been Updated successfully.';
                    } else {
                        $data['status'] = 'ERR';
                        $data['msg'] = 'Some problem occurred, please try again.';
                    }
                }else{
                    $data['status'] = 'ERR';
                    $data['msg'] = 'From date must be smaller than To date';
                }
                echo json_encode($data);
                exit;
            }

            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    public function addeditSeekerPortfolio() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'title' => $_POST['add']['title'],
                    'user_id' => $_POST['add']['userid'],
                    'client' => $_POST['add']['client'],
                    'projectUrl' => $_POST['add']['projectUrl'],
                    'role' => $_POST['add']['role'],
                    'projectDetail' => $_POST['add']['projectDetail'],
                    'project_from' => $_POST['add']['monthnofrom'] . '-' . $_POST['add']['yearnofrom'],
                    'project_to' => $_POST['add']['monthnoto'] . '-' . $_POST['add']['yearnoto']
                );
                //$insert = $this->Modeljobseeker->insertindb(WORK_EXPERIENCE, $addData);
                $last_insert_id = $this->db->insert(PORTFOLIO, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $addData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Portfolio has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
            /* (START) Edit section */
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'title' => $_POST['edit']['title'],
                    'user_id' => $_POST['edit']['user_id'],
                    'client' => $_POST['edit']['client'],
                    'projectUrl' => $_POST['edit']['projectUrl'],
                    'role' => $_POST['edit']['role'],
                    'projectDetail' => $_POST['edit']['projectDetail'],
                    'project_from' => $_POST['edit']['monthnofrom'] . '-' . $_POST['edit']['yearnofrom'],
                    'project_to' => $_POST['edit']['monthnoto'] . '-' . $_POST['edit']['yearnoto']
                        /* 'user_id' => $_POST['edit']['user_id'],
                          'designation' => $_POST['edit']['designation'],
                          'company' => $_POST['edit']['company'],
                          'industry' => $_POST['edit']['industry'],
                          'job_function' => $_POST['edit']['job_function'],
                          'job_role' => $_POST['edit']['job_role'],
                          'job_from' => $_POST['edit']['monthnofrom'] . '-' . $_POST['edit']['yearnofrom'],
                          'job_to' => $_POST['edit']['monthnoto'] . '-' . $_POST['edit']['yearnoto'] */
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modeljobseeker->updatedb(PORTFOLIO, $updateData, $condition);
                if ($update) {
                    $data['data'] = $updateData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Portfolio has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    public function addeditSeekerEduDetail() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'qualification' => $_POST['add']['qualification'],
                    'courseType' => $_POST['add']['courseType'],
                    'specialization' => $_POST['add']['specialization'],
                    'institute' => $_POST['add']['institute'],
                    'study' => $_POST['add']['study'],
                    'yearOfPass' => $_POST['add']['yearOfPass'],
                    'scored' => $_POST['add']['scored'],
                    'location' => $_POST['add']['location'],
                );
                //$insert = $this->Modeljobseeker->insertindb(WORK_EXPERIENCE, $addData);
                $last_insert_id = $this->db->insert(EDUCATION_DETAIL, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $addData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Education Detail has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
            /* (START) Edit section */
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'qualification' => $_POST['edit']['qualification'],
                    'courseType' => $_POST['edit']['courseType'],
                    'specialization' => $_POST['edit']['specialization'],
                    'institute' => $_POST['edit']['institute'],
                    'study' => $_POST['edit']['study'],
                    'yearOfPass' => $_POST['edit']['yearOfPass'],
                    'scored' => $_POST['edit']['scored'],
                    'location' => $_POST['edit']['location'],
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modeljobseeker->updatedb(EDUCATION_DETAIL, $updateData, $condition);
                if ($update) {
                    $data['data'] = $updateData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Education Detail has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    public function addeditSeekerCertification() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'certification' => $_POST['add']['certification'],
                    'certifyYear' => $_POST['add']['certifyYear']
                );
                //$insert = $this->Modeljobseeker->insertindb(WORK_EXPERIENCE, $addData);
                $last_insert_id = $this->db->insert(CERTIFICATION, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $addData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Certification has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
            /* (START) Edit section */
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'certification' => $_POST['edit']['certification'],
                    'certifyYear' => $_POST['edit']['certifyYear']
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modeljobseeker->updatedb(CERTIFICATION, $updateData, $condition);
                if ($update) {
                    $data['data'] = $updateData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Certification has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    public function addeditSeekerAchievement() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            //(START) Add section
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'achievement' => $_POST['add']['achievement'],
                    'createdOn' => CURRENTDATE
                );

                $last_insert_id = $this->db->insert(JOB_SEEKER_ACHIEVEMENT, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $addData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Achievement has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }
            //(START) Edit section
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'achievement' => $_POST['edit']['achievement'],
                    'updatedOn' => CURRENTDATE
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modeljobseeker->updatedb(JOB_SEEKER_ACHIEVEMENT, $updateData, $condition);
                if ($update) {
                    $data['data'] = $updateData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Achievement has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    public function addeditSeekerSkill() {
        if (!empty($_POST)) { 
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'skill' => $_POST['add']['skill'],
                    'experience' => $_POST['add']['experience']
                );
                //$insert = $this->Modeljobseeker->insertindb(WORK_EXPERIENCE, $addData);
                $last_insert_id = $this->db->insert(JOB_SEEKER_SKILL, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $addData;
                    $data['status'] = 'OK';
                    $data['msg'] = 'Skill has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    public function changeAppliedStudied() {
        /* (START) Edit section */
        if (isset($_POST['id']) && !empty($_POST['id'])) {

            $updateData = array(
                $_POST['type'] => $_POST['value'],
            );
            $condition = array('id' => $_POST['id']);

            $update = $this->Modeljobseeker->updatedb(JOB_SEEKER_SKILL, $updateData, $condition);
            if ($update) {
                $data['data'] = $updateData;
                $data['status'] = 'OK';
                $data['msg'] = 'Skill has been Updated successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;


            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
            echo json_encode($data);
            exit;
        }
    }

    public function addeditSeekerLanguage() {
        //print_r($_POST);exit;
        /* (START) Add section */
        if (isset($_POST['add']) && !empty($_POST['add'])) {
            $addData = array(
                'user_id' => $_POST['add']['userid'],
                'languageId' => $_POST['add']['languageId']
            );
            //$insert = $this->Modeljobseeker->insertindb(WORK_EXPERIENCE, $addData);
            $last_insert_id = $this->db->insert(JOB_SEEKER_LANGUAGE, $addData);
            $last_insert_id = $this->db->insert_id();
            if ($last_insert_id > 0) {
                $addData['id'] = $last_insert_id;
                $data['data'] = $addData;
                $data['status'] = 'OK';
                $data['msg'] = 'Language has been Added successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
            /* (END) Add section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    public function changeReadWriteSpeak() {
        /* (START) Edit section */
        if (isset($_POST['id']) && !empty($_POST['id'])) {

            $updateData = array(
                $_POST['type'] => $_POST['value'],
            );
            $condition = array('id' => $_POST['id']);

            $update = $this->Modeljobseeker->updatedb(JOB_SEEKER_LANGUAGE, $updateData, $condition);
            if ($update) {
                $data['data'] = $updateData;
                $data['status'] = 'OK';
                $data['msg'] = 'Language has been Updated successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;


            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
            echo json_encode($data);
            exit;
        }
    }

    

    public function getRoleName($functionId = "") {
        if (isset($_POST['functionId']) && !empty($_POST['functionId'])) {
            $functionId = $_POST['functionId'];
            $data['data'] = $this->functions->getMultiRow(ROLE_MASTER, 'function_id', $functionId, '', '', '');
            echo json_encode($data);
            exit;
        } else {
            if (!empty($functionId)) {
                $data = $this->functions->getMultiRow(ROLE_MASTER, 'function_id', $functionId, '', '', '');
            } else {
                $data = $this->Modeljobseeker->get_rows('', '', '', 'id', ROLE_MASTER);
            }
        }

        return $data;
    }

    public function getSpecializationList($id = "") {
        
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $data['data'] = $this->functions->getMultiRow(EDUCATION_LVL_MASTER, 'parentId', "$id", '', '', '');
            echo json_encode($data);
            exit;
        } else {
            if (!empty($id)) {
                $data = $this->functions->getMultiRow(EDUCATION_LVL_MASTER, 'parentId', $id, '', '', '');
            } else {
                $data = $this->Modeljobseeker->get_rows('', '', '', 'id', EDUCATION_LVL_MASTER);
            }
        }

        return $data;
    }

    public function deleteusers() {
        $data = array();
        $jobseekerId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (!empty($jobseekerId)) {
            $jobseeker_cond = array('id' => $jobseekerId);
            $user_cond = array('userTypeId' => '1', 'referenceId' => $jobseekerId);

            $resumefileName = $this->functions->getSingleColumn(JOB_SEEKER_MASTER, 'jobseekerResume', 'id', $jobseekerId, '`status` = "1"');

            //print_r($jobseekerdata);exit;
            if (!empty($resumefileName)) {
                // unlink resume
                $this->functions->unlinkFile(file_upload_absolute_path() . ABSPATH_JOBSEKER, $resumefileName);
            }
            $delete = $this->Modeljobseeker->deletefromdb(JOB_SEEKER_MASTER, $jobseeker_cond);
            $this->Modeljobseeker->deletefromdb(USER_MASTER, $user_cond);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = 'Job Seeker data has been deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }

    public function resumeUpload() { 
       /* print_r($_POST);die('end');*/
        $jobseekerId = filter_input(INPUT_POST, 'jobseeker_id', FILTER_VALIDATE_INT, INTEGER_FILTER);
      
        if (isset($_FILES['resumeupload']) && $_FILES['resumeupload']['error'] == 0) {

            // uploads image in the folder images
            $file_name = explode(".", $_FILES["resumeupload"]["name"]);
            $fileExt = end($file_name);
            $fileExtL = strtolower($fileExt);
            //echo $fileExtL; die('==');
            if (in_array($fileExtL, unserialize(RESUMETYPE))) {
                if ($_FILES['resumeupload']['size'] < RESUMESIZE) {
                    $time = substr(md5(time()), 0, 10);
                    $newfilename = $time . '.' . $fileExtL;

                    if (!is_dir(UPLOAD . ABSPATH_JOBSEKER)) {
                        mkdir(UPLOAD . ABSPATH_JOBSEKER, 0777, TRUE);
                    }
                    $upload_path = UPLOAD . ABSPATH_JOBSEKER . $newfilename;
                    move_uploaded_file($_FILES['resumeupload']['tmp_name'], $upload_path);
                    // give callback to your angular code with the image src name

                    if (!empty($jobseekerId)) { // edit section only
                        $resumefileName = $this->functions->getSingleColumn(JOB_SEEKER_MASTER, 'jobseekerResume', 'id', $jobseekerId, '`status` = "1"');

                       
                        if (!empty($resumefileName)) {
                            // unlink resume
                            $this->functions->unlinkFile(file_upload_absolute_path() . ABSPATH_JOBSEKER, $resumefileName);
                        }
                        // update new resume file
                        $userData = array(
                            'jobseekerResume' => $newfilename,
                            'resumeUpdatedOn' => CURRENTDATE,
                            'jobseekerResumeFile' =>  $_FILES["resumeupload"]["name"],
                        );
                        $condition = array('id' => $jobseekerId);
                        
                        $this->Modeljobseeker->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
                    }
                    echo json_encode($newfilename);
                } else {
                    echo 2;
                }
            } else {
                echo 1;
            }
        } else {
            echo 3;
        }
    }

    public function resumeUnlink() {
        //$data = array();
        if (!empty($_POST['filename'])) {
            // unlink old logo
            $path = file_upload_absolute_path() . ABSPATH_JOBSEKER;
            $res = $this->functions->unlinkFile($path, $_POST['filename']);
            echo $res;
        }
    }

    public function profileImageUpload() {
          /*print_r($_POST);
          print_r($_FILES);exit; */

        $jobseekerId = filter_input(INPUT_POST, 'userId', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (isset($_FILES['profileImage']) && $_FILES['profileImage']['error'] == 0) {
           
            // uploads image in the folder images
            $file_name = explode(".", $_FILES["profileImage"]["name"]);
            $fileExt = end($file_name);
            $fileExtL = strtolower($fileExt);
            //echo $fileExt; die('==');
            if (in_array($fileExtL, unserialize(IMAGETYPE))) {

                if ($_FILES['profileImage']['size'] <= PROFILEIMAGESIZE) {
                $time = substr(md5(time()), 0, 10);
                $newfilename = $time . '.' . $fileExtL;


                if (!is_dir(UPLOAD . ABSPATH_JOBSEKER_PROFILE)) {
                    mkdir(UPLOAD . ABSPATH_JOBSEKER_PROFILE, 0777, TRUE);
                }
                $upload_path = UPLOAD . ABSPATH_JOBSEKER_PROFILE . $newfilename;
                move_uploaded_file($_FILES['profileImage']['tmp_name'], $upload_path);
                // give callback to your angular code with the image src name

                if (!empty($jobseekerId)) {
                    $profileImageName = $this->functions->getSingleColumn(JOB_SEEKER_MASTER, 'jobseekerImage', 'id', $jobseekerId, '`status` = "1"');

                    //print_r($profileImageName);exit;
                    if (!empty($profileImageName)) {
                        // unlink resume
                        $this->functions->unlinkFile(file_upload_absolute_path() . ABSPATH_JOBSEKER_PROFILE, $profileImageName);
                    }
                    // update new resume file
                    $userData = array(
                        'jobseekerImage' => $newfilename,
                    );
                    $condition = array('id' => $jobseekerId);
                    $this->Modeljobseeker->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
                    echo '1'; // successfully
                }
            } 
            else{

                echo '4'; // Incorrect File size  
            }

           }else {
                echo '2'; // Incorrect File format
            }
        } else {
            echo '3'; // Some problem
        }
    }


    public function getCity($country_id = "") {
        $data = array();
        if (isset($_POST['country_id']) && !empty($_POST['country_id'])) {
            $country_id = $_POST['country_id'];
            $data['getCityList'] = $this->common->getCityFromCountry($country_id);
            echo json_encode($data);
            exit;
        }   
    }



}
