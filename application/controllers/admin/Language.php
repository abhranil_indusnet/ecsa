<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Language extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'language';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        $this->load->model('admin/Modellanguage');
        $this->load->model('admin/common');
        $this->load->model('admin/menu');
        $this->data['ngController'] = 'LanguageCtrl';
    }

    public function index() {
        $data = array();
        $this->data['modalWindow'] = $this->load->view('admin/language/language-add-edit', $data, true);
        $this->data['maincontent'] = $this->load->view('admin/language/language-list', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

    public function getLanguage($start = '', $noOfRecord = '') {
        $numRecords = $this->Modellanguage->getTotalRows();
        if ($numRecords) {
            $data['records'] = $this->Modellanguage->getRows($language_id = '', $start, $noOfRecord, 'id');
            $data['numrecords'] = $numRecords;
            $data['status'] = 'OK';
        } else {
            $data['records'] = array();
            $data['status'] = 'ERR';
        }
        echo json_encode($data);
    }

    public function addLanguage() {
        $data = array();
        if (!empty($_POST['data'])) {
            $name = trim($_POST['data']['name']);
            $checkLanguageExists=$this->Modellanguage->fieldValueCheck(array('name'=>$name,'deleted'=>'0'));
            if(!$checkLanguageExists){
         
            $insertData = array(
                'name' => $name,
                'created_by'=>$this->session->userdata('admin_user_id')
                
            );
            $insert = $this->Modellanguage->insert($insertData);

            if ($insert) {
                $data['languageData'] = $insertData;
                $data['data'] = $insert;
                $data['status'] = 'OK';
                $data['msg'] = 'Added Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Language already exists'; 
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Save';
            $this->data['maincontent'] = $this->load->view('admin/language/language-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }

    public function editLanguage() {
        $data = array();
       // p($_POST,false);
        if (!empty($_POST['data'])) {
            $id = $_POST['data']['id'];
            $name = trim($_POST['data']['name']);
            $checkLanguageExists=$this->Modellanguage->fieldValueCheck(array('name'=>$name,'deleted'=>'0','id!= '=>$id));
            if(!$checkLanguageExists){
            
            $updateData = array(
                'name' => $name,
                'updated_by'=>$this->session->userdata('admin_user_id'),
                'updated_on'=>date('Y-m-d H:i:s')
            );
            $condition = array('id' => $id);
            $update = $this->Modellanguage->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else{
                $data['status'] = 'ERR';
                $data['msg'] = 'Language Name already exists';
        }

            echo json_encode($data);
        } else {
            $data['btn'] = 'Edit';
            $this->data['maincontent'] = $this->load->view('admin/language/language-add-edit', $data, true);
            $this->load->view('admin/layout', $this->data);
        }
    }
    
    public function changeStatus() {
        //p($_POST);
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (!empty($id)) {
            $updateData = array(
                'status' => $status == '1' ? "0" : "1"
            );
            $condition = array('id' => $id);
            $update = $this->Modellanguage->update($updateData, $condition);
            if ($update) {
                $data['data'] = $update;
                $data['status'] = 'OK';
                $data['msg'] = 'Status Updated Successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
            
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Invalid Data.';
            echo json_encode($data);
            exit;
        }
    }
    
    public function deleteLanguage() {
        $data = array();
        if (!empty($_POST['id'])) {
            $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT, INTEGER_FILTER);
            $condition = array('id' => $id);
            $delete = $this->Modellanguage->delete($condition);
            if ($delete) {
                $data['status'] = 'OK';
                $data['msg'] = 'Deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }
    

}
