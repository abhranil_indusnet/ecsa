<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Users Management class created by RakeshT
 */
class Administrative extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'administrative';
        $this->load->library('form_validation');
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->load->model('admin/Modeladminuser');
        $this->load->model('admin/common');
        $this->load->library('email');
    }

    public function index() {
        //p($this->session->userdata);
        $UserID = $this->session->userdata('admin_user_id');
        if ($UserID) {
            redirect('/admin/dashboard');
            return true;
        } else {
            $this->login();
        }
    }

    public function login() {
        $data = array();

        if ($this->input->post('login_submit')) {
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');

            if ($this->form_validation->run() == TRUE) {
                $condition = array(
                    'adminUsername' => $this->input->post('username'),
                    'adminPassword' => md5($this->input->post('password')),
                    'status' => 1,
                    'deleted' => '0',
                ); 
                $check_login = $this->Modeladminuser->fieldValueCheck($condition);
                $validAdmin = $this->Modeladminuser->get_rows($check_login);
                //pr($validAdmin);
                if ($check_login) {
                    $admin_data = array(
                        'adminUserId' => $check_login
                    );
                    $this->Modeladminuser->insert($admin_data);

                    $this->session->set_userdata(array(
                        'admin_logged_in' => TRUE,
                        'admin_user_id' => $check_login,
                        'admin_user_type_id' => $validAdmin['adminUserType'],
                        'admin_username' => $validAdmin['adminFullName']
                    ));
                    $this->session->set_flashdata('success_msg', 'Welcome to administrative panel.');

                    //if ($this->session->userdata('last_page') != '')
                    //redirect($this->session->userdata('last_page'));
                    //redirect('/admin/dashboard');

                    $referer_path = get_cookie('admin_referer_path', TRUE);
                    delete_cookie("admin_referer_path");

                    //echo $referer_path ; die();
                    if ($referer_path) {
                        redirect(base_url() . 'admin/' . $referer_path); //  redirect(base_url($referer_path));
                    } else {
                        redirect(base_url() . 'admin/dashboard/'); //redirect(base_url("dashboard/"));
                    }
                    exit;
                    return true;
                } else {
                    $this->session->set_flashdata('error_msg', 'Wrong username or password, please try again.');
                }
            }
        }
     
        $this->data['maincontent'] = $this->load->view('admin/administrative/login', $data, true);
        $this->load->view('admin/layout-login', $this->data);
    }

    public function forgetPassword() {
        $data = array();


        $this->data['maincontent'] = $this->load->view('admin/administrative/reset-password', $data, true);
        $this->load->view('admin/layout-login', $this->data);
    }

    public function checkEmail($fpass = false) {
        $data = array();

        $condition = array(
            'adminUsername' => $fpass,
            'status' => 1
        );

        echo $check_login = $this->Modeladminuser->fieldValueCheck($condition);
        if ($check_login) {
            $admindata = array(
                'adminaccessToken' => md5(rand(99999, 999999))
            );

            $this->common->update_data(ADM_USERS, $admindata, $condition);

            $this->email->from('info@ecsa.com', 'Your Name');
            $this->email->to('ankur.biswas@indusnet.co.in');

            $this->email->subject('ECSA - Password Recovery Mail System');
            $this->email->message('Hi, ' . $fpass . "\n\r" . "\n\r" . 'Welcome to password recovery system' . "\n\r" . 'Please click on the below link and enter new password' . "\n\r" . "\n\r" . base_url() . 'admin/administrative/newPassword/' . $admindata['adminaccessToken'] . '/' . $check_login . "\n\r" . "\n\r" . "thanks and regards" . "\n\r" . "Team ECSA");

            $this->email->send();
        }
    }

    public function newPassword() {
        $data = array();
        $adminaccessToken = $this->uri->segment(4);
        $adminUserId = $this->uri->segment(5);

        $getToken = $this->Modeladminuser->get_rows($adminUserId);
        $password = $this->input->post('password');
        $update = array(
            'adminPassword' => md5($password)
        );

        $condition = array(
            'adminaccessToken' => $adminaccessToken
        );

        if ($getToken['adminaccessToken'] == $adminaccessToken) {
            if ($this->input->post('login_submit')) {
               

                $user_update = $this->Modeladminuser->update_forgetPassword($update, $condition);

                if ($user_update) {
                    $admindata = array(
                        'adminaccessToken' => ''
                    );

                    $cond = array(
                        'id' => $adminUserId
                    );

                    $this->common->update_data(ADM_USERS, $admindata, $cond);

                    $this->session->set_flashdata('success_msg', 'Password has been changed successfully');
                    redirect(base_url() . 'admin');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong...');
                    redirect(base_url() . 'admin');
                }
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Sorry the link has been expired! Please try again');
            redirect(base_url() . 'admin');
        }

        $data['action'] = base_url() . 'admin/administrative/newPassword/' . $adminaccessToken . '/' . $adminUserId;

        $this->data['maincontent'] = $this->load->view('admin/administrative/forgot-password', $data, true);
        $this->load->view('admin/layout-login', $this->data);
    }

    public function logout() {
        $update = array(
            'adminUserOutTime' => date('Y-m-d H:i:s')
        );

        $condition = array(
            'adminUserId' => $this->session->userdata('admin_user_id')
        );

        $user_update = $this->Modeladminuser->update($update, $condition);

        $this->session->unset_userdata('admin_logged_in');
        $this->session->unset_userdata('admin_user_id');
        $this->session->unset_userdata('admin_username');
        $this->session->unset_userdata('admin_user_type_id');
        $this->session->set_userdata('success_msg', 'You are successfully logged out from administrative panel.');
        redirect(base_url() . 'admin');
    }

}
