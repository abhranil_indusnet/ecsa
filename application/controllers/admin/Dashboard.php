<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Dashboad class created by Nitya
 */
class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->controller = 'dashboard';
        $this->load->library('admin_init_elements');
        $this->admin_init_elements->init_elements();
        $this->admin_init_elements->is_admin_loggedin($this->controller . '/');
        $this->data['ngController'] = '';
    }

    public function index() {
        $data = array();
        $this->data['maincontent'] = $this->load->view('admin/dashboard/index', $data, true);
        $this->load->view('admin/layout', $this->data);
    }

}
