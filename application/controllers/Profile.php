<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('public_init_elements');
        $this->public_init_elements->init_elements();
        $this->load->model('Modelprofile');
        $this->data['ngController'] = 'profileController';
    }

    public function index() {
        $data = array();
        $data['profileId'] = $this->uri->segment('2');
        $data['getIndustryList'] = $this->functions->getMultiRow(INDUSTRY_MASTER, 'status', '1', '', '', '');
        $data['getFunctionList'] = $this->functions->getMultiRow(FUNCTION_MASTER, 'status', '1', '', '', '');
        $data['workExperienceData'] = $this->functions->getMultiRow(WORK_EXPERIENCE, 'user_id', $data['profileId'], '', '', '');
        $this->data['maincontent'] = $this->load->view('profile/index', $data, true);
        $this->load->view('layout', $this->data);
    }

    public function profileDetail() {
        $jobseeker_id = $this->uri->segment('3');
        if ($jobseeker_id) {
            
            $data['personal'] = $this->Modelprofile->getSeekerDetail($jobseeker_id);
            //$this->functions->getSingleColumn(JOB_SEEKER_MASTER, '*', 'userId', $jobseeker_id, '');         
            $data['getIndustryList'] = $this->Modelprofile->get_rows('', '', '', '', INDUSTRY_MASTER);
            $data['getAllRoleList'] = $this->Modelprofile->get_rows('', '', '', '', ROLE_MASTER);
            $data['getFunctionList'] = $this->Modelprofile->get_rows('', '', '', 'id', FUNCTION_MASTER);
            $data['getskillList'] = $this->functions->getMultiRow(SKILL_MASTER, 'status', '1', '', '', '');
            $data['getLanguageList'] = $this->functions->getMultiRow(LANGUAGE_MASTER, 'status', '1', '', '', 'deleted = "0"');
            $data['getProficiencyList'] = unserialize(PROFICIENCYTYPE);
            $data['getGenderList'] = unserialize(GENDERTYPE);
            // /print_r($data['getLanguageList']);exit;
            //echo $this->db->last_query();
            //print_r($data['getskillList']);exit;
            $data['getqualificationList'] = $this->functions->getMultiRow(EDUCATION_LVL_MASTER, 'parentId', '0', '', '', '');
            $data['workExperienceData'] = $this->functions->getMultiRow(WORK_EXPERIENCE, 'user_id', $jobseeker_id, '', '', '');
            //print_r($data['personal']);exit;
            if (isset($data['personal']) && !empty($data['personal'])) {
                foreach ($data['personal'] as $key => $value) {

                    if ($key == 'resumeUpdatedOn') {
                        if (isset($value) && ($value != '0000-00-00')) {
                            $data['personal']['resumeUpdatedOn'] = date("jS F, Y", strtotime($value));
                        }
                    }

                    if ($key == 'jobseekerPreferedIndustry') {
                        $sub = array();
                        $child = array();
                        $exData = explode(',', $value);
                        foreach ($exData as $valueSub) {
                            if (isset($data['getIndustryList']) && !empty($data['getIndustryList'])) {
                                foreach ($data['getIndustryList'] as $valueInd) {
                                    if ($valueInd['id'] == $valueSub) {
                                        $child[] = array('id' => $valueSub, 'industryName' => $valueInd['industryName']);
                                    }
                                }
                            }
                        }
                        $data['personal']['jobseekerPreferedIndustry'] = $child;
                        unset($child);
                    }

                    if ($key == 'jobseekerPreferedRole') {
                        $sub = array();
                        $child = array();
                        $exData = explode(',', $value);
                        foreach ($exData as $valueSub) {
                            if (isset($data['getAllRoleList']) && !empty($data['getAllRoleList'])) {
                                foreach ($data['getAllRoleList'] as $valueInd) {
                                    if ($valueInd['id'] == $valueSub) {
                                        $child[] = array('id' => $valueSub, 'roleName' => $valueInd['roleName']);
                                    }
                                }
                            }
                        }
                        $data['personal']['jobseekerPreferedRole'] = $child;
                        unset($child);
                    }
                    if ($key == 'jobseekerExperience') {
                        $jobseekerExperience = explode('-', $value);
                        if(!empty($jobseekerExperience)){
                            $data['personal']['expYear'] = $jobseekerExperience[0];
                            $data['personal']['expmonth'] = $jobseekerExperience[1];
                            
                        }
                    }

                    
                }
            }
            //print_r($data['personal']);exit;
            if (isset($data['workExperienceData']) && !empty($data['workExperienceData'])) {
                foreach ($data['workExperienceData'] as $key => $value) {
                    $data['getRoleList'][$key] = $this->getRoleName($value['job_function']);

                    if (isset($value['job_from']) && ($value['job_from'] != '0000-00-00')) {
                        $data['workExperienceData'][$key]['job_from_txt'] = date("jS F, Y", strtotime($value['job_from'])); 
                    }
                    if (isset($value['job_to']) && ($value['job_to'] != '0000-00-00')) {
                        $data['workExperienceData'][$key]['job_to_txt'] = date("jS F, Y", strtotime($value['job_to']));
                    }
                    if(isset($value['industry']) && !empty($value['industry'])){
                        $data['workExperienceData'][$key]['industry_name'] = $this->functions->getSingleColumn(INDUSTRY_MASTER, 'industryName', 'id', $value['industry'], '`status` = "1"');
                    }
                    if(isset($value['job_function']) && !empty($value['job_function'])){
                        $data['workExperienceData'][$key]['function_name'] = $this->functions->getSingleColumn(FUNCTION_MASTER, 'functionName', 'id', $value['job_function'], '`status` = "1"');
                    }
                    if(isset($value['job_role']) && !empty($value['job_role'])){
                        $data['workExperienceData'][$key]['role_name'] = $this->functions->getSingleColumn(ROLE_MASTER, 'roleName', 'id', $value['job_role'], '`status` = "1"');
                    }
                }
            }
            $data['portfolioData'] = $this->functions->getMultiRow(PORTFOLIO, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['portfolioData']) && !empty($data['portfolioData'])) {
                foreach ($data['portfolioData'] as $key => $value) {

                    if (isset($value['project_from']) && !empty($value['project_from'])) {
                        $datefrom = explode('-', $value['project_from']);
                        $data['portfolioData'][$key]['monthnofrom'] = $datefrom[0];
                        $data['portfolioData'][$key]['yearnofrom'] = $datefrom[1];
                    }
                    if (isset($value['project_to']) && !empty($value['project_to'])) {
                        $dateto = explode('-', $value['project_to']);
                        $data['portfolioData'][$key]['monthnoto'] = $dateto[0];
                        $data['portfolioData'][$key]['yearnoto'] = $dateto[1];
                    }
                }
            }
            $data['educationDetailData'] = $this->functions->getMultiRow(EDUCATION_DETAIL, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['educationDetailData']) && !empty($data['educationDetailData'])) {
                foreach ($data['educationDetailData'] as $key => $value) {
                    $data['getSpecializationListEdit'][$key] = $this->getSpecializationList($value['qualification']);
                    if(isset($value['qualification']) && !empty($value['qualification'])){
                        $data['educationDetailData'][$key]['qualificationName'] = $this->functions->getSingleColumn(EDUCATION_LVL_MASTER, 'educationLevelName', 'id', $value['qualification'], '`status` = "1"');
                    }
                    if(isset($value['qualification']) && !empty($value['qualification'])){
                        $data['educationDetailData'][$key]['specializationName'] = $this->functions->getSingleColumn(EDUCATION_LVL_MASTER, 'educationLevelName', 'id', $value['specialization'], '`status` = "1"');
                    }
                }
            }
            $data['certificationData'] = $this->functions->getMultiRow(CERTIFICATION, 'user_id', $jobseeker_id, '', '', '');
            $data['achievementData'] = $this->functions->getMultiRow(JOB_SEEKER_ACHIEVEMENT,'user_id',$jobseeker_id,'','','');
            $data['seekerSkillData'] = $this->functions->getMultiRow(JOB_SEEKER_SKILL, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['seekerSkillData']) && !empty($data['seekerSkillData'])) {
                foreach ($data['seekerSkillData'] as $key => $value) {
                    $data['seekerSkillData'][$key]['skill'] = $this->functions->getSingleColumn(SKILL_MASTER, 'skillTitle', 'id', $value['skill'], '`status` = "1"');
                }
            }
            $data['seekerLanguageData'] = $this->functions->getMultiRow(JOB_SEEKER_LANGUAGE, 'user_id', $jobseeker_id, '', '', '');
            if (isset($data['seekerLanguageData']) && !empty($data['seekerLanguageData'])) {
                foreach ($data['seekerLanguageData'] as $key => $value) {
                    $data['seekerLanguageData'][$key]['languageName'] = $this->functions->getSingleColumn(LANGUAGE_MASTER, 'name', 'id', $value['languageId'], '`status` = "1"');
                    if (isset($data['getProficiencyList'])) {
                        foreach ($data['getProficiencyList'] as $subkey => $subvalue) {
                            if ($subkey == $value['proficiency']){
                                $data['seekerLanguageData'][$key]['proficiency'] = $subvalue;
                            }
                            /*if (($subkey == $value['proficiency']) && ($subkey > 0)) {
                                $data['seekerLanguageData'][$key]['proficiency'] = $subvalue;
                            } else {
                                $data['seekerLanguageData'][$key]['proficiency'] = '--';
                            }*/
                        }
                    }
                }
            }
            if ($data) {
                $data['status'] = 'OK';
                $data['msg'] = 'user data';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
        }
    }

    public function editPersonailDetail() {
        //print_r($_POST);exit;
        if (!empty($_POST['personal'])) {
            //print_r($_POST['personal']);

            $emailExist = $this->functions->emailExist(TYPE_JOB,$_POST['personal']['userEmail'],$_POST['personal']['userId']);
            if($emailExist == '0'){

                $industryData = '';
                $roleData = '';
                if (isset($_POST['personal']['jobseekerPreferedIndustry']) && !empty($_POST['personal']['jobseekerPreferedIndustry'])) {
                    $industryIdArray = array();
                    foreach ($_POST['personal']['jobseekerPreferedIndustry'] as $value) {
                        $industryIdArray[] = $value['id'];
                    }
                    $industryData = implode(',', $industryIdArray);
                }

                if (isset($_POST['personal']['jobseekerPreferedRole']) && !empty($_POST['personal']['jobseekerPreferedRole'])) {
                    $roleIdArray = array();
                    foreach ($_POST['personal']['jobseekerPreferedRole'] as $value) {
                        $roleIdArray[] = $value['id'];
                    }
                    $roleData = implode(',', $roleIdArray);
                }
                //echo $roleData; exit;
                $userData = array(
                    'jobseekerFirstName' => $_POST['personal']['jobseekerFirstName'],
                    'jobseekerLastName' => $_POST['personal']['jobseekerLastName'],
                    'jobseekerCurrentLocation' => $_POST['personal']['jobseekerCurrentLocation'],
                    'jobseekerPreferedLocation' => $_POST['personal']['jobseekerPreferedLocation'],
                    'jobseekerDesignation' => $_POST['personal']['jobseekerDesignation'],
                    'jobseekerCurrentcompany' => $_POST['personal']['jobseekerCurrentcompany'],
                    'jobseekerExperienceYear' => $_POST['personal']['jobseekerExperienceYear'],
                    'jobseekerExperienceMonth' => $_POST['personal']['jobseekerExperienceMonth'],
                    'jobseekerCTClakh' => $_POST['personal']['jobseekerCTClakh'],
                    'jobseekerCTCthousand' => $_POST['personal']['jobseekerCTCthousand'],
                    'jobseekerMobile' => $_POST['personal']['jobseekerMobile'],
                    'jobseekerPreferedIndustry' => $industryData,
                    'jobseekerPreferedRole' => $roleData,
                    'jobseekerGender' => $_POST['personal']['jobseekerGender'],
                    'jobseekerNoticePeriod' => $_POST['personal']['jobseekerNoticePeriod'],
                    'jobseekerNoticePeriodNeg' => $_POST['personal']['jobseekerNoticePeriodNeg'],
                    'jobseekerNoticePeriodSer' => $_POST['personal']['jobseekerNoticePeriodSer'],
                    'hobbies' => $_POST['personal']['hobbies']
                );
                //Usermaster email change
                $userMarter = array('userEmail' => $_POST['personal']['userEmail']);
                $updateUserMarter = $this->Modelprofile->updatedb(USER_MASTER, $userMarter, array('id' => $_POST['personal']['userId']));

                $condition = array('id' => $_POST['personal']['id']);
                $update = $this->Modelprofile->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
                if ($update) {
                    $editdata['data'] = $this->fetchPersonalDetail($_POST['personal']['userId']);
                    $editdata['status'] = 'OK';
                    $editdata['msg'] = 'Jobseeker data has been updated successfully.';
                } else {
                    $editdata['status'] = 'ERR';
                    $editdata['msg'] = 'Some problem occurred, please try again.';
                }
            }else{
                $editdata['status'] = 'ERR';
                $editdata['msg'] = 'Email Already Exist';
            }
            
            //print_r($data);exit;
            echo json_encode($editdata);
        }
        if (!empty($_POST['hobbies'])) {
            $userData = array(
                'hobbies' => $_POST['hobbies']['hobbies'],
            );

            $condition = array('id' => $_POST['hobbies']['userid']);
            $update = $this->Modeljobseeker->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
            if ($update) {
                $editdata['data'] = $update;
                $editdata['status'] = 'OK';
                $editdata['msg'] = 'Hobbies data has been updated successfully.';
            } else {
                $editdata['status'] = 'ERR';
                $editdata['msg'] = 'Some Hobbies occurred, please try again.';
            }
            //print_r($data);exit;
            echo json_encode($editdata);
        }
    }

    function fetchPersonalDetail($jobseeker_id = ''){
        //print_r($_POST);exit();
        if(isset($_POST['id']) && !empty($_POST['id'])){
            $jobseeker_id = $_POST['id'];
        }
        $data['personal'] = $this->Modelprofile->getSeekerDetail($jobseeker_id); 
        $data['getIndustryList'] = $this->Modelprofile->get_rows('', '', '', '', INDUSTRY_MASTER);
        $data['getAllRoleList'] = $this->Modelprofile->get_rows('', '', '', '', ROLE_MASTER);
        if (isset($data['personal']) && !empty($data['personal'])) {
            foreach ($data['personal'] as $key => $value) {

                if ($key == 'resumeUpdatedOn') {
                    if (isset($value) && ($value != '0000-00-00')) {
                        $data['personal']['resumeUpdatedOn'] = date("jS F, Y", strtotime($value));
                    }
                }

                if ($key == 'jobseekerPreferedIndustry') {
                    $sub = array();
                    $child = array();
                    $exData = explode(',', $value);
                    foreach ($exData as $valueSub) {
                        if (isset($data['getIndustryList']) && !empty($data['getIndustryList'])) {
                            foreach ($data['getIndustryList'] as $valueInd) {
                                if ($valueInd['id'] == $valueSub) {
                                    $child[] = array('id' => $valueSub, 'industryName' => $valueInd['industryName']);
                                }
                            }
                        }
                    }
                    $data['personal']['jobseekerPreferedIndustry'] = $child;
                    unset($child);
                }

                if ($key == 'jobseekerPreferedRole') {
                    $sub = array();
                    $child = array();
                    $exData = explode(',', $value);
                    foreach ($exData as $valueSub) {
                        if (isset($data['getAllRoleList']) && !empty($data['getAllRoleList'])) {
                            foreach ($data['getAllRoleList'] as $valueInd) {
                                if ($valueInd['id'] == $valueSub) {
                                    $child[] = array('id' => $valueSub, 'roleName' => $valueInd['roleName']);
                                }
                            }
                        }
                    }
                    $data['personal']['jobseekerPreferedRole'] = $child;
                    unset($child);
                }
                if ($key == 'jobseekerExperience') {
                    $jobseekerExperience = explode('-', $value);
                    if(!empty($jobseekerExperience)){
                        $data['personal']['expYear'] = $jobseekerExperience[0];
                        $data['personal']['expmonth'] = $jobseekerExperience[1];
                        
                    }
                }

                
            }
        }
        if(isset($_POST['id']) && !empty($_POST['id'])){
            $data['status'] = 'OK';
            echo json_encode($data);
            exit;
        }

        return $data;
    }

    public function addeditSeekerWorkExp() {
        // /print_r($_POST);exit;
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                //Validation Checking 
                if(isset($_POST['add']['job_from']) && isset($_POST['add']['job_to'])){
                    $dateValidate = $this->functions->validateFromToDate($_POST['add']['job_from'],$_POST['add']['job_to']);
                    if($dateValidate == true){
                        $addData = array(
                            'user_id' => $_POST['add']['userid'],
                            'designation' => $_POST['add']['designation'],
                            'company' => $_POST['add']['company'],
                            'industry' => $_POST['add']['industry'],
                            'job_function' => $_POST['add']['job_function'],
                            'job_role' => $_POST['add']['job_role'],
                            'job_from' => $_POST['add']['job_from'],
                            'job_to' => $_POST['add']['job_to']
                        );

                        //$insert = $this->Modeljobseeker->insertindb(WORK_EXPERIENCE, $addData);
                        $last_insert_id = $this->db->insert(WORK_EXPERIENCE, $addData);
                        $last_insert_id = $this->db->insert_id();
                        if ($last_insert_id > 0) {
                            $addData['id'] = $last_insert_id;
                            $data['data'] = $this->fetchWorkExperienceData($_POST['add']['userid']);
                            $data['status'] = 'OK';
                            $data['msg'] = 'Work Experience has been Added successfully.';
                        } else {
                            $data['status'] = 'ERR';
                            $data['msg'] = 'Some problem occurred, please try again.';
                        }  
                    }else{
                        $data['status'] = 'ERR';
                        $data['msg'] = 'From date must be smaller than To date';
                    }      
                }else{
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Date Cannot be Blank';
                }
                
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
            /* (START) Edit section */
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'designation' => $_POST['edit']['designation'],
                    'company' => $_POST['edit']['company'],
                    'industry' => $_POST['edit']['industry'],
                    'job_function' => $_POST['edit']['job_function'],
                    'job_role' => $_POST['edit']['job_role'],
                    'job_from' => $_POST['edit']['job_from'],
                    'job_to' => $_POST['edit']['job_to']
                );
                $dateValidate = $this->functions->validateFromToDate($_POST['edit']['job_from'],$_POST['edit']['job_to']);
                if($dateValidate == true){
                    $condition = array('id' => $_POST['edit']['id']);
                    $update = $this->Modelprofile->updatedb(WORK_EXPERIENCE, $updateData, $condition);
                    if ($update) {
                        $data['data'] = $this->fetchWorkExperienceData($_POST['edit']['user_id']);
                        $data['status'] = 'OK';
                        $data['msg'] = 'Work Experience has been Updated successfully.';
                    } else {
                        $data['status'] = 'ERR';
                        $data['msg'] = 'Some problem occurred, please try again.';
                    }
                }else{
                    $data['status'] = 'ERR';
                    $data['msg'] = 'From date must be smaller than To date';
                }
                
                echo json_encode($data);
                exit;
            }

            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    function fetchWorkExperienceData($userId){

        $data['workExperienceData'] = $this->functions->getMultiRow(WORK_EXPERIENCE, 'user_id', $userId, '', '', '');
        
        if (isset($data['workExperienceData']) && !empty($data['workExperienceData'])) {
                foreach ($data['workExperienceData'] as $key => $value) {
                    $data['getRoleList'][$key] = $this->getRoleName($value['job_function']);

                    if (isset($value['job_from']) && ($value['job_from'] != '0000-00-00')) {
                        $data['workExperienceData'][$key]['job_from_txt'] = date("jS F, Y", strtotime($value['job_from']));
                    }
                    if (isset($value['job_to']) && ($value['job_to'] != '0000-00-00')) {
                        $data['workExperienceData'][$key]['job_to_txt'] = date("jS F, Y", strtotime($value['job_to']));
                    }
                    if(isset($value['industry']) && !empty($value['industry'])){
                        $data['workExperienceData'][$key]['industry_name'] = $this->functions->getSingleColumn(INDUSTRY_MASTER, 'industryName', 'id', $value['industry'], '`status` = "1"');
                    }
                    if(isset($value['job_function']) && !empty($value['job_function'])){
                        $data['workExperienceData'][$key]['function_name'] = $this->functions->getSingleColumn(FUNCTION_MASTER, 'functionName', 'id', $value['job_function'], '`status` = "1"');
                    }
                    if(isset($value['job_role']) && !empty($value['job_role'])){
                        $data['workExperienceData'][$key]['role_name'] = $this->functions->getSingleColumn(ROLE_MASTER, 'roleName', 'id', $value['job_role'], '`status` = "1"');
                    }
                }
        }
        return $data;
        
    }

    public function addeditSeekerEduDetail() {
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'qualification' => $_POST['add']['qualification'],
                    'courseType' => $_POST['add']['courseType'],
                    'specialization' => $_POST['add']['specialization'],
                    'institute' => $_POST['add']['institute'],
                    'study' => $_POST['add']['study'],
                    'yearOfPass' => $_POST['add']['yearOfPass'],
                    'scored' => $_POST['add']['scored'],
                    'location' => $_POST['add']['location'],
                );
                $last_insert_id = $this->db->insert(EDUCATION_DETAIL, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $this->fetchEducationDetailData($_POST['add']['userid']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Education Detail has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
            /* (START) Edit section */
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'qualification' => $_POST['edit']['qualification'],
                    'courseType' => $_POST['edit']['courseType'],
                    'specialization' => $_POST['edit']['specialization'],
                    'institute' => $_POST['edit']['institute'],
                    'study' => $_POST['edit']['study'],
                    'yearOfPass' => $_POST['edit']['yearOfPass'],
                    'scored' => $_POST['edit']['scored'],
                    'location' => $_POST['edit']['location'],
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modelprofile->updatedb(EDUCATION_DETAIL, $updateData, $condition);
                if ($update) {
                    $data['data'] = $this->fetchEducationDetailData($_POST['edit']['user_id']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Education Detail has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    function fetchEducationDetailData($userId){
        $data['educationDetailData'] = $this->functions->getMultiRow(EDUCATION_DETAIL, 'user_id', $userId, '', '', '');
        if (isset($data['educationDetailData']) && !empty($data['educationDetailData'])) {
            foreach ($data['educationDetailData'] as $key => $value) {
                $data['getSpecializationListEdit'][$key] = $this->getSpecializationList($value['qualification']);
                if(isset($value['qualification']) && !empty($value['qualification'])){
                    $data['educationDetailData'][$key]['qualificationName'] = $this->functions->getSingleColumn(EDUCATION_LVL_MASTER, 'educationLevelName', 'id', $value['qualification'], '`status` = "1"');
                }
                if(isset($value['qualification']) && !empty($value['qualification'])){
                    $data['educationDetailData'][$key]['specializationName'] = $this->functions->getSingleColumn(EDUCATION_LVL_MASTER, 'educationLevelName', 'id', $value['specialization'], '`status` = "1"');
                }
            }
        }
        return $data;
    }

    public function addeditSeekerSkill() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'skill' => $_POST['add']['skill'],
                    'experience' => $_POST['add']['experience']
                );
                $last_insert_id = $this->db->insert(JOB_SEEKER_SKILL, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $this->fetchSeekerSkillData($_POST['add']['userid']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Skill has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'experience' => $_POST['edit']['experience'],
                    'applied' => $_POST['edit']['applied'],
                    'studied' => $_POST['edit']['studied']
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modelprofile->updatedb(JOB_SEEKER_SKILL, $updateData, $condition);
                if ($update) {
                    $data['data'] = $this->fetchSeekerSkillData($_POST['edit']['user_id']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Skill has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
    }

    function fetchSeekerSkillData($jobseeker_id){
        $data['seekerSkillData'] = $this->functions->getMultiRow(JOB_SEEKER_SKILL, 'user_id', $jobseeker_id, '', '', '');
        if (isset($data['seekerSkillData']) && !empty($data['seekerSkillData'])) {
            foreach ($data['seekerSkillData'] as $key => $value) {
                $data['seekerSkillData'][$key]['skill'] = $this->functions->getSingleColumn(SKILL_MASTER, 'skillTitle', 'id', $value['skill'], '`status` = "1"');
            }
        }
        return $data;
    }

    public function addeditSeekerCertification() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            /* (START) Add section */
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'certification' => $_POST['add']['certification'],
                    'certifyYear' => $_POST['add']['certifyYear']
                );
                $last_insert_id = $this->db->insert(CERTIFICATION, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $this->fetchCertificationData($_POST['add']['userid']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Certification has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (END) Add section */
            /* (START) Edit section */
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'certification' => $_POST['edit']['certification'],
                    'certifyYear' => $_POST['edit']['certifyYear']
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modelprofile->updatedb(CERTIFICATION, $updateData, $condition);
                if ($update) {
                    $data['data'] = $this->fetchCertificationData($_POST['edit']['user_id']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Certification has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }

            /* (START) Edit section */
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    function fetchCertificationData($jobseeker_id){
        $data['certificationData'] = $this->functions->getMultiRow(CERTIFICATION, 'user_id', $jobseeker_id, '', '', '');
        return $data;
    }

    public function addeditSeekerLanguage() {
        //print_r($_POST);exit;
         //(START) Add section 
        if (isset($_POST['add']) && !empty($_POST['add'])) {
            $addData = array(
                'user_id' => $_POST['add']['userid'],
                'languageId' => $_POST['add']['languageId'],
                'proficiency' => $_POST['add']['proficiency']
            );
            $last_insert_id = $this->db->insert(JOB_SEEKER_LANGUAGE, $addData);
            $last_insert_id = $this->db->insert_id();
            if ($last_insert_id > 0) {
                $addData['id'] = $last_insert_id;
                $data['data'] = $this->fetchSeekerLanguageData($_POST['add']['userid']);
                $data['status'] = 'OK';
                $data['msg'] = 'Language has been Added successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
            // (END) Add section 
        }

        if (isset($_POST['edit']) && !empty($_POST['edit'])) {

            $updateData = array(
                'user_id' => $_POST['edit']['user_id'],
                'languageId' => $_POST['edit']['languageId'],
                'languageRead' => $_POST['edit']['languageRead'],
                'languageWrite' => $_POST['edit']['languageWrite'],
                'languageSpeak' => $_POST['edit']['languageSpeak'],
                'proficiency' => $_POST['edit']['proficiency']
            );
            $condition = array('id' => $_POST['edit']['id']);

            $update = $this->Modelprofile->updatedb(JOB_SEEKER_LANGUAGE, $updateData, $condition);
            if ($update) {
                $data['data'] = $this->fetchSeekerLanguageData($_POST['edit']['user_id']);
                $data['status'] = 'OK';
                $data['msg'] = 'Language has been Updated successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
            echo json_encode($data);
            exit;
        }
    }

    function fetchSeekerLanguageData($jobseeker_id){
        $data['seekerLanguageData'] = $this->functions->getMultiRow(JOB_SEEKER_LANGUAGE, 'user_id', $jobseeker_id, '', '', '');
        $data['getProficiencyList'] = unserialize(PROFICIENCYTYPE);
        if (isset($data['seekerLanguageData']) && !empty($data['seekerLanguageData'])) {
            foreach ($data['seekerLanguageData'] as $key => $value) {
                $data['seekerLanguageData'][$key]['languageName'] = $this->functions->getSingleColumn(LANGUAGE_MASTER, 'name', 'id', $value['languageId'], '`status` = "1"');
                if (isset($data['getProficiencyList'])) {
                    foreach ($data['getProficiencyList'] as $subkey => $subvalue) {
                        if ($subkey == $value['proficiency']){
                            $data['seekerLanguageData'][$key]['proficiency'] = $subvalue;
                        }
                    }
                }
            }
        }

        return $data;
    }

    public function addeditSeekerAchievement() {
        //print_r($_POST);exit;
        if (!empty($_POST)) {
            //(START) Add section
            if (isset($_POST['add']) && !empty($_POST['add'])) {
                $addData = array(
                    'user_id' => $_POST['add']['userid'],
                    'achievement' => $_POST['add']['achievement'],
                    'createdOn' => CURRENTDATE
                );

                $last_insert_id = $this->db->insert(JOB_SEEKER_ACHIEVEMENT, $addData);
                $last_insert_id = $this->db->insert_id();
                if ($last_insert_id > 0) {
                    $addData['id'] = $last_insert_id;
                    $data['data'] = $this->fetchAchievementData($_POST['add']['userid']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Achievement has been Added successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }
            //(START) Edit section
            if (isset($_POST['edit']) && !empty($_POST['edit'])) {

                $updateData = array(
                    'user_id' => $_POST['edit']['user_id'],
                    'achievement' => $_POST['edit']['achievement'],
                    'updatedOn' => CURRENTDATE
                );
                $condition = array('id' => $_POST['edit']['id']);

                $update = $this->Modelprofile->updatedb(JOB_SEEKER_ACHIEVEMENT, $updateData, $condition);
                if ($update) {
                    $data['data'] = $this->fetchAchievementData($_POST['edit']['user_id']);
                    $data['status'] = 'OK';
                    $data['msg'] = 'Achievement has been Updated successfully.';
                } else {
                    $data['status'] = 'ERR';
                    $data['msg'] = 'Some problem occurred, please try again.';
                }
                echo json_encode($data);
                exit;
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = 'Some problem occurred, please try again.';
        }
        echo json_encode($data);
        exit;
    }

    function fetchAchievementData($jobseeker_id){
        $data['achievementData'] = $this->functions->getMultiRow(JOB_SEEKER_ACHIEVEMENT,'user_id',$jobseeker_id,'','','');
        return $data;
    }

    public function deleteCommon() {
        $data = array();
        if (!empty($_POST['id']) && !empty($_POST['type'])) {
            if ($_POST['type'] == 'workexp') {
                $tableName = WORK_EXPERIENCE;
                $message = 'Work Experience';
                $function = 'fetchWorkExperienceData';
            }
            if ($_POST['type'] == 'portfolio') {
                $tableName = PORTFOLIO;
                $message = 'Portfolio';
            }
            if ($_POST['type'] == 'eduDetail') {
                $tableName = EDUCATION_DETAIL;
                $message = 'Education Qualification';
                $function = 'fetchEducationDetailData';
            }
            if ($_POST['type'] == 'certify') {
                $tableName = CERTIFICATION;
                $message = 'Certification';
                $function = 'fetchCertificationData';
            }
            if ($_POST['type'] == 'achievement') {
                $tableName = JOB_SEEKER_ACHIEVEMENT;
                $message = 'Achievement';
                $function = 'fetchAchievementData';
            }
            if ($_POST['type'] == 'skill') {
                $tableName = JOB_SEEKER_SKILL;
                $message = 'Skill';
                $function = 'fetchSeekerSkillData';
            }
            if ($_POST['type'] == 'language') {
                $tableName = JOB_SEEKER_LANGUAGE;
                $message = 'Language';
                $function = 'fetchSeekerLanguageData';
            }

            $condition = array('id' => $_POST['id']);
            $delete = $this->Modelprofile->deletefromdb($tableName, $condition);
            if ($delete) {
                $data['data'] = $this->$function($_POST['userId']);
                $data['status'] = 'OK';
                $data['msg'] = $message . ' has been deleted successfully.';
            } else {
                $data['status'] = 'ERR';
                $data['msg'] = 'Some problem occurred, please try again.';
            }
        } else {
            $data['status'] = 'ERR';
            $data['msg'] = $message . ' Details not Found';
        }
        echo json_encode($data);
    }

    public function getRoleName($functionId = "") {
        // /print_r($_POST);exit;
        if (isset($_POST['functionId']) && !empty($_POST['functionId'])) {
            $functionId = $_POST['functionId'];
            $data['data'] = $this->functions->getMultiRow(ROLE_MASTER, 'function_id', $functionId, '', '', '');
            echo json_encode($data);
            exit;
        } else {
            if (!empty($functionId)) {
                $data = $this->functions->getMultiRow(ROLE_MASTER, 'function_id', $functionId, '', '', '');
            } else {
                $data = $this->Modelprofile->get_rows('', '', '', 'id', ROLE_MASTER);
            }
        }

        return $data;
    }
    public function getSpecializationList($id = "") {
        //print_r($_POST);exit;
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $data['data'] = $this->functions->getMultiRow(EDUCATION_LVL_MASTER, 'parentId', "$id", '', '', '');
            echo json_encode($data);
            exit;
        } else {
            if (!empty($id)) {
                $data = $this->functions->getMultiRow(EDUCATION_LVL_MASTER, 'parentId', $id, '', '', '');
            } else {
                $data = $this->Modelprofile->get_rows('', '', '', 'id', EDUCATION_LVL_MASTER);
            }
        }

        return $data;
    }

    public function profileImageUpload() {
         /*print_r($_POST);
          print_r($_FILES);exit; */

        $jobseekerId = filter_input(INPUT_POST, 'Id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (isset($_FILES['profileImage']) && $_FILES['profileImage']['error'] == 0) {

            // uploads image in the folder images
            $file_name = explode(".", $_FILES["profileImage"]["name"]);
            $fileExt = end($file_name);
            $fileExtL = strtolower($fileExt);
            //echo $fileExt; die('==');
            if (in_array($fileExtL, unserialize(IMAGETYPE))) {
                $time = substr(md5(time()), 0, 10);
                $newfilename = $time . '.' . $fileExtL;

                if (!is_dir(UPLOAD . ABSPATH_JOBSEKER_PROFILE)) {
                    mkdir(UPLOAD . ABSPATH_JOBSEKER_PROFILE, 0777, TRUE);
                }
                $upload_path = UPLOAD . ABSPATH_JOBSEKER_PROFILE . $newfilename;
                move_uploaded_file($_FILES['profileImage']['tmp_name'], $upload_path);
                // give callback to your angular code with the image src name

                if (!empty($jobseekerId)) {
                    $profileImageName = $this->functions->getSingleColumn(JOB_SEEKER_MASTER, 'jobseekerImage', 'userId', $jobseekerId, '`status` = "1"');

                    //print_r($profileImageName);exit;
                    if (!empty($profileImageName)) {
                        // unlink resume
                        $this->functions->unlinkFile(file_upload_absolute_path() . ABSPATH_JOBSEKER_PROFILE, $profileImageName);
                    }
                    // update new resume file
                    $userData = array(
                        'jobseekerImage' => $newfilename,
                    );
                    $condition = array('userId' => $jobseekerId);
                    $this->Modelprofile->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
                    echo '1'; // successfully
                }
            } else {
                echo '2'; // Incorrect File formate
            }
        } else {
            echo '3'; // Some problem
        }
    }

    public function resumeUpload() {
        //print_r($_FILES);die('end');
        $jobseekerId = filter_input(INPUT_POST, 'jobseeker_id', FILTER_VALIDATE_INT, INTEGER_FILTER);
        if (isset($_FILES['resumeupload']) && $_FILES['resumeupload']['error'] == 0) {

            // uploads image in the folder images
            $file_name = explode(".", $_FILES["resumeupload"]["name"]);
            $fileExt = end($file_name);
            $fileExtL = strtolower($fileExt);
            //echo $fileExtL; die('==');
            if (in_array($fileExtL, unserialize(RESUMETYPE))) {
                if ($_FILES['resumeupload']['size'] < RESUMESIZE) {
                    $time = substr(md5(time()), 0, 10);
                    $newfilename = $time . '.' . $fileExtL;

                    if (!is_dir(UPLOAD . ABSPATH_JOBSEKER)) {
                        mkdir(UPLOAD . ABSPATH_JOBSEKER, 0777, TRUE);
                    }
                    $upload_path = UPLOAD . ABSPATH_JOBSEKER . $newfilename;
                    move_uploaded_file($_FILES['resumeupload']['tmp_name'], $upload_path);
                    // give callback to your angular code with the image src name

                    if (!empty($jobseekerId)) { // edit section only
                        $resumefileName = $this->functions->getSingleColumn(JOB_SEEKER_MASTER, 'jobseekerResume', 'userId', $jobseekerId, '`status` = "1"');

                        //print_r($resumefileName);exit;
                        if (!empty($resumefileName)) {
                            // unlink resume
                            $this->functions->unlinkFile(file_upload_absolute_path() . ABSPATH_JOBSEKER, $resumefileName);
                        }
                        // update new resume file
                        $userData = array(
                            'jobseekerResume' => $newfilename,
                            'jobseekerResumeFile' => $_FILES["resumeupload"]["name"],
                            'resumeUpdatedOn' => CURRENTDATE,
                        );
                        $condition = array('userId' => $jobseekerId);
                        $this->Modelprofile->updatedb(JOB_SEEKER_MASTER, $userData, $condition);
                    }
                    echo json_encode($newfilename);
                } else {
                    echo 2;
                }
            } else {
                echo 1;
            }
        } else {
            echo 0;
        }
    }
}
