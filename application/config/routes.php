<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
/* $route['default_controller'] = 'welcome';
  $route['404_override'] = '';
  $route['translate_uri_dashes'] = FALSE; */

//Frontend

$route['default_controller'] = "home";
$route['404_override'] = '';

$route['resetPassword/(:any)'] = 'home/resetPassword/$1';

// Admin
$route['admin'] = "admin/Administrative";
$route['admin/dashboard'] = "admin/Dashboard";

/*$route['admin/administrative/forgetPassword'] = "admin/administrative/forgetPassword";
$route['admin/administrative/checkemail/(:any)'] = "admin/administrative/checkEmail/$2";
$route['admin/administrative/newPassword/(:any)/(:num)'] = "admin/administrative/newPassword/$2/$3";

$route['admin/settings/'] = "admin/settings/index";
$route['admin/settings/defaultpermission/(:num)'] = "admin/settings/defaultPermission/$2";

$route['admin/adminuser/'] = "admin/adminuser/index";
$route['admin/adminuser/addusers'] = "admin/adminuser/addusers";
$route['admin/adminuser/userpermission/(:num)'] = "admin/adminuser/userpermission/$2";*/

// Jobseeker
/*$route['admin/manage_jobseeker/'] = "admin/manage_jobseeker/index";
$route['admin/manage_jobseeker/edit/(:num)'] = "admin/manage_jobseeker/edit/$2";*/


// Page Mangement from ADMIN
/*$route['admin/cmspage/'] = "admin/cmspage/index";
$route['admin/cmspage/addCmsPage'] = "admin/cmspage/addCmsPage";
$route['admin/cmspage/editCmspage'] = "admin/cmspage/editCmspage";
$route['admin/cmspage/editCmspage/(:num)'] = "admin/cmspage/editCmspage/$2";*/

// Employer Mangement from ADMIN
/*$route['admin/employer/'] = "admin/employer/index";
$route['admin/employer/editemployers/(:num)'] = "admin/employer/editemployers/$1";
$route['admin/employer/updateempdata/(:num)'] = "admin/employer/updateempdata/$1";
$route['admin/employer/getstates/(:num)/(:num)'] = "admin/employer/getstates/$1/$2";
$route['admin/employer/getcities/(:num)/(:num)'] = "admin/employer/getcities/$1/$2";*/

// Jobs Mangement from ADMIN
/*$route['admin/job/e/(:num)'] = "admin/job/index/e/$1";
$route['admin/job/r/(:num)'] = "admin/job/index/r/$1";
$route['admin/job/getdetails/(:num)'] = "admin/job/getdetails/$1";

$route['admin/subscriber'] = "admin/newsletter/subscriber";*/

/* FrontEnd */

//page
//$route['page/(:any)'] = "page/index/$2";

//Profile
/*$route['profile/(:num)'] = "profile/index/$2";
$route['profile/(:num)/(:any)'] = "profile/index/$2/$3";*/

//recruiter
/*$route['recruiter/(:num)/(:any)'] = "recruiter/index/$2/$3";*/

//employer
//$route['employer/dashboard'] = "employer/index";
/*$route['employer/post-job'] = "employer/postjob";
$route['employer/(:num)/(:any)'] = "employer/index/$2/$3";*/




