<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

//custom defination
defined('STYLEEFFECT') OR define('STYLEEFFECT', 'bounce'); // wait me effect style

//Database tables prefix
define('PRE', 'ecsa_');

//Database tables Declaration

define('ADM_ACTION_MASTER', PRE . 'adminActionMaster');
define('ADM_CMS_PAGES', PRE . 'adminCmsPages');
define('ADM_MENU_MASTER', PRE . 'adminMenuMaster');
define('ADM_USER_LOGS', PRE . 'adminUserLogs');
define('ADM_USER_MENU_PERMISSION', PRE . 'adminUserMenuPermission');
define('ADM_USERS', PRE . 'adminUsers');
define('ADM_USERTYPE_DEFAULT_PERMISSION', PRE . 'adminUserTypeDefaultPermission');
define('ADM_USERTYPES', PRE . 'adminUserTypes');
define('CERTIFICATION', PRE . 'certification');
define('CITY', PRE . 'cities');
define('COUNTRY', PRE . 'countries');
define('DEPARTMENT_MASTER', PRE . 'departmentMaster');
define('EDUCATION_DETAIL', PRE . 'educationDetail');
define('EDUCATION_LVL_MASTER', PRE . 'educationLevelMaster');
define('FIELD_OF_STUDY_MASTER', PRE . 'fieldOfStudyMaster');
define('INDUSTRY_MASTER', PRE . 'industryMaster');
define('JOB_SEEKER_ACHIEVEMENT', PRE . 'jobseekerAchievement');
define('JOB_SEEKER_LANGUAGE', PRE . 'jobseekerLanguage');
define('JOB_SEEKER_MASTER', PRE . 'jobseekerMaster');
define('JOBSEEKER_REFERENCE_MASTER', PRE . 'jobseekerReferenceMaster');
define('JOB_SEEKER_SKILL', PRE . 'jobseekerSkill');
define('LANGUAGE_MASTER', PRE . 'languageMaster');
define('ROLE_MASTER', PRE . 'roleMaster');
define('SKILL_MASTER', PRE . 'skillMaster');
define('STATE', PRE .'states');
define('USER_MASTER', PRE . 'userMaster');
define('WORK_EXPERIENCE', PRE . 'workExperience');




// Default Time Zone
//define('default_timezone', 'America/Los_Angeles');

//globally used Current Date
define('CURRENTDATE', date('Y-m-d H:i:s'));

//Directory/Folder Declaration file upload only
define('UPLOAD', './uploads/');

define('ABSPATH_JOBSEKER', 'jobseeker/');
define('ABSPATH_JOBSEKER_PROFILE', 'jobseeker/profile/');

define('ABSPATH_EMPLOYER', 'employer/');
define('ABSPATH_EMPLOYER_PROFILE', 'employer/profile/');
define('ABSPATH_EMPLOYER_THUMB', 'employer/profile/thumb/');
define('ABSPATH_EMPLOYER_BANNER', 'employer/banner/');

define('ABSPATH_RECRUITER', 'recruiter/');
define('ABSPATH_RECRUITER_PROFILE', 'recruiter/profile/');
define('ABSPATH_RECRUITER_THUMB', 'recruiter/profile/thumb/');

define('PROFILE_HEIGHT', 200);
define('PROFILE_WIDTH', 200);

define('ABSPATH_MENU', 'menu/');
define('ABSPATH_XML', './xml/');


//Satic Custom Variable Declaration (constant, serialize array)
define('PASSWORD_EXPIRE_IN', '+180 days');
define('ACCESS_ERR', 'You don\'t have the permission to access.');
define("RESUMETYPE", serialize(array('doc', 'docx', 'rtf', 'txt', 'pdf', 'odt')));
define("IMAGETYPE", serialize(array('jpeg','jpg','gif','png','bmp')));
define("PROFICIENCYTYPE", serialize(array('Beginner', 'Proficient', 'Expert')));
define("GENDERTYPE", serialize(array('Male', 'Female')));
define("MARITAL_STATUS_TYPE", serialize(array('Single', 'Married','Divorced')));


// global Custom Variable Declaration
define("INTEGER_FILTER", serialize(array('options' => array('default' => 0))));
define('RESUMESIZE', 4194304); //4 megabytes (this size is in bytes)
define('PROFILEIMAGESIZE', 204800); //200 kilobytes (this size is in bytes)
define('BANNERSIZE', 4194304); //4 megabytes (this size is in bytes)
define('TYPE_JOB', 1); //Jobseeker
define('TYPE_EMP', 2); //Employer
define('TYPE_REC', 3); //Recruiter

/*define("TEMPLATE_KEY", serialize(array( 
    array('name' => "Newsletter", 'id' => 'newsletter'), 
    array('name' => "Jobseeker Registration", 'id' => 'jobseeker_signup'), 
    array('name' => "Jobseeker Activation", 'id' => 'jobseeker_active'),
    array('name' => "Jobseeker Welcome", 'id' => 'jobseeker_welcome'),
    array('name' => "Jobseeker Forget Password", 'id' => 'jobseeker_forget_pass'),
    array('name' => "Employer Registration", 'id' => 'employer_signup'),
    
    array('name' => "Application Notification (Employer)", 'id' => 'applicationNotification_employer'),
    array('name' => "Application Status Change (Recruiter)", 'id' => 'applicationStatusChange_Recruiter'),
    array('name' => "Application Status Change (Jobseeker)", 'id' => 'applicationStatusChange_Seeker'),
    
    array('name' => "New Job Apply (Employer)", 'id' => 'job_apply_employer'),
    array('name' => "New Job Apply (Recruiter)", 'id' => 'job_apply_recruiter'),
    array('name' => "New Job Apply (Jobseeker)", 'id' => 'job_apply_jobseeker'),
    
    )));*/

//Google App Details
/*define('GOOGLE_OAUTH_CLIENT_ID', '698842015936-sh9c5ffdopufnk2cpi9c86hmvv691ilb.apps.googleusercontent.com');
define('GOOGLE_OAUTH_CLIENT_SECRET', 'T_kl0iFllfLLSwWjAD4aSDz8');
define('GOOGLE_OAUTH_REDIRECT_URI', 'http://localhost/multirec/');
define("GOOGLE_SITE_NAME", 'http://localhost/');*/



