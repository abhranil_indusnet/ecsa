<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin_init_elements {

    var $CI;
    var $data;

    function __construct() {
        $this->CI = & get_instance();
    }
    /**
     * Checks user for page access
     *
     * This function takes the page-name and checks for user authenticity for admin section
     * Returns true if user is authentic
     * Redirects to user login page if user is not authentic
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    function is_admin_loggedin($section_name) {
        //if (!$this->CI->session->userdata('admin_logged_in') && $this->CI->session->userdata('admin_user_id') == '') {
            //redirect('admin/');
        //}
       // $this->CI->session->set_userdata('referred_from', current_url());
        $UserID = $this->CI->session->userdata('admin_user_id');
        if (!$UserID) {
            $cookie = array(
                'name' => 'admin_referer_path',
                'value' => $section_name,
                'expire' => '86500',
                'domain' => '',
                'path' => '/',
                'prefix' => '',
            );
            set_cookie($cookie);
           
            redirect('admin/');
            exit;
        }

        return true;
        
        
    }

    function init_elements() {
        $this->init_head();
        $this->init_header();
        $this->init_nav_menu();
        $this->init_footer();
    }

    function init_head() {
        $data = array();
        $this->CI->data['head'] = $this->CI->load->view('admin/elements/head', $data, true);
    }

    function init_header() {
        $data = array();
        $this->CI->data['header'] = $this->CI->load->view('admin/elements/header', $data, true);
    }

    function init_nav_menu() {
        $data = array();
        //$this->CI->load->model('admin_user');
        //$admin_user_id = $this->CI->session->userdata('admin_user_id');
        //$data = $this->CI->admin_user->get_rows($admin_user_id);
        $this->CI->data['navigation_menu'] = $this->CI->load->view('admin/elements/navigation-menu', $data, true);
    }

    function init_footer() {
        $data = array();
        $this->CI->data['footer'] = $this->CI->load->view('admin/elements/footer', $data, true);
    }

}
