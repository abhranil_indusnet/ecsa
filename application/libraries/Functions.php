<?php

/**
 * Common function for all common 
 * @author  Mohan
 * @version 1.0
 * @since   03/01/2017
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Functions {

    /**
     * Constructor
     */
    function __construct() {
        
        $this->obj = & get_instance();
       
    }

    /**
     * Checks user for page access
     *
     * This function takes the page-name and checks for user authenticity
     * Returns true if user is authentic
     * Redirects to user login page if user is not authentic
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    function checkUser($section_name) {
        $UserID = $this->obj->nsession->userdata('member_session_id');
        if (!$UserID) {
            $cookie = array(
                'name' => 'fe_referer_path',
                'value' => $section_name,
                'expire' => '86500',
                'domain' => '',
                'path' => '/',
                'prefix' => '',
            );
            set_cookie($cookie);
            redirect(base_url() . 'member/');
        }
        return true;
    }

    /**
     * Checks user for page access
     *
     * This function takes the page-name and checks for user authenticity for admin section
     * Returns true if user is authentic
     * Redirects to user login page if user is not authentic
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    function checkAdmin($section_name) {
        $UserID = $this->obj->nsession->userdata('user_session_id');

        if (!$UserID) {
            $cookie = array(
                'name' => 'admin_referer_path',
                'value' => $section_name,
                'expire' => '86500',
                'domain' => '',
                'path' => '/',
                'prefix' => '',
            );
            set_cookie($cookie);
            redirect(base_url() . 'login/');
            exit;
        }

        return true;
    }

    /**
     * Checks user for page access
     *
     * This function takes the page-name and checks for user authenticity for admin section
     * Returns true if user is authentic
     * Redirects to user login page if user is not authentic
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    function checkAuthAdmin($section_name) {
        $UserID = $this->obj->session->userdata('user_session_id');

        if (!$UserID) {
            $cookie = array(
                'name' => 'admin_referer_path',
                'value' => $section_name,
                'expire' => '86500',
                'domain' => '',
                'path' => '/',
                'prefix' => '',
            );
            set_cookie($cookie);

            // check company_url exist or not?
            $split_page = $this->obj->uri->segment(1, 0);
            if ($split_page) {
                $this->obj->load->model('Modellogin');
                $companyDet = $this->obj->Modellogin->checkCompany($split_page);
                if ($companyDet == 1) {
                    // all ok
                    redirect(base_url() . 'login');
                } else {
                    redirect(base_url() . 'logout');
                }
            }
        } else {
            return true;
        }
    }

    function createThumbnail($path_to_image_directory, $path_to_thumbs_directory, $filename, $height, $width) {

        if (preg_match('/[.](jpg)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        } else if (preg_match('/[.](gif)$/', $filename)) {
            $im = imagecreatefromgif($path_to_image_directory . $filename);
        } else if (preg_match('/[.](png)$/', $filename)) {
            $im = imagecreatefrompng($path_to_image_directory . $filename);
        } else if (preg_match('/[.](jpeg)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        }

        $ox = imagesx($im);
        $oy = imagesy($im);

        $nx = $height;
        $ny = $width;
        $nm = imagecreatetruecolor($nx, $ny);

        imagecopyresized($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);

        if (!file_exists($path_to_thumbs_directory)) {
            if (!mkdir($path_to_thumbs_directory)) {
                die("There was a problem. Please try again!");
            }
        }
        imagejpeg($nm, $path_to_thumbs_directory . $filename);
        chmod($path_to_thumbs_directory, 0777);
    }

    function cropTheImage($path_to_image_directory, $path_to_thumbs_directory, $filename, $height_crop, $width_crop) {

        // http://php.net/manual/en/function.imagecopyresampled.php
        // Set a maximum height and width
        $width = $width_crop;
        $height = $height_crop;
        //ini_set('gd.jpeg_ignore_warning', true);
        if (preg_match('/[.](jpg)$/', $filename)) {
            $image = imagecreatefromjpeg($path_to_image_directory . $filename);
        } else if (preg_match('/[.](gif)$/', $filename)) {
            $image = imagecreatefromgif($path_to_image_directory . $filename);
        } else if (preg_match('/[.](png)$/', $filename)) {
            $image = imagecreatefrompng($path_to_image_directory . $filename);
        } else if (preg_match('/[.](jpeg)$/', $filename)) {
            $image = imagecreatefromjpeg($path_to_image_directory . $filename);
        }
        //ini_set('gd.jpeg_ignore_warning', true);
        // Content type
        // header('Content-Type: image/jpeg');
        // Get new dimensions       
        list($width_orig, $height_orig) = getimagesize($path_to_image_directory . $filename);


        if ($height_orig > $width_orig) {
            $ratio_orig = $height_orig / $width_orig;
            $width = $width * ($ratio_orig);
            $height = $width * ($ratio_orig);
            $xm = (($width / 2) - ($width_crop / 2)) * 2;
            $ym = (($height / 2) - ($height_crop / 2)) * 2;
        } else {
            $ratio_orig = $width_orig / $height_orig;
            $width = $height * ($ratio_orig);
            $height = $height * ($ratio_orig);
            $xm = (($width / 2) - ($width_crop / 2)) * 2;
            $ym = (($height / 2) - ($height_crop / 2)) * 2;
        }
        // Resample
        $image_p = imagecreatetruecolor($width_crop, $height_crop);
        //$image = imagecreatefromjpeg($path_to_image_directory . $filename);
        imagecopyresampled($image_p, $image, 0, 0, $xm, $ym, $width, $height, $width_orig, $height_orig);

        // Output
        if (!file_exists($path_to_thumbs_directory)) {
            if (!mkdir($path_to_thumbs_directory)) {
                die("There was a problem. Please try again!");
            }
        }
        imagejpeg($image_p, $path_to_thumbs_directory . $filename, 100);
        chmod($path_to_thumbs_directory, 0777);
        //imagejpeg($image_p, null, 100);
    }

    function getSingle($id, $table, $query = false, $fetch_key = '', $isEscapeArr = array()) {
        if (!empty($query) && is_array($query)) {
            $query_string = implode(",", $query);
        } else {
            $query_string = '*';
        }


        if (!empty($fetch_key)) {
            $fetch_key = $fetch_key;
        } else {
            $fetch_key = 'id';
        }
        $sql = "SELECT " . $query_string . " FROM " . $table . " WHERE $fetch_key = '" . $id . "'";
        $recordSet = $this->obj->db->query($sql);

        $rs = false;
        if ($recordSet->num_rows() > 0) {
            $rs = array();
            foreach ($recordSet->result_array() as $row) {
                foreach ($row as $key => $val) {
                    if (!in_array($key, $isEscapeArr)) {
                        $recordSet->fields[$key] = outputEscapeString($val);
                    } else {
                        $recordSet->fields[$key] = outputEscapeString($val, 'TEXTAREA');
                    }
                }
                $rs[] = $recordSet->fields;
            }
        }
        return $rs;
    }

    /**
     * getNameTable
     *
     * This function USED TO FETCH A single value from database 
     * param $table is the table name
     * param $col is the column  name which you want to fetch
     * param $field is the condition column  name
     * param $value is the condition column  value 
     * @access	public
     * @return	string
     * param $param other condition
     */
    function getSingleColumn($table, $col, $field = '', $value = '', $param = '') {
        $query = "SELECT " . $col . " FROM " . $table . " where 1 ";
        if ($field != '' && $value != '') {
            $query.="AND " . $field . "='" . $value . "' ";
        }
        if ($param) {
            $whereclause = " AND ";
            $query .=$whereclause . $param;
        }
        $recordSet = $this->obj->db->query($query);
        if ($recordSet->num_rows() > 0) {
            $row = $recordSet->row_array();
            if($col == '*'){
                return $row;
            }else{
                return $row[$col];
            } 
            
            
        } else {
            return "";
        }
    }

    /**
     * getListTable
     * This function USED TO fetch row details 
     * param $table_name is the table name
     * param $field is the condition column  name
     * param $value is the condition column  value 
     * @access	public
     * @return	array
     * param $orderfield is the order by field
     * param $ordertype is the order by type
     * 
     */
    function getMultiRow($table_name, $field = '', $value = '', $orderfield = '', $ordertype = 'ASC', $param = '', $getQuery = false) {
        $sql = "SELECT * FROM `" . $table_name . "` WHERE 1";
        if ($field != '' && $value != '') {
            $sql.=" AND `" . $field . "`='" . $value . "'";
        }
        if (!empty($param)) {
            $sql.=" AND " . $param;
        }
        if ($orderfield != '') {
            $sql.=" ORDER BY `" . $orderfield . "` " . $ordertype . "";
        }
        //echo $sql;die;
        $recordSet = $this->obj->db->query($sql);

        if ($getQuery) {
            echo $sql;
        }
        //return $sql;
        $rs = false;
        if ($recordSet->num_rows() > 0) {
            $rs = array();
            $isEscapeArr = array();
            foreach ($recordSet->result_array() as $row) {
                foreach ($row as $key => $val) {
                    if (!in_array($key, $isEscapeArr)) {
                        $recordSet->fields[$key] = outputEscapeString($val);
                    }
                }
                $rs[] = $recordSet->fields;
            }
        } else {
            return false;
        }
        //pr($rs);
        return $rs;
    }

    


    function queryBilder($select, $table, $condition = false, $getQuery = false, $multiple = false, $isEscapeArr = array()) {
        if (!empty($select) && is_array($select)) {
            $query_string = implode(",", $select);
        } else {
            $query_string = $select;
        }

        if ($condition) {
            $where = "WHERE " . $condition;
        } else {
            $where = "WHERE 1 ";
        }

        $sql = "SELECT " . $query_string . " FROM " . $table . " $where ";
        $recordSet = $this->obj->db->query($sql);
        if ($getQuery) {
            echo $this->obj->db->last_query() . '<br>';
        }

        if ($multiple) {
            $rs = false;
            if ($recordSet->num_rows() > 0) {
                $rs = array();
                foreach ($recordSet->result_array() as $row) {
                    foreach ($row as $key => $val) {
                        if (!in_array($key, $isEscapeArr)) {
                            $recordSet->fields[$key] = outputEscapeString($val);
                        } else {
                            $recordSet->fields[$key] = outputEscapeString($val, 'TEXTAREA');
                        }
                    }
                    $rs[] = $recordSet->fields;
                }
            }
            return $rs;
        } else {
            $rs = false;
            if ($recordSet->num_rows() > 0) {
                $rs = array();
                $isEscapeArr = array();
                foreach ($recordSet->result_array() as $row) {
                    foreach ($row as $key => $val) {
                        if (!in_array($key, $isEscapeArr)) {
                            $recordSet->fields[$key] = outputEscapeString($val);
                        }
                    }
                    $rs[] = $recordSet->fields;
                }
            } else {
                return false;
            }
            
            return $rs;
            
            
            //$result = $recordSet->row_array();
            //return $result;
        }
    }

    /**
     * existRecords
     *
     * This function USED TO check the record is exists or not
     * param $table is the table name
     * param $col is the column  name which you want to fetch
     * param $field_name is the condition column  name
     * param $field_value is the condition column  value 
     * @access	public
     * @return	string
     * param $pk is the checking column name
     * param $pk_value is the checking column value
     * param $param other condition
     */
    function existRecords($table, $field_name, $field_value, $pk, $pk_value = 0, $field_name1 = "", $field_value1 = "") {

        $query = "SELECT COUNT(" . $pk . ") as CNT FROM " . $table . " where " . $field_name . "=" . $this->obj->db->escape($field_value) . " ";

        if ($field_name1 != "" && $field_value1 != "") {
            $query.=" AND " . $field_name1 . "='" . $field_value1 . "' ";
        }
        if ($pk_value) {
            $query.=" AND " . $pk . "!='" . $pk_value . "'";
        }

        $recordSet = $this->obj->db->query($query);
        if ($recordSet->num_rows() > 0) {
            $row = $recordSet->row();
            return $row->CNT;
        } else {
            return "";
        }
    }

    function fetchCategoryTree($table_name = '', $parent = 0, $spacing = '', $user_tree_array = '') {

        $CI = & get_instance();

        if (!is_array($user_tree_array)) {
            $user_tree_array = array();
        }

        $where = array('status' => '1', 'deleted' => '0', 'parentid' => $parent, 'company_id' => $CI->session->userdata('active_company_id'));
        $this->obj->db->from($table_name);
        $this->obj->db->where($where);
        $this->obj->db->order_by("company_id", 'ASC');
        $query = $this->obj->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $user_tree_array[] = array("id" => $row->id, "pid" => $row->parentid, "name" => $spacing . $row->typename);
                $user_tree_array = $this->fetchCategoryTree($table_name, $row->id, $spacing, $user_tree_array);
            }
        }
        return $user_tree_array;
    }

    function multiArrayUnique($data = array()) {

        if (isset($data) && !empty($data) && is_array($data)) {
            $data = array_map("unserialize", array_unique(array_map("serialize", $data)));
        }

        return $data;
    }

    function multiDimIn_array($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }

    /**
     * send_email
     * This function USED TO send email 
     * param $to_email is the  email address of the receiver
     * param $search_text is the  assinged text in email template
     * param $replace_text is the  value which have tobe replaced with the assigned text
     * param $subject is the  subject of email
     * 
     */
    function sendAttachEmail($to, $from, $fromName, $recipients, $subject, $message, $attachment = '') {
        //$this->load->library('email');
        $daycheck = $this->mailDayCheck($to);
        if ($daycheck == 1) {
            $this->obj->email->clear(TRUE);
            $this->obj->email->set_mailtype("html");
            $this->obj->email->from($from, $fromName);
            $this->obj->email->to($to);
            $this->obj->email->cc($recipients);
            $this->obj->email->subject($subject);
            $this->obj->email->message($message);
            if ($attachment != '') {
                $this->obj->email->attach($attachment);
            }

            if (!$this->obj->email->send()) {
                return false;
            } else {
                return true;
            }
            //echo $this->obj->email->print_debugger();
            //exit;
            return TRUE;
        } else {
            return false;
        }
    }

    function singleEmail($to, $from, $fromName, $subject, $message) {
        $this->obj->email->clear(TRUE);
        $this->obj->email->set_mailtype("html");
        $this->obj->email->from($from, $fromName);
        $this->obj->email->to($to);
        $this->obj->email->subject($subject);
        $this->obj->email->message($message);
        if (!$this->obj->email->send()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *   A function for easily uploading files. This function will automatically generate a new 
     *   file name so that files are not overwritten.
     *   Taken From: http://www.bin-co.com/php/scripts/upload_function/
     *   Arguments:    $file_id- The name of the input field contianing the file.
     *   $folder    - The folder to which the file should be uploaded to - it must be writable. OPTIONAL
     *   $types    - A list of comma(,) seperated extensions that can be uploaded. If it is empty, anything goes OPTIONAL
     *   Returns  : This is somewhat complicated - this function returns an array with two values...
     *   The first element is randomly generated filename to which the file was uploaded to.
     *   The second element is the status - if the upload failed, it will be 'Error : Cannot upload the file 'name.txt'.' or something like that
     */
    function uploadAll($file_id, $folder = "", $types = "") {
        if (!$_FILES[$file_id]['name']) {
            return array('', 'No file specified');
        }

        $file_title = $_FILES[$file_id]['name'];
        //Get file extension
        $ext_arr = explode(".", basename($file_title));
        $ext = strtolower($ext_arr[count($ext_arr) - 1]); //Get the last extension
        //Not really uniqe - but for all practical reasons, it is
        $uniqer = substr(md5(uniqid(rand(), 1)), 0, 5);
        $file_name = $uniqer . '_' . strtotime(date("Y-m-d H:i:s")) . "." . $ext; //Get Unique Name

        $all_types = explode(",", strtolower($types));
        if ($types) {
            if (in_array($ext, $all_types)) {
                
            } else {
                $result = "'" . $_FILES[$file_id]['name'] . "' is not a valid file."; //Show error if any.
                return array('', $result);
            }
        }

        if (!is_dir($folder)) {
            mkdir($folder, 0777, TRUE);
        }
        //Where the file must be uploaded to
        if ($folder) {
            $folder .= '/'; //Add a '/' at the end of the folder
        }
        $uploadfile = $folder . $file_name;

        $result = '';
        //Move the file from the stored location to the new location
        if (!move_uploaded_file($_FILES[$file_id]['tmp_name'], $uploadfile)) {
            $result = "Cannot upload the file '" . $_FILES[$file_id]['name'] . "'"; //Show error if any.
            if (!file_exists($folder)) {
                $result .= " : Folder don't exist.";
            } elseif (!is_writable($folder)) {
                $result .= " : Folder not writable.";
            } elseif (!is_writable($uploadfile)) {
                $result .= " : File not writable.";
            }
            $file_name = '';
        } else {
            if (!$_FILES[$file_id]['size']) { //Check if the file is made
                @unlink($uploadfile); //Delete the Empty file
                $file_name = '';
                $result = "Empty file found - please use a valid file."; //Show the error message
            } else {
                chmod($uploadfile, 0777); //Make it universally writable.
            }
        }

        return array($file_name, $result);
    }

    // Generates a strong password of N length containing at least one lower case letter,
    // one uppercase letter, one digit, and one special character. The remaining characters
    // in the password are chosen at random from those four sets.
    //
    // The available characters in each set are user friendly - there are no ambiguous
    // characters such as i, l, 1, o, 0, etc. This, coupled with the $add_dashes option,
    // makes it much easier for users to manually type or speak their passwords.
    //
    // Note: the $add_dashes option will increase the length of the password by
    // floor(sqrt(N)) characters.
    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    function generateRandomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function randomString($length = 5) {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    function randomNumber($length = 4) {
        $str = "";
        $characters = range('0', '9');
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    function getAge($raiseddate) {
        $date = new DateTime(); //this returns the current date time
        $date1 = $date->format('Y-m-d H:i:s');

        $date2A = new DateTime($raiseddate); //this returns the current date time
        $date2 = $date2A->format('Y-m-d H:i:s');

        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        $interval = $datetime2->diff($datetime1);

        return $interval->format('%a');
    }

    function getAge2($dob) {
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dob), date_create($today));
        return $diff->format('%yYears, %mMonths, %dDays');
    }

    function usFormat($number) {

        $v = number_format($number, 2, '.', '');
        $v = '$' . $v;
        return $v;
    }

    /**
     * @description:Used to change global date format to Y-m-d H:i:s
     * @author: Mohan
     * @date: 8/11/2016 
     * @$date date
     * @return $format formated date
     * @access public
     */
    function dbdateFormat($dateArg) {
        $date = str_replace("-", "/", $dateArg);
        $format = date("Y-m-d H:i:s", strtotime($date));
        return $format;
    }

    /**
     * @description:Used to change site date format to n-j-Y
     * @author: Mohan
     * @date: 8/11/2016 
     * @$date date
     * @return $format formated date
     * @access public
     */
    function sitedateFormat($date) {
        $format = date("n-j-Y", strtotime($date));
        return $format;
    }

    /**
     * @description:Used for Unlink files from folder
     * @author: Mohan
     * @date: 03/01/2017 
     * @return the result of array of json
     */
    function unlinkFile($dir, $file) { 
        if (file_exists($dir . $file)) {
            if (unlink($dir . $file)) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
    }
    function display_filesize($filesize) {

        if (is_numeric($filesize)) {
            $decr = 1024;
            $step = 0;
            $prefix = array('Byte', 'KB', 'MB', 'GB', 'TB', 'PB');

            while (($filesize / $decr) > 0.9) {
                $filesize = $filesize / $decr;
                $step++;
            }
            return round($filesize, 2) . ' ' . $prefix[$step];
        } else {

            return 'NaN';
        }
    }
    /**
     * @description:check email exist or not in usermaster?
     * @author: Mohan
     * @date: 15/02/2017 
     * @param int $usertype an integer of user type id value  
     * @param str $email user email id
     * @param int $id an integer of user id value  
     * @return total count
     */
    public function emailExist($usertype, $email, $id = '') {
        $this->obj->db->from(USER_MASTER);
        $this->obj->db->where(array('deleted' => '0', 'userEmail' => $email, 'userTypeId' => "$usertype"));
        
        if($id != ''){ // for edit only
             $this->obj->db->where("id <>",$id);
        }
        $query = $this->obj->db->get();
        $query->result_array();
        //echo $this->obj->db->last_query();//die();
        return ($query->num_rows());
    }
    
    
    public function fileUnlink($tblName, $column, $id , $filepath) {
        $fileName = $this->getSingleColumn($tblName, $column , 'id', $id, '');
        if (!empty($fileName)) {
            $this->unlinkFile(file_upload_absolute_path() . $filepath, $fileName);
            return true;
        }else{
            return false;
        }
        
    }
    function validateFromToDate($from,$to){

        if((isset($from) && !empty($from)) && (isset($to) && !empty($to))){
            $timestampFrom = strtotime($from);
            $timestampTo = strtotime($to);
            if($timestampTo > $timestampFrom){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function dateDiff($startDate,$endDate) {

        $diff = date_diff(date_create($startDate), date_create($endDate));
        return $diff->format('%y Year(s), %m Month(s)');
    }

}

// END Functions Class
/* End of file functions.php */