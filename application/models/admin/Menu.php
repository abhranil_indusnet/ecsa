<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends CI_Model{
	
	function __construct() {
		$this->tableName = ADM_MENU_MASTER;
		$this->adminUserTypes = ADM_USERTYPES;
		
	}
	
	public function get_rows($user_id = '')
	{
		$this->db->from($this->tableName);
		if($user_id == ''){
			$query = $this->db->get();
			$result = $query->result_array();
		}else{
			$this->db->where($this->primaryKey,$user_id);
			$query = $this->db->get();
			$result = $query->row_array();
		}
		
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}

	public function get_typerows()
	{
		$this->db->from($this->adminUserTypes);
		$this->db->where('id !=', '1');

			$query = $this->db->get();
			$result = $query->result_array();
		
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}
	
	public function get_parent_menus($condition=array())
	{
		$this->db->from($this->tableName);
	
			$this->db->where('adminMenuParentId',0);
			$this->db->where('status',1);
			$this->db->where($condition);
			$query = $this->db->get();
			$result = $query->result_array();
					
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}
	
	public function get_child_menus($parent_id)
	{
		$this->db->from($this->tableName);
	
			$this->db->where('adminMenuParentId',$parent_id);
			$this->db->where('status',1);
			$query = $this->db->get();
			$result = $query->result_array();
					
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}
	
	
	public function get_permission($user_type_id = '',$submenu_id)
	{
		$this->db->from(ADM_USERTYPE_DEFAULT_PERMISSION);
		if($user_type_id == ''){
			$query = $this->db->get();
			$result = $query->result_array();
		}else{
			$this->db->where('adminTypeId',$user_type_id);
			$this->db->where('adminMenuId',$submenu_id);
			$query = $this->db->get();
			$result = $query->row_array();
		}
		//echo $this->db->last_query();die;
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}

	public function get_default_permission($condition=array())
	{
		$this->db->from(ADM_USERTYPE_DEFAULT_PERMISSION);
	    if(!empty($condition)){
	    $this->db->where($condition);	
	    }    
		$query = $this->db->get();
		$result = $query->result_array();
		
		//echo $this->db->last_query();die;
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}



	public function get_user_permission($user_id = '',$menu_id)
	{
		$this->db->from(ADM_USER_MENU_PERMISSION);
		if($user_id == ''){
			$query = $this->db->get();
			$result = $query->result_array();
		}else{
			$this->db->where('adminUserId',$user_id);
			$this->db->where('adminMenuId',$menu_id);
			$query = $this->db->get();
			$result = $query->row_array();
		}
		
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}
	

}
