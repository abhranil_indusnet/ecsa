<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Modeladminuser extends CI_Model {

    function __construct() {
        //$this->tableName = ADM_USERS; //'mrc_adminUsers';
        //$this->mrc_adminMenuMaster = ADM_MENU_MASTER; //'mrc_adminMenuMaster';
        $this->primaryKey = 'id';
    }

    public function get_num_rows($user_id = '',$params=array()) {
        $this->db->from(ADM_USERS);
        $this->db->where("status", 1);
        if ($user_id != '') {
        $this->db->where_not_in("id", $user_id);
        }
        $this->db->where('deleted', '0');

        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("notInCondition",$params)){
          foreach ($params['notInCondition'] as $key => $value) {
                $this->db->where_not_in($key,$value);
            }
        }

        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $user_id);
            
            $query = $this->db->get();
            $result = $query->row_array();
        }
      
        return ($query->num_rows());
    }

    public function get_rows($user_id = '', $start = '', $noOfRecord = '', $orderBy = '',$params=array()) {
        $this->db->from(ADM_USERS);
        $this->db->where('deleted', '0');
         if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("notInCondition",$params)){
          foreach ($params['notInCondition'] as $key => $value) {
                $this->db->where_not_in($key,$value);
            }
        }
        
        

        if ($start != '' && $noOfRecord != '') {
            $this->db->limit($noOfRecord, $start);
        }

        if ($orderBy != '') {
            $this->db->order_by($orderBy, 'desc');
        }
        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $user_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }

        
         //echo $this->db->last_query();die;
        //pr($result);die;
        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function get_admin_email($email = '', $user_id = '') {
        $this->db->from(ADM_USERS);
        $this->db->where("adminUsername", $email);
        $this->db->where('deleted', '0');

        if ($user_id != '') {
            $this->db->where("id <>", $user_id);
        }

        $query = $this->db->get();
        $query->result_array();

        return ($query->num_rows());
    }

    public function insert($user_data = array()) {
        $insert = $this->db->insert(ADM_USER_LOGS, $user_data);

        return $insert ? $this->db->insert_id() : FALSE;
    }

    public function update($user_data = array(), $conditions = array()) {

        /* if(!array_key_exists("modified",$user_data)){
          $user_data['modified'] = date("Y-m-d H:i:s");
          } */

        $update = $this->db->update(ADM_USER_LOGS, $user_data, $conditions);

        return $update ? TRUE : FALSE;
    }

    public function update_forgetPassword($user_data = array(), $conditions = array()) {

        $update = $this->db->update(ADM_USERS, $user_data, $conditions);
        //$this->db->last_query();
        return $update ? TRUE : FALSE;
    }

    public function fieldValueCheck($field_value_array) {
        $where = $field_value_array;
        $query = $this->db->get_where(ADM_USERS, $where);
        //echo $this->db->last_query();die;
        if ($query->num_rows() > 0) { 
            $result = $query->row_array();
            return $result[$this->primaryKey];
        } else {
            return FALSE;
        }
    }

    public function get_parent_menus() {
        $this->db->from(ADM_MENU_MASTER);

        $this->db->where('adminMenuParentId', 0);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result_array();

        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function get_permission($user_id = '', $submenu_id) {
        $this->db->from(ADM_USER_MENU_PERMISSION);
        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where('adminUserId', $user_id);
            $this->db->where('adminMenuId', $submenu_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }
        //echo $this->db->last_query();
        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function get_child_menus($parent_id) {
        $this->db->from(ADM_MENU_MASTER);

        $this->db->where('adminMenuParentId', $parent_id);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result_array();

        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function get_adminmenurows($user_id = '') {
        $this->db->from(ADM_MENU_MASTER);
        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $user_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }

        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function insertindb($tblName = '', $userData = array()) {
        $insert = $this->db->insert($tblName, $userData);

        return $insert ? $this->db->insert_id() : FALSE;
    }

    public function updatedb($tblName = '', $userData = array(), $conditions = array()) {
        $update = $this->db->update($tblName, $userData, $conditions);

        return $update ? TRUE : FALSE;
    }

    public function deletefromdb($tblName = '', $condition = array()) {
        $delete = $this->db->delete($tblName, $condition);
        return $delete ? TRUE : FALSE;
    }

}
