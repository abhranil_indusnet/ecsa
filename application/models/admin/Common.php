<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common extends CI_Model{
	

	
	public function get_data($table_name,$key="")
	{
		$this->db->from($table_name);
		if($key == ''){
			$query = $this->db->get();
			$result = $query->result_array();
		}else{
			//$this->db->where($this->primaryKey,$key);
			$this->db->where('id',$key);
			$query = $this->db->get();
			$result = $query->row_array();
		}
		
		return ($query->num_rows() > 0)?$result:FALSE;
		
	}

	public function get_rows($param = array(),$tbl='')
	{
		if(!is_array($param) && $param != ''){
			$params['id'] = $param;
		}else{
			$params = $param;
		}
		
		$this->db->select('*');
		$this->db->from($tbl);

		if(array_key_exists("conditions",$params)){
			foreach ($params['conditions'] as $key => $value) {
				
				 $this->db->where($key,$value);
				
			}
		}else{
			foreach ($params as $key => $value) {
				$this->db->where('id',$value);
			}
		}
        $this->db->order_by('id','desc');
		if(array_key_exists("limit",$params))
			{
             $this->db->limit($params['limit']['length'],$params['limit']['start']);
			}
		
		if(!array_key_exists("id",$params)){
			$query = $this->db->get();
		    $result = $query->result_array();			
		}
		else
		{
			$query = $this->db->get();
			$result = $query->row_array();
		}
		return ($query->num_rows() > 0)?$result:FALSE;
	}

	
	
	
	
	
	public function insert_data($table_name,$data = array())
	{
		

		$insert = $this->db->insert($table_name,$data);

		return $insert?$this->db->insert_id():FALSE;
	}
	
	public function update_data($table_name,$data = array(), $conditions = array())
	{
	
		$update = $this->db->update($table_name,$data,$conditions);

		return $update?TRUE:FALSE;
	}
	
	public function delete_data($table,$condition)
	{
		$delete = $this->db->delete($table,$condition);
		return $delete?TRUE:FALSE;
	}

	public function getCityFromCountry($country_id='') {

		$this->db->select('c.*');
        $this->db->from(CITY.' as c');
        $this->db->join(STATE.' as s', 'c.state_id=s.id','inner');
        $this->db->join(COUNTRY.' as con', 's.country_id=con.id','inner');
        $this->db->where('con.id',$country_id);
        
        
            $query = $this->db->get();
            $result = $query->result_array();
       
         
        //pr($result);die;
        return ($query->num_rows() > 0) ? $result : FALSE;
    }
	

}
