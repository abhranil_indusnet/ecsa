<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class Modeljobseeker extends CI_Model {

    function __construct() {
        $this->tableName = JOB_SEEKER_MASTER;
        $this->jbp_adminMenuMaster = ADM_MENU_MASTER;
        $this->primaryKey = 'id';
    }

    public function get_num_rows($user_id = '') {
        $this->db->from($this->tableName);
        $this->db->where(array('deleted' => "0"));

        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $user_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }

        return ($query->num_rows());
    }

    public function get_rows($user_id = '', $start = '', $noOfRecord = '', $orderBy = '',$tableName='') {
        if($tableName != ''){
            $this->tableName = $tableName;
        }
        $this->db->from($this->tableName);
        
        $this->db->where(array('deleted' => "0"));

        if ($start != '' && $noOfRecord != '') {
            $this->db->limit($noOfRecord, $start);
        }

        if ($orderBy != '') {
            $this->db->order_by($orderBy, 'desc');
        }

        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $user_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }

        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    function getSeekerList($start = '', $noOfRecord =''){

            $this->db->select('um.userEmail,sm.*');
            $this->db->from(USER_MASTER . ' um');
            $this->db->join(JOB_SEEKER_MASTER . ' sm', 'um.id = sm.userId', 'left');
            $this->db->where(array('um.status'=> '1','um.deleted'=> '0','um.userTypeId' => TYPE_JOB));
            $this->db->order_by('um.id', 'desc');

            if ($start != '' && $noOfRecord != '') {
                $this->db->limit($noOfRecord, $start);
            }

            $recordSet = $this->db->get();

            if ($recordSet->num_rows() > 0) {
                return $recordSet->result_array();
            }else{
                return false;
            }
        
    }

    function getSeekerDetail($userId){

        if(isset($userId) && !empty($userId)){
            $this->db->select('um.userEmail,sm.*');
            $this->db->from(USER_MASTER . ' um');
            $this->db->join(JOB_SEEKER_MASTER . ' sm', 'um.id = sm.userId', 'left');
            $this->db->where(array('um.status'=> '1','um.deleted'=> '0','um.id' => $userId ,'um.userTypeId' => TYPE_JOB));
            $recordSet = $this->db->get();
            if ($recordSet->num_rows() > 0) {
                return $recordSet->row_array();
            }else{
                return false;
            }
        }else{
            return false;
        }
        
    }

    public function insert($user_data = array()) {
        $insert = $this->db->insert('mrc_adminUserLogs', $user_data);

        return $insert ? $this->db->insert_id() : FALSE;
    }

    public function update($user_data = array(), $conditions = array()) {

        /* if(!array_key_exists("modified",$user_data)){
          $user_data['modified'] = date("Y-m-d H:i:s");
          } */

        $update = $this->db->update('mrc_adminUserLogs', $user_data, $conditions);

        return $update ? TRUE : FALSE;
    }

    public function update_forgetPassword($user_data = array(), $conditions = array()) {

        $update = $this->db->update($this->tableName, $user_data, $conditions);
        //$this->db->last_query();
        return $update ? TRUE : FALSE;
    }

    public function fieldValueCheck($field_value_array) {
        $where = $field_value_array;
        $query = $this->db->get_where($this->tableName, $where);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result[$this->primaryKey];
        } else {
            return FALSE;
        }
    }

    public function get_parent_menus() {
        $this->db->from($this->mrc_adminMenuMaster);

        $this->db->where('adminMenuParentId', 0);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result_array();

        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function get_permission($user_id = '', $submenu_id) {
        $this->db->from('mrc_adminUserMenuPermission');
        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where('adminUserId', $user_id);
            $this->db->where('adminMenuId', $submenu_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }
        //echo $this->db->last_query();
        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function get_child_menus($parent_id) {
        $this->db->from($this->mrc_adminMenuMaster);

        $this->db->where('adminMenuParentId', $parent_id);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result_array();

        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function getadminmenurows($user_id = '') {
        $this->db->from($this->mrc_adminMenuMaster);
        if ($user_id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $user_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }

        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function insertindb($tblName = '', $userData = array()) {
        $insert = $this->db->insert($tblName, $userData);

        return $insert ? $this->db->insert_id() : FALSE;
    }

    public function updatedb($tblName = '', $userData = array(), $conditions = array()) {
        $update = $this->db->update($tblName, $userData, $conditions);

        return $update ? TRUE : FALSE;
    }

    public function deletefromdb($tblName = '', $condition = array()) {
        $delete = $this->db->delete($tblName, $condition);
        //$data = array('deleted' => "1");
        //$delete = $this->db->update($tblName, $data, $condition);
        return $delete ? TRUE : FALSE;
    }

}
