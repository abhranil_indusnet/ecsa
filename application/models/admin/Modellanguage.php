<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Modellanguage extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->primaryKey = 'id';
    }

    public function getTotalRows($id = '') {
        $this->db->from(LANGUAGE_MASTER);
        $this->db->where(array('deleted' => "0"));

        if ($id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $id);
            $query = $this->db->get();
            $result = $query->row_array();
        }
        return ($query->num_rows());
    }

    public function getRows($id = '', $start = '', $noOfRecord = '', $orderBy = '') {
        $this->db->from(LANGUAGE_MASTER);
        $this->db->where(array('deleted' => "0"));

        if ($start != '' && $noOfRecord != '') {
            $this->db->limit($noOfRecord, $start);
        }

        if ($orderBy != '') {
            $this->db->order_by($orderBy, 'desc');
        }

        if ($id == '') {
            $query = $this->db->get();
            $result = $query->result_array();
        } else {
            $this->db->where($this->primaryKey, $id);
            $query = $this->db->get();
            $result = $query->row_array();
        }
        return ($query->num_rows() > 0) ? $result : FALSE;
    }

    public function insert($data = array()) {
        $insert = $this->db->insert(LANGUAGE_MASTER, $data);

        return $insert ? $this->db->insert_id() : FALSE;
    }

    public function update($data = array(), $conditions = array()) {
        $query = $this->db->get_where(LANGUAGE_MASTER, $conditions);
        $query->row_array();
        if ($query->num_rows() > 0) {
            $update_res = $this->db->update(LANGUAGE_MASTER, $data, $conditions);
            return $update_res;
        }
    }
    
    public function delete($condition = array()) {
        $data = array("deleted" => "1","deleted_by"=>$this->session->userdata('admin_user_id'));
        $delete = $this->db->update(LANGUAGE_MASTER, $data, $condition);
        return $delete ? TRUE : FALSE;
    }

    public function fieldValueCheck($field_value_array)
    {
        $where = $field_value_array;
        $query = $this->db->get_where(LANGUAGE_MASTER,$where);
        if($query->num_rows() > 0){
            $result = $query->row_array();
            return $result['id'];
        }else{
            return FALSE;
        }
    }

}
