<?php



if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_leftmenu')) {

    function get_leftmenu() {
        $ci = & get_instance();
        $ci->db->select('am.*');
        $ci->db->from(ADM_MENU_MASTER.' as am');
        //$ci->db->join(ADM_USERTYPE_DEFAULT_PERMISSION.' as aup','aup.adminMenuId =am.id','inner');
        $ci->db->where('am.adminMenuParentId', 0);
        $ci->db->where('am.status', 1);
        $ci->db->order_by('am.adminMenuOrder', 'ASC');
        $ci->db->group_by('am.adminMenuGroupName');
        $query = $ci->db->get();
        $rows = $query->result();

        $menu = '';

        foreach ($rows as $row) {
            $menu .='<div class="left-sidebar-section">
                            <div class="section-title">' . $row->adminMenuGroupName . '</div>
                            <ul class="list-unstyled" id="navigation">' . get_leftchildmenu($row->adminMenuGroupName) . '</ul></div>';
        }

        echo $menu;
    }

}

if (!function_exists('get_leftchildmenu')) {

    function get_leftchildmenu($group_name) {
        $ci = & get_instance();
        $ci->db->select('am.*');
        $ci->db->from(ADM_MENU_MASTER.' as am');
        //if($ci->session->userdata('admin_user_type_id')!=1){
        $ci->db->join(ADM_USER_MENU_PERMISSION.' as aup','aup.adminMenuId =am.id','inner');    
        $ci->db->where('aup.adminUserId =', $ci->session->userdata('admin_user_id'));    
        //}
        $ci->db->where('am.adminMenuGroupName', $group_name);
        $ci->db->where('am.status', 1);
        $ci->db->where('am.adminMenuParentId =', '0');
        $ci->db->order_by('am.adminMenuOrder', 'ASC');

        
       
        
        $query = $ci->db->get();
        $rows = $query->result();
        //echo $ci->db->last_query();die;
        $active = '';
        foreach ($rows as $row) {
            if ($row->adminMenuSlug == '#') {
                $active .= '<li><a href="' . base_url() . 'admin/' . $row->adminMenuSlug . '"><span class="btn btn-flat collapsed"  data-toggle="collapse" data-parent="#' . $group_name . '" data-target="#' . $row->adminMenuIcon . '">
								<span class="btn-title">' . $row->adminMenuName . '</span>
								<i class="material-icons pull-left icon">' . $row->adminMenuIcon . '</i><i class="pull-right fa fa-caret-down"></i></span></a><div class="collapse" id="email"><ul class="list-unstyled">' . get_leftSubchildmenu($row->id) . '</ul></div>
							</li>';
            } else {
                $active .= '<li><a href="' . base_url() . 'admin/' . $row->adminMenuSlug . '"><span class="btn btn-flat" >
								<span class="btn-title">' . $row->adminMenuName . '</span>
								<i class="material-icons pull-left icon">' . $row->adminMenuIcon . '</i></span></a>
							</li>';
            }
        }

        return $active;
    }

}

if (!function_exists('get_leftSubchildmenu')) {

    function get_leftSubchildmenu($parent_id) {
        $ci = & get_instance();
        $ci->db->select('am.*');
        $ci->db->from(ADM_MENU_MASTER.' as am');
        //if($ci->session->userdata('admin_user_type_id')!=1){
        $ci->db->join(ADM_USER_MENU_PERMISSION.' as aup','aup.adminMenuId =am.id','inner');    
        $ci->db->where('aup.adminUserId =', $ci->session->userdata('admin_user_id'));     
        //}
       
        $ci->db->where('am.adminMenuParentId', $parent_id);
        $ci->db->where('am.status', 1);
        $ci->db->where('am.adminMenuParentId !=', '0');

        $ci->db->order_by('am.adminMenuOrder', 'ASC');
        $query = $ci->db->get();
        $rows = $query->result();

        $activeSubChild = '';
        foreach ($rows as $row) {
            $activeSubChild .= '<li><a href="' . base_url() . 'admin/' . $row->adminMenuSlug . '" class="btn btn-flat"><span class="title">' . $row->adminMenuName . '</span></a></li>';
        }

        return $activeSubChild;
    }

}


if (!function_exists('sendEmail')) {
    
   function sendEmail($emailTo='', $emailFrom='', $emailSubject='', $emailMessage='',$userName='')
   {
       $ci = & get_instance();
       $ci->load->library('email'); 
        
       $ci->email->from($emailTo, $userName);
       $ci->email->to($emailTo);
       $ci->email->subject($emailSubject);
       $ci->email->message($emailMessage);
       
       $sendemail = $ci->email->send();
       
       return $sendemail;
   }
}

//check controller/Menu permission in admin section 
//created by Ankur


if (!function_exists('checkPermission')) {

    function checkPermission($adminMenuSlug) {
        $ci = & get_instance();
        $ci->db->select('am.*');
        $ci->db->from(ADM_MENU_MASTER.' as am');
        $ci->db->join(ADM_USER_MENU_PERMISSION.' as aup','aup.adminMenuId =am.id','inner');
        $ci->db->where('am.adminMenuSlug', $adminMenuSlug);
        $ci->db->where('am.status', 1);
        $ci->db->where('aup.adminUserId =', $ci->session->userdata('admin_user_id'));    
        $query = $ci->db->get();
        if($query->num_rows() == 0){
         
         redirect('admin');
        }
    }

}

if (!function_exists('checkActionPermission')) {

    function checkActionPermission($adminMenuSlug,$actionType) { 
        $ci = & get_instance();
        $ci->db->select('aup.adminMenuPermission');
        $ci->db->from(ADM_USER_MENU_PERMISSION.' as aup');
        $ci->db->join(ADM_MENU_MASTER.' as am','aup.adminMenuId =am.id','inner');
        $ci->db->where('am.adminMenuSlug', $adminMenuSlug);
        $ci->db->where('aup.adminUserId =', $ci->session->userdata('admin_user_id'));    
        $query = $ci->db->get();
       
        if($query->num_rows() > 0){
         $adminMenuPermission=$query->row_array();

         $adminMenuPermission=$adminMenuPermission['adminMenuPermission'];

         if(in_array($actionType,json_decode($adminMenuPermission))){
            return true;
         }else{
            return false;
         }
         
        }
    }

}


if (!function_exists('pr')) {

    function pr($arr, $e = 1) {
        if (is_array($arr)) {
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        } else {
            echo "<br>Not an array...<br>";
            echo "<pre>";
            var_dump($arr);
            echo "</pre>";
        }

        if ($e == 1)
            exit();
        else
            echo "<br>";
    }

}


